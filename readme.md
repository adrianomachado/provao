# Backend Ordenação do provão

API responsável pela ordenção do provão.

### Instalação

Esta aplicação necessita do [Node.js](https://nodejs.org/) v10+ e do [yarn](https://yarnpkg.com/).

Para instalar as dependências:

```sh
$ yarn install
```

### Executar

Para iniciar o servidor executar:

```sh
$ yarn run dev
```

### Produção

Todo código que subir para o branch ``master`` irá fazer o
build de forma automática e subir para produção.


### Documentação

A documentação da api encontra-se em:
- Ordenação provão: https://provaoordenacao.docs.apiary.io/#
- Provão online: https://provaoonline.docs.apiary.io/#

