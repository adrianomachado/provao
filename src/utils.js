const regexDigitChecker = /\D/g;
const containsOnlyNumber = word => word.search(regexDigitChecker) === -1;

const addWhereInToQuery = (builder, arr, field) => {
  if (arr.length > 0) {
    builder = builder.whereIn(field, arr);
  }
  return builder;
};

module.exports = { containsOnlyNumber, addWhereInToQuery };
