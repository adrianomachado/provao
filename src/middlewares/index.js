const { containsOnlyNumber } = require('../utils');
const ErrorHandler = require('../services/ErrorHandler');

const validateParamMiddleware = param => {
  return (req, res, next) => {
    if (!containsOnlyNumber(req.params[param])) {
      const error = new ErrorHandler({
        status: 400,
        message: 'O id precisa ser um número!',
      });
      return res.status(error.status).send(error);
    }
    return next();
  };
};

const handleErrorMiddleware = (err, req, res, next) => {
  if (err.hasOwnProperty('status')) { //eslint-disable-line
    return res.status(err.status).json(err);
  }
  const error = new ErrorHandler({
    status: 500,
    message: err,
  });
  return res.status(error.status).json(error);
};

module.exports = { validateParamMiddleware, handleErrorMiddleware };
