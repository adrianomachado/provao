const express = require('express');

const router = express.Router();

const GrupoProduto = require('../models/GrupoProduto');

router.get('/', async (req, res, next) => {
  try {
    const grupos = await GrupoProduto.get(req.query);
    return res.json(grupos);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
