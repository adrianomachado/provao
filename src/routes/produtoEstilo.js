const express = require('express');

const router = express.Router();

const ProdutoEstilo = require('../models/ProdutoEstilo');
const ArquivoController = require('../controllers/ArquivoController');
const ProdutoEstiloController = require('../controllers/ProdutoEstiloController');

router.get('/', async (req, res, next) => {
  try {
    const produtosEstilo = await ProdutoEstilo.getProdutos(req.query);
    return res.json(produtosEstilo);
  } catch (e) {
    return next(e);
  }
});

router.get('/:id_produto_estilo', ProdutoEstiloController.list);
router.patch('/update', ProdutoEstiloController.update);
router.get('/image/:id_produto_estilo', ArquivoController.show);

module.exports = router;
