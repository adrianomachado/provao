const express = require('express');

const router = express.Router();

const Linha = require('../models/LinhaProduto');

router.get('/', async (req, res, next) => {
  try {
    const linhas = await Linha.get(req.query);
    return res.json(linhas);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
