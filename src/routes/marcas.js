const express = require('express');

const router = express.Router();

const Marca = require('../models/Marca');

router.get('/', async (req, res, next) => {
  try {
    const marcas = await Marca.getMarcas();
    return res.json(marcas);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
