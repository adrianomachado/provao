const express = require('express');

const router = express.Router();

const Colecao = require('../models/Colecao');

router.get('/', async (req, res, next) => {
  try {
    const colecoes = await Colecao.getColecoesEstilo();
    return res.json(colecoes);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
