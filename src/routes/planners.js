const express = require('express');

const router = express.Router();

const Usuario = require('../models/Usuario');

router.get('/', async (req, res, next) => {
  try {
    const usuarios = await Usuario.getPlanners().limit(100);
    return res.json(usuarios);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
