const express = require('express');

const router = express.Router();

const RodadaProvao = require('../models/RodadaProvao');
const { validateParamMiddleware } = require('../middlewares/index');

const RodadaAtivaController = require('../controllers/RodadaAtivaController');
const VotoController = require('../controllers/VotoController');

router.post('/', async (req, res, next) => {
  try {
    const rodadaProvaoCriada = await RodadaProvao.createRodadaProvao(req.body);
    return res
      .status(200)
      .json({ msg: 'Rodada criada com sucesso', rodadaProvaoCriada });
  } catch (e) {
    console.log(e);
    return res.json(e);
  }
});

router.delete(
  '/:id_rodada',
  validateParamMiddleware('id_rodada'),
  async (req, res, next) => {
    const { id_rodada } = req.params;

    try {
      const rodadaProvao = await RodadaProvao.deleteRodadaById(id_rodada);
      return res.json(rodadaProvao);
    } catch (e) {
      return next(e);
    }
  }
);

router.patch(
  '/:id_rodada',
  validateParamMiddleware('id_rodada'),
  async (req, res, next) => {
    try {
      const rodadasProvao = await RodadaProvao.updateRodadaProvao(
        req.params,
        req.body
      );
      return res.json(rodadasProvao);
    } catch (e) {
      return next(e);
    }
  }
);

router.get('/:id_rodada/iniciar', RodadaAtivaController.store);
router.get('/:id_rodada/finalizar', RodadaAtivaController.update);
router.post('/:id_rodada/voto', VotoController.store);
router.get('/ultima_rodada/:id_provao', async (req, res, next) => {
  const { id_provao } = req.params;
  try {
    if (!id_provao) {
      return res.status(400).json({
        error:
          'Você precisa informar o parâmetro id_provao na url da requisição ',
      });
    }
    const ultima_rodada_votada = await RodadaProvao.getLastRodada(id_provao);
    return res.json({
      id_ultima_rodada_votada: ultima_rodada_votada[0][0].id_rodada,
    });
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
