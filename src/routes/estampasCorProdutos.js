const express = require('express');

const router = express.Router();

const EstampaCorProduto = require('../models/EstampaCorProduto');

router.get('/', async (req, res, next) => {
  try {
    const estampas = await EstampaCorProduto.get(req.query);
    return res.json(estampas);
  } catch (e) {
    return next(e);
  }
});

module.exports = router;
