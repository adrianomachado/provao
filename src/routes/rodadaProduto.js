const express = require('express');

const router = express.Router();

const RodadaProduto = require('../models/RodadaProduto');
const { validateParamMiddleware } = require('../middlewares/index');

router.delete(
  '/:id_rodada',
  validateParamMiddleware('id_rodada'),
  async (req, res, next) => {
    const { id_rodada } = req.params;

    try {
      const rodadaProduto = await RodadaProduto.deleteRodadaProdutoById(
        id_rodada
      );
      return res.json(rodadaProduto);
    } catch (e) {
      return next(e);
    }
  }
);

module.exports = router;
