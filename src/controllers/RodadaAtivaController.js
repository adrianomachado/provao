const RodadaProvao = require('../models/RodadaProvao');
const RodadaProduto = require('../models/RodadaProduto');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

class RodadaAtivaController {
  async startNextRound(req, res) {
    const { id_provao, id_rodada } = req.query;

    try {
      const rodada = await RodadaProvao.getRodadaById(id_rodada);
      const { dia } = rodada[0];
      const nextRound = await RodadaProvao.getNextRodadaInProvao(
        dia,
        id_rodada,
        id_provao
      );

      const { id_proxima_rodada } = nextRound[0][0];

      if (!id_proxima_rodada)
        return res.status(200).json({
          msg: 'Não há mais rodadas para votar neste provão',
          id_rodada: null,
          rodada_iniciada: [],
        });

      // VERIFICANDO SE AINDA TINHA ALGUMA RODADA ATIVA

      const rodadas_ativas = await RodadaProvao.query().where({
        fl_ativo: 1,
        id_provao,
      });
      if (rodadas_ativas.length >= 1) {
        await RodadaProvao.updateRodadaProvao(
          { id_rodada: rodadas_ativas[0].id_rodada },
          { fl_ativo: 0 }
        );
      }

      // iniciando nova rodada
      await RodadaProvao.updateRodadaProvao(
        { id_rodada: id_proxima_rodada },
        {
          fl_ativo: 1,
        }
      );
      const rodada_iniciada = await RodadaProvao.getRodadaById(
        id_proxima_rodada
      );

      req.io.emit(`provao_${id_provao}`, {
        votacao: true,
        id_rodada: id_proxima_rodada,
      });

      return res
        .status(200)
        .json({ rodada_iniciada, msg: 'Nova rodada iniciada' });
    } catch (err) {
      return res.status(500).json({ err });
    }
  }

  async show(req, res) {
    const { id_provao } = req.params;
    if (!id_provao)
      return res
        .status(400)
        .json({ error: 'É necessário informar um id_provao' });

    const rodada_ativa = await RodadaProvao.getRodadaAtiva(id_provao);

    if (rodada_ativa.length < 1) {
      return res
        .status(404)
        .json({ msg: 'Não há rodada ativa para esse provão', rodada_ativa });
    }
    return res.status(200).json(rodada_ativa);
  }

  async resumo(req, res, next) {
    try {
      const { id_provao } = req.params;

      const ultima_rodada_votada = await RodadaProvao.getLastRodada(id_provao);

      if (ultima_rodada_votada[0].length < 1)
        return res
          .status(404)
          .json({ err: ' Nenhuma rodada foi votada nesse provão' });

      const { id_rodada } = ultima_rodada_votada[0][0];
      const rodada = await RodadaProvao.getRodadaById(id_rodada);

      if (rodada.length < 1) {
        return res
          .status(404)
          .json({ error: 'Rodada não existe no banco', id_rodada });
      }

      const resumo_rodada = await RodadaProvao.getResumoRodada(id_rodada);

      if (resumo_rodada[0].length < 1) {
        return res
          .status(400)
          .json({ error: 'Não há resumo para esta rodada' });
      }
      const produtosRodada = await RodadaProduto.getProdutosRodada(id_rodada);
      const resumo = [];
      for (let i = 0; i < produtosRodada.length; i += 1) {
        const produto = produtosRodada[i];
        const voto = {};
        const configuracoes = [];
        resumo_rodada[0].map(resumo => {
          if (resumo.id_produto_estilo === produto.id_produto_estilo) {
            voto.id_rodada_produto = resumo.id_rodada_produto;
            voto.id_produto_estilo = resumo.id_produto_estilo;
            voto.produto = produto.produto;
            voto.desc_produto = resumo.desc_produto;
            voto.estampa_correta = resumo.estampa_correta;
            voto.avg_markup_estilo = resumo.avg_markup_estilo;
            configuracoes.push({
              nome: resumo.nome,
              id_configuracao: resumo.id_configuracao,
              voto: resumo.voto,
            });
            voto.votos = configuracoes;
          }
        });
        resumo.push(voto);
      }
      return res.status(200).json({ id_rodada, resumo });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }

    // .json({ resumo_rodada: resumo_rodada[0], produtosRodada });
  }

  async store(req, res) {
    const { id_rodada } = req.params;
    try {
      const rodada = await RodadaProvao.getRodadaById(id_rodada);
      const { id_provao } = rodada[0];
      if (rodada.length < 1) {
        return res.status(400).json({ error: 'Rodada não existe no banco' });
      }

      if (rodada[0].fl_ativo === 1) {
        req.io.emit(`provao_${id_provao}`, { votacao: true, id_rodada });

        return res.json({
          rodada_iniciada: rodada,
          config: { select: {} },
          msg: 'Rodada iniciada com sucesso',
        });
      }

      const rodadas_ativas = await RodadaProvao.query().where({
        fl_ativo: 1,
        id_provao,
      });
      if (rodadas_ativas.length >= 1) {
        await RodadaProvao.updateRodadaProvao(
          { id_rodada: rodadas_ativas[0].id_rodada },
          { fl_ativo: 0 }
        );
      }
      await RodadaProvao.updateRodadaProvao(
        { id_rodada },
        {
          fl_ativo: 1,
        }
      );
      const rodada_iniciada = await RodadaProvao.getRodadaById(id_rodada);

      req.io.emit(`provao_${id_provao}`, { votacao: true, id_rodada });
      return res.json({
        rodada_iniciada,
        config: { select: {} },
        msg: 'Rodada iniciada com sucesso',
      });
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }

  async update(req, res) {
    const { id_rodada } = req.params;
    const rodada = await RodadaProvao.getRodadaById(id_rodada);

    if (rodada[0].fl_ativo !== 1) {
      return res.status(400).json({ error: 'Esta rodada não está ativa' });
    }
    const { id_provao } = rodada[0];
    const rodada_finalizada = await RodadaProvao.updateRodadaProvao(
      { id_rodada },
      {
        fl_ativo: 0,
      }
    );
    // rodada_finalizada.resumo = await RodadaProvao.getResumoRodada(id_rodada);
    req.io.emit(`provao_${id_provao}`, { votacao: false });

    return res
      .status(200)
      .json({ rodada_finalizada, msg: 'Rodada encerrada com sucesso' });
  }
}

module.exports = new RodadaAtivaController();
