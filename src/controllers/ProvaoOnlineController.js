const Provao = require('../models/Provao');
const Etiqueta = require('../models/Etiqueta');
const ConfiguracoesProvoes = require('../models/ConfiguracoesProvoes');
const VotoProdutos = require('../models/VotoProdutos');
const RodadaProduto = require('../models/RodadaProduto');

class ProvaoOnlineController {
  async mountProvao(req, res) {
    const { id_usuario, id_marca = 6 } = req.query;

    try {
      const etiquetas = await Etiqueta.getEtiquetas(id_marca).map(
        etiqueta => etiqueta.id_etiqueta
      );

      const lista_provoes_id_disponiveis = (await Provao.getProvoesOnlineByEtiquetas(
        etiquetas
      )).map(provao => provao.id_provao);
      console.log(lista_provoes_id_disponiveis);
      if (lista_provoes_id_disponiveis.length === 0) {
        return res
          .status(400)
          .json({ msg: 'Não há provões disponíveis para votação online' });
      }
      const produtos = await Provao.getProdutosNotVoted(
        lista_provoes_id_disponiveis,
        id_usuario
      );
      if (produtos[0].length === 0)
        return res
          .status(200)
          .json({ msg: 'Você já votou em todos os produtos! Obrigado :)' });

      const id_rodada_produtos = produtos[0].map(
        produto => produto.id_rodada_produto
      );

      // se o produto não tiver sido votado, ele não vai aparecer nessa query, alguma condição pra trazer produto votado num_votos_produto = 0
      if (id_rodada_produtos.length < 1)
        return res.status(400).json({ msg: 'Não há produtos nesses provões' });
      const produto_com_menos_votos = await VotoProdutos.getProdutosLessVoted(
        id_rodada_produtos,
        id_usuario
      );

      if (produto_com_menos_votos[0].length < 1) {
        return res.status(400).json({ msg: 'não há produtos menos votados' });
      }
      const { id_rodada_produto } = produto_com_menos_votos[0][0];

      const configuracoes = await ConfiguracoesProvoes.getConfiguracoesByRodadaProduto(
        id_rodada_produto
      );
      const produto = await RodadaProduto.getProdutobyRodadaProduto(
        id_rodada_produto
      );
      return res.json({
        produto,
        id_rodada_produto,
        configuracoes: configuracoes[0],
        id_rodada_produtos,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }
}

module.exports = new ProvaoOnlineController();
