const ProdutoEstilo = require('../models/ProdutoEstilo');

class PlannerController {
  async show(req, res) {
    try {
      const planners = await ProdutoEstilo.getPlanners(req.query);
      if (planners) return res.status(200).json(planners);
      return res
        .status(200)
        .json({ planners: [], msg: 'Planners não encontradas' });
    } catch (err) {
      return res
        .status(500)
        .json({ err: 'Problemas para pegar lista de planners ' });
    }
  }
}

module.exports = new PlannerController();
