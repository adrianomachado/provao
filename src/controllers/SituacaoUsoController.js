const SituacaoUso = require('../models/SituacaoUso');

class SituacaoUsoController {
  async index(req, res) {
    try {
      const situacoes = await SituacaoUso.getSituacaoUso();
      return res.status(200).json(situacoes);
    } catch (err) {
      return res
        .status(400)
        .json({ error: 'Não foi possível obter as situações de uso' });
    }
  }
}

module.exports = new SituacaoUsoController();
