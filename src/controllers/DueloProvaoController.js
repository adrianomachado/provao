const DueloProvao = require('../models/DueloProvao');
const TorneioProvao = require('../models/TorneioProvao');
const Usuario = require('../models/Usuario');
const MountDuel = require('../services/MountDuel');
const Torneio = require('../services/Torneio');

class DueloProvaoController {
  async index(req, res, next) {
    const { id_usuario } = req.params;
    try {
      const torneioId = await TorneioProvao.getIdTorneioByUsuario(id_usuario);
      const duelos = await DueloProvao.getDueloProvaoToVote(torneioId);
      const votos = await Usuario.getVotosProvaoOnline(id_usuario);
      return res
        .status(200)
        .json({ id_torneios_provoes: torneioId, votos, duelos });
    } catch (err) {
      return next(err);
    }
  }

  async update(req, res, next) {
    const { id_torneios_provoes, duelos, id_usuario } = req.body;

    await DueloProvao.updateDuelos(duelos);
    const votos = await Usuario.getVotosProvaoOnline(id_usuario);
    const produtosVencedores = duelos.map(duelo => duelo.id_produto_vencedor);

    const newCombinationsId = await MountDuel.mountNewCombinations(
      produtosVencedores
    );

    if (!newCombinationsId) {
      try {
        const id_torneio_novo = await Torneio.finalizar(id_torneios_provoes);
        const novosDuelos = await DueloProvao.getDueloProvaoToVote(
          id_torneio_novo
        );
        return res.status(200).json({
          msg: 'Torneio finalizado. Criado novo torneio.',
          votos,
          duelos: novosDuelos,
          id_torneios_provoes: id_torneio_novo,
        });
      } catch (e) {
        next(e);
      }
    }

    if (newCombinationsId.error) {
      return res.status(500).json({ error: newCombinationsId });
    }

    const duelsToVote = await DueloProvao.query()
      .where({
        id_combinacoes_produtos: null,
        id_produto_vencedor: null,
        id_torneios_provoes,
      })
      .orderBy('id_duelos_provoes', 'asc');

    if (duelsToVote.length < newCombinationsId.length) {
      return res.status(400).json({
        error:
          'O número de novas rodadas e de novas combinacoes de produtos é diferente',
      });
    }

    for (let i = 0; i < newCombinationsId.length; i += 1) {
      const updateDuel = await DueloProvao.query() //eslint-disable-line
        .update({ id_combinacoes_produtos: newCombinationsId[i] })
        .where({ id_duelos_provoes: duelsToVote[i].id_duelos_provoes });
    }

    const novosDuelos = await DueloProvao.getDueloProvaoToVote(
      id_torneios_provoes
    );

    return res.status(200).json({
      msg: 'produtos votados com sucesso',
      votos,
      duelos: novosDuelos,
      id_torneios_provoes,
    });
  }
}

module.exports = new DueloProvaoController();
