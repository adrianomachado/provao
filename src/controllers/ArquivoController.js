const Arquivo = require('../models/Arquivo');

class ArquivoController {
  async show(req, res) {
    const { id_produto_estilo } = req.params;

    if (!id_produto_estilo) {
      return res
        .status(401)
        .json({ error: 'Não foi informado produto estilo como parâmetro' });
    }

    const listImages = await Arquivo.getAllImages(id_produto_estilo);
    return res.status(200).json({ listImages: listImages[0] });
  }
}

module.exports = new ArquivoController();
