const ProdutoEstilo = require('../models/ProdutoEstilo');

class ProdutoEstiloController {
  async update(req, res) {
    const listaProduto = req.body;
    listaProduto.map(async produto => {
      try {
        const { id_produto_estilo } = produto;
        const produtoFields = Object.keys(produto);
        produtoFields.map(field => { //eslint-disable-line
          if (produto[field] === null || produto[field] === '') {
            delete produto[field];
          }
        });

        console.log(produto); //eslint-disable-line
        const produto_update = await ProdutoEstilo.query() //eslint-disable-line
          .update(produto)
          .where({ id_produto_estilo });
        return produto_update;
      } catch (err) {
        console.log(err); //eslint-disable-line
        return res.status(400).json({ error: err });
      }
    });
    return res.status(200).json({ msg: 'Produtos atualizados com sucesso' });
  }

  async list(req, res, next) {
    const { id_produto_estilo } = req.params;
    try {
      const produtoEstilo = await ProdutoEstilo.get(id_produto_estilo);
      return res.status(200).json(produtoEstilo);
    } catch(e) {
      next(e);
    }

  }
}

module.exports = new ProdutoEstiloController();
