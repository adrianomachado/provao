// const RodadaProvao = require('../models/RodadaProvao');
const Provao = require('../models/Provao');
const Etiqueta = require('../models/Etiqueta');
const ConfiguracoesProvoes = require('../models/ConfiguracoesProvoes');

class ProvaoController {
  async update(req, res) {
    try {
      const { id_provao } = req.params;
      const provao = await Provao.query().where({ id_provao });
      if (!provao[0])
        return res.status(404).json({ msg: 'provão não encontrado' });

      const { fl_provao_online } = provao[0];
      const novaFlag = fl_provao_online === 1 ? 0 : 1;
      const provaoUpdated = await Provao.makeProvaoAvailableToVoteOnline(
        id_provao,
        novaFlag
      );
      console.log(provao);
      return res.status(200).json({ provao, provaoUpdated });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }

  async list(req, res) {
    try {
      const { id_colecao, id_marca, page } = req.query;
      if (!id_marca) {
        return res.status(500).json({
          err: 'Por favor informe o id_marca',
        });
      }
      const etiquetas = await Etiqueta.getEtiquetas(id_marca).map(
        etiqueta => etiqueta.id_etiqueta
      );

      const provoes = await Provao.getProvoes(etiquetas, id_colecao, page);

      if (!provoes[0])
        return res.status(404).json({ err: ' Nenhum provão foi encontrado' });
      return res.status(200).json({ provoes });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }

  async store(req, res) {
    const { id_etiqueta, id_usuario, id_colecao, nome } = req.body;
    try {
      if (!id_colecao)
        return res.status(400).json({
          message:
            'Não foi possível criar o provão pois o campo "id_colecao" não foi informado',
          status: 400,
        });
      if (!id_etiqueta)
        return res.status(400).json({
          message:
            'Não foi possível criar o provão pois o campo "id_etiqueta" não foi informado',
          status: 400,
        });
      if (!id_usuario)
        return res.status(400).json({
          message:
            'Não foi possível criar o provão pois o campo "id_usuario" não foi informado',
          status: 400,
        });
      if (!nome)
        return res.status(400).json({
          message:
            'Não foi possível criar o provão pois o campo "nome" não foi informado',
          status: 400,
        });
      const provaoExists = await Provao.query().where({
        nome,
        id_etiqueta,
        id_usuario,
        id_colecao,
      });

      if (provaoExists[0]) {
        return res.status(400).json({ msg: 'Este provão já existe' });
      }
      let configuracoes_ultimo_provao = [];
      const ultimo_provao = await Provao.getLastProvaoCreated(id_etiqueta);
      if (ultimo_provao[0]) {
        configuracoes_ultimo_provao = await ConfiguracoesProvoes.listByProvao(
          ultimo_provao[0].id_provao
        );
      }
      const provao = await Provao.create(req.body);
      return res.status(201).json({
        msg: 'Provão criado com sucesso',
        provao,
        configuracoes_ultimo_provao,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err: JSON.stringify(err) });
    }
  }

  async listByBrand(req, res) {
    const { id_marca } = req.params;
    try {
      const etiquetas = await Etiqueta.getEtiquetas(id_marca).map(
        etiqueta => etiqueta.id_etiqueta
      );

      const provoes = await Provao.getProvesByEtiquetas(etiquetas);

      return res.json({ provoes });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }

  async getById(req, res) {
    const { id_provao } = req.params;
    try {
      const provao = await Provao.query()
        .eager('[colecao,usuario,etiqueta]')
        .where({ id_provao });
      if (!provao) {
        return res.status(404).json({
          error: {
            status: 404,
            msg: 'Nenhum provão encontrado',
          },
        });
      }
      return res.status(200).json({ provao: provao[0] });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }
}
module.exports = new ProvaoController();
