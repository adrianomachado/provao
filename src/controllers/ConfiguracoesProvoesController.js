const Provao = require('../models/Provao');
const Configuracoes = require('../models/Configuracoes');
const ConfiguracoesProvoes = require('../models/ConfiguracoesProvoes');

class ConfiguracoesProvoesController {
  async store(req, res) {
    try {
      const config = await Provao.updateConfigs(req.params, req.body);
      return res
        .status(201)
        .json({ msg: 'Configuração criada com sucesso', config });
    } catch (err) {
      return res.status(500).json({ err: JSON.stringify(err) });
    }
  }

  async findProvaoById(req, res, next) {
    const { id_provao } = req.params;
    try {
      const configuracoes = await ConfiguracoesProvoes.listByProvao(id_provao);
      return res.status(200).json({ configuracoes });
    } catch (e) {
      next(e);
    }
  }

  async list(req, res) {
    const { page } = req.query;
    try {
      const configuracoes = await Configuracoes.list(page);
      return res.status(200).json({ configuracoes });
    } catch (err) {
      return res.status(500).json({ err });
    }
  }
}
module.exports = new ConfiguracoesProvoesController();
