const Etiqueta = require('../models/Etiqueta');

class EtiquetaController {
  async list(req, res) {
    const { id_marca } = req.params;

    if (!id_marca) {
      return res
        .status(401)
        .json({ error: 'Não foi informado id_marca como parâmetro' });
    }

    const etiquetas = await Etiqueta.getEtiquetasWithName(id_marca);
    return res.status(200).json({ etiquetas });
  }
}

module.exports = new EtiquetaController();
