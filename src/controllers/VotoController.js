// const RodadaProvao = require('../models/RodadaProvao');
const VotoProdutos = require('../models/VotoProdutos');

class VotoController {
  async store(req, res) {
    try {
      const { id_usuario, lista_de_votos } = req.body;

      for (let i = 0; i < lista_de_votos.length; i += 1) {
        const { id_rodada_produto, votos_rodada_produto } = lista_de_votos[i];
        const rodadaVotada = await VotoProdutos.getVoto( //eslint-disable-line
          id_usuario,
          id_rodada_produto
        );
        for (let j = 0; j < votos_rodada_produto.length; j += 1) {
          const { voto, id_configuracao_provao } = votos_rodada_produto[j];
          if (rodadaVotada.length >= 1) {
            const votoUpdated = await VotoProdutos.updateVoto( //eslint-disable-line
              id_rodada_produto,
              id_usuario,
              id_configuracao_provao,
              voto
            );
          } else {
            const newVoto = await VotoProdutos.saveVoto(//eslint-disable-line
              id_rodada_produto,
              id_usuario,
              id_configuracao_provao,
              voto
            );
          }
        }
      }

      return res.status(200).json({ msgs: 'votos inseridos com sucesso' });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }
}
module.exports = new VotoController();
