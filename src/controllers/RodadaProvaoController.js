const RodadaProvao = require('../models/RodadaProvao');
const RodadaProduto = require('../models/RodadaProduto');

class RodadaProvaoController {
  async list(req, res) {
    const { id_provao } = req.params;
    const { dia } = req.query;
    try {
      const rodadasProvao = await RodadaProvao.getRodadasProvao(id_provao, dia);
      return res.json(
        rodadasProvao.map(rodadaProvao => rodadaProvao.formataParaEnvio())
      );
    } catch (err) {
      return res.status(500).json({ err });
    }
  }

  async showResumo(req, res) {
    const { id_rodada } = req.params;
    try {
      const resumo_rodada = await RodadaProvao.getResumoRodada(id_rodada);

      if (resumo_rodada[0].length < 1) {
        return res
          .status(400)
          .json({ error: 'Não há resumo para esta rodada' });
      }
      const produtosRodada = await RodadaProduto.getProdutosRodada(id_rodada);
      const resumo = [];
      for (let i = 0; i < produtosRodada.length; i += 1) {
        const produto = produtosRodada[i];
        const voto = {};
        const configuracoes = [];
        resumo_rodada[0].map(resumo => {
          if (resumo.id_produto_estilo === produto.id_produto_estilo) {
            voto.id_rodada_produto = resumo.id_rodada_produto;
            voto.id_produto_estilo = resumo.id_produto_estilo;
            voto.produto = produto.produto;
            voto.desc_produto = resumo.desc_produto;
            voto.estampa_correta = resumo.estampa_correta;
            voto.avg_markup_estilo = resumo.avg_markup_estilo;
            configuracoes.push({
              nome: resumo.nome,
              id_configuracao: resumo.id_configuracao,
              voto: resumo.voto,
            });
            voto.votos = configuracoes;
          }
        });
        resumo.push(voto);
      }
      return res.status(200).json({ id_rodada, resumo });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ err });
    }
  }
}
module.exports = new RodadaProvaoController();
