const axios = require('axios');
const ErrorHandler = require('./ErrorHandler.js');
const { BASE_URL_TORNEIO } = require('./consts');

const MESSAGE_ERROR = 'Não foi possivel criar um novo torneio. Problema na service de torneios.';

class Torneio {

  static async finalizar(id_torneios_provoes) {
    try {
      const { data } = await axios.post(`${BASE_URL_TORNEIO}/${id_torneios_provoes}`);
      return data.id_torneios_provoes;
    } catch (e) {
      throw new ErrorHandler({message: MESSAGE_ERROR, status: 500});
    }
  }

  static async criar(id_usuario) {
    try {
      const { data } = await axios.post(`${BASE_URL_TORNEIO}`, {id_usuario});
      return data.id_torneios_provoes;
    } catch (e) {
      throw new ErrorHandler({message: MESSAGE_ERROR, status: 500});
    }
  }

}

module.exports = Torneio;
