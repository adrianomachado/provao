class ErrorHandler {
  constructor({ message, status }) {
    this.status = status;
    this.message = `${this.codeTranslator()} - ${message}`;
  }

  codeTranslator() {
    switch (this.status) {
      case 400:
        return 'Bad Request';
      case 401:
        return 'Unauthorized';
      case 403:
        return 'Forbidden';
      case 404:
        return 'Not found';
      default:
        return 'Internal Server Error';
    }
  }
}

module.exports = ErrorHandler;
