const Arquivo = require('../models/Arquivo')
const CombinacaoProduto = require('../models/CombinacaoProduto')

class MountDuel {
  async firstRound(duelo_prontos) {
    try {
      const duelosToSend = [];

      for (let i = 0; i < duelo_prontos.length; i += 1) {
        const produtos = [];
        const duelObject = {};

        const duelo = duelo_prontos[i];
          const productWithPrice1 = await Arquivo.getImagesAndPrice( //eslint-disable-line
          duelo.produtos.id_produto_estilo_1
        );
          const productWithPrice2 = await Arquivo.getImagesAndPrice(//eslint-disable-line
          duelo.produtos.id_produto_estilo_2
        );

        produtos.push(productWithPrice1[0]);
        produtos.push(productWithPrice2[0]);
        duelObject.id_duelos_provoes = duelo.id_duelos_provoes;
        duelObject.produtos = produtos;
        duelosToSend.push(duelObject);
      }
      return duelosToSend;
    } catch (mySqlError) {
      return { error: mySqlError };
    }
  }

  async procuraCombinacoesExistentes(id_produto1, id_produto2) {
    return await
      CombinacaoProduto
        .query()
        .where({ //eslint-disable-line
          id_produto_estilo_1: id_produto1,
          id_produto_estilo_2: id_produto2,
        })
        .orWhere({ //eslint-disable-line
          id_produto_estilo_1: id_produto2,
          id_produto_estilo_2: id_produto1,
        })
        .map(combinacao => combinacao.id_combinacoes_produtos);
  }


  async mountNewCombinations(newProductsList) {
    if (newProductsList.length <= 1) return null;

    try {
      let novasCombinacoes = [];
      for (let i = 0; i < newProductsList.length; i += 2) {

        const combinacoesExistentes = await this.procuraCombinacoesExistentes(newProductsList[i], newProductsList[i+1]);

        if (combinacoesExistentes.length > 0) {
          novasCombinacoes.push(combinacoesExistentes[0]);
        } else {
          const combinacaoNova = await CombinacaoProduto.query().insert({//eslint-disable-line
            id_produto_estilo_1: newProductsList[i + 1],
            id_produto_estilo_2: newProductsList[i],
          });
          novasCombinacoes.push(combinacaoNova[0].id_combinacoes_produtos);
        }

      }

      return novasCombinacoes;
    } catch (mySqlError) {
      console.log(mySqlError);
      return {
        msg: 'Produtos vencedores foram salvos',
        error: mySqlError,
        errorMsg: 'Problemas para gerar nova(s) combinação de produtos',
      };
    }
  }
}

module.exports = new MountDuel();
