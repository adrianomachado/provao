require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { Model } = require('objection');
const cors = require('cors');
const knexConfig = require('./knexfile');

const knexConnection = require('knex')(knexConfig);

const { handleErrorMiddleware } = require('./middlewares');

Model.knex(knexConnection);

const marcaRouter = require('./routes/marcas');
const rodadaProvaoRouter = require('./routes/rodadaProvao');
const rodadaProdutoRouter = require('./routes/rodadaProduto');
const produtoEstiloRouter = require('./routes/produtoEstilo');
const plannerRouter = require('./routes/planners');
const colecaoRouter = require('./routes/colecoes');
const linhaRouter = require('./routes/linhas');
const grupoProdutoRouter = require('./routes/gruposProduto');
const estampasCorProdutosRouter = require('./routes/estampasCorProdutos');
const SituacaoUsoController = require('./controllers/SituacaoUsoController');
const DueloProvaoController = require('./controllers/DueloProvaoController');
const RodadaAtivaController = require('./controllers/RodadaAtivaController');

const ProvaoController = require('./controllers/ProvaoController');
const ConfiguracoesProvoesController = require('./controllers/ConfiguracoesProvoesController');
const VotoController = require('./controllers/VotoController');
const RodadaProvaoController = require('./controllers/RodadaProvaoController');
const EtiquetaController = require('./controllers/EtiquetaController');
const PlannerController = require('./controllers/PlannerController');
const ProvaoOnlineController = require('./controllers/ProvaoOnlineController');

const app = express();
const server = require('http').Server(app); //eslint-disable-line
const io = require('socket.io')(server, {
  pingInterval: 13000,
  pingTimeout: 5000,
}); //eslint-disable-line

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(('client/build')))
app.use(cors());

app.use((req, res, next) => {
  req.io = io;
  return next();
});
app.use('/api/marcas', marcaRouter);
app.use('/api/linhas_produto', linhaRouter);
app.use('/api/grupos_produto', grupoProdutoRouter);
app.use('/api/estampas_produto', estampasCorProdutosRouter);
app.use('/api/rodadas_provao', rodadaProvaoRouter);
app.use('/api/rodadas_produto', rodadaProdutoRouter);
app.use('/api/produtos_estilo', produtoEstiloRouter);
app.use('/api/planners', plannerRouter);
app.use('/api/colecoes', colecaoRouter);
app.get('/api/situacao_uso', SituacaoUsoController.index);
app.post('/api/duelos', DueloProvaoController.update);

app.get('/api/duelos/:id_usuario', DueloProvaoController.index);

// PROVÃO
app.get('/api/provoes', ProvaoController.list);
app.get('/api/provoes/proxima_rodada', RodadaAtivaController.startNextRound);

app.get('/api/provoes/:id_provao', RodadaProvaoController.list);
app.get('/api/provao/:id_provao', ProvaoController.getById);

app.post('/api/provoes', ProvaoController.store);
app.patch('/api/rodadas_produto/vote', VotoController.store);

app.get('/api/provoes/:id_marca/marca', ProvaoController.listByBrand);

// CONFIGURAÇÕES
app.get(
  '/api/provoes/:id_provao/configuracoes',
  ConfiguracoesProvoesController.findProvaoById
);
app.patch(
  '/api/provoes/:id_provao/configuracoes',
  ConfiguracoesProvoesController.store
);
app.get('/api/provoes/:id_provao/rodada_ativa', RodadaAtivaController.show);

app.get('/api/configuracoes', ConfiguracoesProvoesController.list);

app.get('/api/provoes/:id_provao/resumo', RodadaAtivaController.resumo);

app.get('/api/etiquetas/:id_marca', EtiquetaController.list);

app.get('/api/rodadas_provao/:id_rodada/resumo', RodadaProvaoController.showResumo);

app.get('/api/filterPlanner', PlannerController.show);

app.get('/api/provaoOnline', ProvaoOnlineController.mountProvao);

app.use(handleErrorMiddleware);

app.get('/api/provoes/:id_provao/disponibilizar_provao', ProvaoController.update);
// PEGANDO PLANNER

app.get('*', (req,res) => { 
  res.sendFile(path.join(__dirname,'..','client','build','index.html'));
})
const usernames = {};

io.sockets.on('connection', socket => {
  // when the client emits 'adduser', this listens and executes
  socket.on('adduser', ({ user, provao, voto }) => {
    socket.username = user;
    socket.room = provao;
    usernames[user] = { user, voto };
    socket.join(provao);
    socket.emit('updatechat', usernames);
    socket.broadcast.to(provao).emit('updatechat', usernames);
    socket.emit('updaterooms', provao);
  });

  socket.on('endVote', user => {
    usernames[user] = { user, voto: true };

    io.sockets.in(socket.room).emit('updatechat', usernames);
  });

  socket.on('disconnect', () => {
    delete usernames[socket.username];
    io.sockets.emit('updatechat', usernames);
    socket.broadcast.emit('updatechat', usernames);
    socket.leave(socket.room);
  });
});

const port = process.env.PORT || '3000';

server.listen(port, () => {
  console.log(`App listening on ${port}`);
});
module.exports = server;
