let databaseConfig = {
  client: 'mysql2',
  useNullAsDefault: true,
};
function productionConfig() {
  return {
    connection: {
      socketPath: process.env.MYSQL_SOCKET,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASS,
    },
  };
}

function developmentConfig() {
  return {
    debug: 'knex:query',
    connection: {
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASS,
      port: process.env.MYSQL_PORT,
    },
  };
}

if (process.env.NODE_ENV === 'production') {
  databaseConfig = Object.assign(databaseConfig, productionConfig());
} else {
  databaseConfig = Object.assign(databaseConfig, developmentConfig());
}

module.exports = { ...databaseConfig };
