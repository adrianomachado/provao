const { Model } = require('objection');
const { SCHEMA_ESTILO } = require('./consts');

class Configuracoes extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.configuracoes`;
  }

  static get idColumn() {
    return 'id_configuracao';
  }

  static async list(page = 0, limit = 20) {
    return await Configuracoes.query().offset(page*limit).limit(limit); //eslint-disable-line 
  }
}

module.exports = Configuracoes;
