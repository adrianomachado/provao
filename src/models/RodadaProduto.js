const { Model } = require('objection');
const ErrorHandler = require('../services/ErrorHandler');
const { SCHEMA_ESTILO } = require('./consts');
const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line


class RodadaProduto extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.rodadas_produtos`;
  }

  static get idColumn() {
    return 'id_rodada_produto';
  }

  static async deleteRodadaProdutoById(idRodada) {
    const rodadasProdutos = await RodadaProduto.query()
      .eager('votos')
      .where('id_rodada', idRodada);

    if (rodadasProdutos.length === 0) {
      throw new ErrorHandler({
        message: 'O id rodada não foi encontrado.',
        status: 404,
      });
    }

    const rodadaVotada = rodadasProdutos.some(rodadaProduto =>
      rodadaProduto.isRodadaVotada()
    );

    if (rodadaVotada) {
      throw new ErrorHandler({
        message: 'Essa rodada possui produtos votados',
        status: 403,
      });
    }

    return rodadasProdutos;
  }

  static async getProdutosRodada(id_rodada) {
    return await RodadaProduto.query() //eslint-disable-line
      .where({ id_rodada })
      .select('id_produto_estilo')
      .eager('produto.[fotoPrincipal, fotos]');
  }

  static async getProdutobyRodadaProduto(id_rodada_produto) {
    return await RodadaProduto.query() //eslint-disable-line
      .where({ id_rodada_produto })
      .select('id_produto_estilo', 'id_rodada_produto')
      .eager('produto.[fotoPrincipal, fotos]');
  }

  static async saveVotos(id_rodada_produto, data) {
    const options = {
      noDelete: ['votos'],
      // relate: true,
      unrelate: true,
    };
    return RodadaProduto.query().upsertGraph(
      {
        id_rodada_produto,
        ...data,
      },
      options
    );
  }

  // talvez tenha que receber um Id usuário
  static async toVoteProvaoOnline(provoes) {
    try {
      const list = knexConnection('estilo.rodadas_produtos')
        .join(
          `estilo.rodadas`,
          'estilo.rodadas_produtos.id_rodada',
          '=',
          'estilo.rodadas.id_rodada'
        )
        .join(
          `estilo.provoes`,
          'estilo.rodadas.id_provao',
          '=',
          'estilo.provoes.id_provao'
        )
        .join(
          `estilo.configuracoes_provoes`,
          'estilo.provoes.id_provao',
          '=',
          'estilo.configuracoes_provoes.id_provao'
        )
        .whereIn('estilo.provoes.id_provao', provoes)
        .select('id_rodada_produto', 'id_produto_estilo');
      return list;
    } catch (err) {
      return err;
    }
  }

  static get relationMappings() {
    const ProdutoEstilo = require('./ProdutoEstilo'); //eslint-disable-line
    const VotoProdutos = require('./VotoProdutos');//eslint-disable-line
    return {
      produto: {
        relation: Model.HasOneRelation,
        modelClass: ProdutoEstilo,
        join: {
          from: `${RodadaProduto.tableName}.id_produto_estilo`,
          to: `${ProdutoEstilo.tableName}.id_produto_estilo`,
        },
      },
      votos: {
        relation: Model.HasManyRelation,
        modelClass: VotoProdutos,
        join: {
          from: `${RodadaProduto.tableName}.id_rodada_produto`,
          to: `${VotoProdutos.tableName}.id_rodada_produto`,
        },
      },
    };
  }

  isRodadaVotada() {
    return this.votos.length > 0;
  }
}

module.exports = RodadaProduto;
