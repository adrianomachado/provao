const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

class EstampaCorProduto extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Estampa_Cor`;
  }

  static get idColumn() {
    return 'id_estampa_cor';
  }

  static get({ limit = 10, desc = '' }) {
    return EstampaCorProduto.query()
      .where('estampa_cor', 'like', `%${desc}%`)
      .limit(limit);
  }
}

module.exports = EstampaCorProduto;
