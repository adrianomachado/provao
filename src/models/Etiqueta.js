const { Model } = require('objection');
const { SCHEMA_ESTILO } = require('./consts');

class Etiqueta extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.etiquetas`;
  }

  static getEtiquetas(id_marca) {
    return Etiqueta.query()
      .where({ id_marca })
      .select('id_etiqueta');
  }

  static getEtiquetasWithName(id_marca) {
    return Etiqueta.query().where({ id_marca });
  }

  static get idColumn() {
    return 'id_etiqueta';
  }
}

module.exports = Etiqueta;
