const { Model } = require('objection');

const { SCHEMA_ESTILO } = require('./consts');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

class Provao extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.provoes`;
  }

  static get idColumn() {
    return 'id_provao';
  }

  static async makeProvaoAvailableToVoteOnline(id_provao, fl_provao_online) {
    return Provao.query()
      .update({ fl_provao_online })
      .where({ id_provao });
  }

  static async create({ id_colecao, id_etiqueta, id_usuario, nome }) {
    return await Provao.query().insert({    //eslint-disable-line

      id_colecao,
      id_etiqueta,
      id_usuario,
      nome,
    });
  }

  static async getProdutosNotVoted(provoes, id_usuario) {
    return knexConnection.schema
      .raw(`SELECT estilo.rodadas_produtos.id_rodada_produto
    FROM estilo.configuracoes_provoes join estilo.provoes ON estilo.provoes.id_provao = estilo.configuracoes_provoes.id_provao  JOIN estilo.rodadas ON estilo.rodadas.id_provao = estilo.provoes.id_provao   join estilo.rodadas_produtos ON estilo.rodadas_produtos.id_rodada = estilo.rodadas.id_rodada where estilo.provoes.id_provao in (${provoes})  and  estilo.rodadas_produtos.id_rodada_produto not in (select id_rodada_produto from estilo.votos_produtos where id_usuario = ${id_usuario}) group by estilo.rodadas_produtos.id_rodada_produto`);
  }

  static async getLastProvaoCreated(id_etiqueta) {
    return await Provao.query() //eslint-disable-line
      .where({ id_etiqueta })
      .orderBy('id_provao', 'desc')
      .limit(1);
  }

  static async getProvesByEtiquetas(etiquetas) {
    const provoes =  await Provao.query()//eslint-disable-line
      .whereIn('id_etiqueta', etiquetas)
      .eager('[colecao,usuario,etiqueta]');
    return provoes;
  }

  static async getProvoesOnlineByEtiquetas(etiquetas) {
    return Provao.query()//eslint-disable-line
      .whereIn('id_etiqueta', etiquetas)
      .andWhere('fl_provao_online', 1);
  }

  static async getProvoes(etiquetas, id_colecao, page = 0, limit = 300) {
    if (!id_colecao) {
      const provoes =  await Provao.query().eager('[colecao,usuario,etiqueta]')//eslint-disable-line
        .whereIn('id_etiqueta', etiquetas)
        .offset(page * limit)
        .limit(limit)
        .orderBy('id_provao', 'desc');
      return provoes.map(provao => {
        delete provao.id_colecao;
        delete provao.id_usuario;
        delete provao.id_etiqueta;
        return provao;
      });
    }
    const provoes =  await Provao.query().eager('[colecao,usuario,etiqueta]')//eslint-disable-line
      .whereIn('id_etiqueta', etiquetas)
      .where({ id_colecao })
      .offset(page * limit)
      .limit(limit);
    return provoes.map(provao => {
      delete provao.id_colecao;
      delete provao.id_usuario;
      delete provao.id_etiqueta;
      return provao;
    });
  }

  static get relationMappings() {
    const ConfiguracoesProvoes = require('./ConfiguracoesProvoes'); //eslint-disable-line
    const Colecao = require('./Colecao'); //eslint-disable-line
    const Usuario = require('./Usuario'); //eslint-disable-line
    const Etiqueta = require('./Etiqueta'); //eslint-disable-line
    return {
      configuracoes: {
        relation: Model.HasManyRelation,
        modelClass: ConfiguracoesProvoes,
        join: {
          from: `${Provao.tableName}.id_provao`,
          to: `${ConfiguracoesProvoes.tableName}.id_provao`,
        },
      },
      colecao: {
        relation: Model.HasOneRelation,
        modelClass: Colecao,
        filter: query => query.select('id_colecao', 'colecao'),
        join: {
          from: `${Provao.tableName}.id_colecao`,
          to: `${Colecao.tableName}.id_colecao`,
        },
      },
      usuario: {
        relation: Model.HasOneRelation,
        modelClass: Usuario,
        filter: query => query.select('id_usuario', 'login'),

        join: {
          from: `${Provao.tableName}.id_usuario`,
          to: `${Usuario.tableName}.id_usuario`,
        },
      },
      etiqueta: {
        relation: Model.HasOneRelation,
        modelClass: Etiqueta,
        filter: query => query.select('id_etiqueta', 'nome'),

        join: {
          from: `${Provao.tableName}.id_etiqueta`,
          to: `${Etiqueta.tableName}.id_etiqueta`,
        },
      },
    };
  }

  static async updateConfigs({ id_provao }, data) {
    return Provao.query().upsertGraph({
      id_provao,
      ...data,
    });
  }
}

module.exports = Provao;
