const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

class LinhaProduto extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Linha`;
  }

  static get idColumn() {
    return 'id_linha';
  }

  static get({ limit = 10, desc = '' }) {
    return LinhaProduto.query()
      .where('linha', 'like', `%${desc}%`)
      .limit(limit);
  }
}

module.exports = LinhaProduto;
