const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

class Marca extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Marca`;
  }

  static get idColumn() {
    return 'id_marca';
  }

  static getMarcas() {
    return Marca.query()
      .column(['id_marca', 'marca'])
      .whereNull('inativo');
  }
}

module.exports = Marca;
