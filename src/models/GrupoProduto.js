const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

class GrupoProduto extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Grupo_Produto`;
  }

  static get idColumn() {
    return 'id_grupo_produto';
  }

  static get({ limit = 10, desc = '' }) {
    return GrupoProduto.query()
      .where('grupo_produto', 'like', `%${desc}%`)
      .limit(limit);
  }
}

module.exports = GrupoProduto;
