const { Model } = require('objection');
const ErrorHandler = require('../services/ErrorHandler');
const { SCHEMA_ESTILO } = require('./consts');

class CombinacaoProduto extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.combinacoes_produtos`;
  }

  static get idColumn() {
    return 'id_combinacoes_produtos';
  }

  static get relationMappings() {
    const ProdutoEstilo = require('./ProdutoEstilo');
    return {
      produto_estilo_1: {
        relation: Model.HasOneRelation,
        modelClass: ProdutoEstilo,
        filter: CombinacaoProduto.filtroColunasProdutoEstilo,
        join: {
          from: `${CombinacaoProduto.tableName}.id_produto_estilo_1`,
          to: `${ProdutoEstilo.tableName}.id_produto_estilo`,
        },
      },
      produto_estilo_2: {
        relation: Model.HasOneRelation,
        modelClass: ProdutoEstilo,
        filter: CombinacaoProduto.filtroColunasProdutoEstilo,
        join: {
          from: `${CombinacaoProduto.tableName}.id_produto_estilo_2`,
          to: `${ProdutoEstilo.tableName}.id_produto_estilo`,
        },
      },
    };
  }

  static filtroColunasProdutoEstilo(query) {
    return query.select('id_produto_estilo', 'preco_estilo');
  }

  static async getProdutos(id_combinacoes_produtos) {
    const produtos = await CombinacaoProduto.query()
      .eager('[produto_estilo_1.[fotos, grupo, linha], produto_estilo_2.[fotos, grupo, linha]]')
      .where({ id_combinacoes_produtos });

    if (produtos.length === 0) {
      throw new ErrorHandler({message: 'A combinação de produtos não foi encontrada' , status: 404});
    }

    const { produto_estilo_1, produto_estilo_2} = produtos[0];

    return [produto_estilo_1, produto_estilo_2];
  }
}

module.exports = CombinacaoProduto;
