const { Model } = require('objection');

const { SCHEMA_ATELIER } = require('./consts');

const ID_GRUPO_PLANNER = 7;

class Usuario extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Usuario`;
  }

  static get idColumn() {
    return 'id_usuario';
  }

  static getPlanners() {
    return Usuario.query()
      .column('id_usuario', { nome: 'login' })
      .where({ id_grupo: ID_GRUPO_PLANNER });
  }

  static getPlannersPorMarca(marca) {
    return Usuario.query()
      .column('id_usuario', { nome: 'login' })
      .where({ id_grupo: ID_GRUPO_PLANNER, id_marca_grupo: marca });
  }

  static async getVotosProvaoOnline(id_usuario) {
    const TorneioProvao = require('./TorneioProvao');
    const DueloProvao = require('./DueloProvao');
    const CombinacaoProduto = require('./CombinacaoProduto');
    const { votos } = await TorneioProvao
      .query()
      .first()
      .count(`${DueloProvao.tableName}.${DueloProvao.idColumn} as votos`)
      .join(
        DueloProvao.tableName,
        `${DueloProvao.tableName}.${TorneioProvao.idColumn}`,
        '=',
        `${TorneioProvao.tableName}.${TorneioProvao.idColumn}`
      )
      .join(
        CombinacaoProduto.tableName,
        `${CombinacaoProduto.tableName}.${CombinacaoProduto.idColumn}`,
        '=',
        `${DueloProvao.tableName}.${CombinacaoProduto.idColumn}`
      )
      .where({id_usuario, fl_ativo: 1})
      .whereNotNull('id_produto_vencedor');
    return votos;
  }
}

module.exports = Usuario;
