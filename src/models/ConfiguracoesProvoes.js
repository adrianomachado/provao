const { Model } = require('objection');
const { SCHEMA_ESTILO } = require('./consts');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

class ConfiguracoesProvoes extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.configuracoes_provoes`;
  }

  static get idColumn() {
    return 'id_configuracao_provao';
  }

  static async create({ nome, tipo, valores, estilo }) {
    return await ConfiguracoesProvoes.query().insert({    //eslint-disable-line

      nome,
      tipo,
      valores,
      estilo,
    });
  }

  static async getConfiguracoesByRodadaProduto(id_rodada_produto) {
    return knexConnection.schema
      .raw(`select id_configuracao_provao, configuracoes.* from estilo.configuracoes_provoes join estilo.provoes on estilo.provoes.id_provao = estilo.configuracoes_provoes.id_provao
      join estilo.rodadas on estilo.rodadas.id_provao = estilo.provoes.id_provao
      join estilo.rodadas_produtos on estilo.rodadas_produtos.id_rodada = estilo.rodadas.id_rodada
      join estilo.configuracoes on estilo.configuracoes.id_configuracao = estilo.configuracoes_provoes.id_configuracao
      where id_rodada_produto = ${id_rodada_produto}`);
  }

  static async listByProvao(id_provao) {
    return await ConfiguracoesProvoes.query().eager('configuracoes').where({ id_provao }); //eslint-disable-line
  }

  static get relationMappings() {
    const Configuracoes = require('./Configuracoes'); //eslint-disable-line
    return {
      configuracoes: {
        relation: Model.HasOneRelation,
        modelClass: Configuracoes,
        join: {
          from: `${ConfiguracoesProvoes.tableName}.id_configuracao`,
          to: `${Configuracoes.tableName}.id_configuracao`,
        },
      },
    };
  }
}

module.exports = ConfiguracoesProvoes;
