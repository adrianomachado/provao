const { Model } = require('objection');
const ErrorHandler = require('../services/ErrorHandler');
const TorneioService = require('../services/Torneio');
const { SCHEMA_ESTILO } = require('./consts');

class TorneioProvao extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.torneios_provoes`;
  }

  static get idColumn() {
    return 'id_torneios_provoes';
  }

  static async getIdTorneioByUsuario(id_usuario){
    const torneio = await TorneioProvao.query()
      .where({id_usuario})
      .andWhere({fl_torneio_ativo: 1});

    if (torneio.length === 0 ){
      return await TorneioService.criar(id_usuario);
    }

    return torneio[0].id_torneios_provoes;
  }

}

module.exports = TorneioProvao;
