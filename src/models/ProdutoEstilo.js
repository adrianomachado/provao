const { Model } = require('objection');
const RodadaProduto = require('./RodadaProduto');
const ErrorHandler = require('../services/ErrorHandler');
const { addWhereInToQuery } = require('../utils');
const { SCHEMA_ATELIER } = require('./consts');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

class ProdutoEstilo extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Produto_Estilo`;
  }

  static get idColumn() {
    return 'id_produto_estilo';
  }

  static validateGetProdutosParams(colecao, marca) {
    const errorMsg = 'É obrigatório passar a';
    if (!colecao) {
      throw new ErrorHandler({ message: `${errorMsg} coleção'`, status: 400 });
    }
    if (!marca) {
      throw new ErrorHandler({ message: `${errorMsg} marca'`, status: 400 });
    }
  }

  static async get(id_produto_estilo) {
    const produtoEstilo = await ProdutoEstilo.query()
      .eager('fotos')
      .where({ id_produto_estilo });
    if (produtoEstilo.length === 0) {
      throw new Error({
        message: 'Produto estilo não encontrado',
        status: 400,
      });
    }
    return produtoEstilo[0];
  }

  static getPlanners({ limit = 10, desc = '' }) {
    return ProdutoEstilo.query()
      .select('planner')
      .groupBy('planner')
      .where('planner', 'like', `%${desc}%`)
      .limit(limit);
  }

  static async getProdutos({
    colecao,
    marca,
    linha = [],
    grupo = [],
    estampa = [],
    desc = '',
    planners = [],
  }) {
    ProdutoEstilo.validateGetProdutosParams(colecao, marca);

    const RodadaProvao = require('./RodadaProvao');
    const Colecao = require('./Colecao');

    const colecaoQuery = await Colecao.getColecao(colecao);

    return ProdutoEstilo.query()
      .eager('[fotoPrincipal, fotos, estampaCor]')
      .limit(20)
      .where({
        id_colecao: colecao,
        id_marca: marca,
      })
      .where(builder => {
        builder = addWhereInToQuery(builder, linha, `id_linha`);
        builder = addWhereInToQuery(builder, grupo, `id_grupo_produto`);
        builder = addWhereInToQuery(builder, estampa, `id_estampa_cor`);
        builder = addWhereInToQuery(builder, planners, `planner`);
        return builder;
      })
      .where(function() { //eslint-disable-line
        this.where('desc_produto', 'like', `%${desc}%`).orWhere(
          'produto',
          'like',
          `%${desc}%`
        );
      })
      .whereNotExists(function () {  //eslint-disable-line
        this.select('*')
          .from(RodadaProduto.tableName)
          .innerJoin(
            RodadaProvao.tableName,
            `${RodadaProvao.tableName}.${RodadaProvao.idColumn}`,
            `${RodadaProduto.tableName}.${RodadaProvao.idColumn}`
          )
          .whereRaw(
            `${RodadaProduto.tableName}.id_produto_estilo = ${ProdutoEstilo.tableName}.id_produto_estilo`
          );
        // TEM UMA DIFERENÇA!!
      });
  }

  static async getFotos(id_produto_estilo) {
    return knexConnection.schema.raw('');
  }

  static get relationMappings() {
    const Arquivo = require('./Arquivo');
    const EstampaCorProduto = require('./EstampaCorProduto');
    const LinhaProduto = require('./LinhaProduto');
    const GrupoProduto = require('./GrupoProduto');
    return {
      fotoPrincipal: {
        relation: Model.HasOneRelation,
        modelClass: Arquivo,
        join: {
          from: `${ProdutoEstilo.tableName}.id_arquivo`,
          to: `${Arquivo.tableName}.id_arquivo`,
        },
      },
      fotos: {
        relation: Model.HasManyRelation,
        modelClass: Arquivo,
        modify: query => query.where({ fl_provao: 1 }),
        join: {
          from: `${ProdutoEstilo.tableName}.id_produto_estilo`,
          to: `${Arquivo.tableName}.id_produto_estilo`,
        },
      },
      estampaCor: {
        relation: Model.HasOneRelation,
        modelClass: EstampaCorProduto,
        join: {
          from: `${ProdutoEstilo.tableName}.id_estampa_cor`,
          to: `${EstampaCorProduto.tableName}.id_estampa_cor`,
        },
      },
      grupo: {
        relation: Model.HasOneRelation,
        modelClass: GrupoProduto,
        join: {
          from: `${ProdutoEstilo.tableName}.id_grupo_produto`,
          to: `${GrupoProduto.tableName}.id_grupo_produto`,
        },
      },
      linha: {
        relation: Model.HasOneRelation,
        modelClass: LinhaProduto,
        join: {
          from: `${ProdutoEstilo.tableName}.id_linha`,
          to: `${LinhaProduto.tableName}.id_linha`,
        },
      },
    };
  }
}

module.exports = ProdutoEstilo;
