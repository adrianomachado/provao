const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

class SituacaoUso extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Situacao_Uso`;
  }

  static get idColumn() {
    return 'id_situacao_uso';
  }

  static async getSituacaoUso() {
    return await SituacaoUso.query().select('Situacao_Uso.situacao_uso as label','Situacao_Uso.id_situacao_uso as value ').where({ inativo: null }); //eslint-disable-line
  }
}

module.exports = SituacaoUso;
