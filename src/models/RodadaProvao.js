const { Model } = require('objection');
const ErrorHandler = require('../services/ErrorHandler');
const knexConfig = require('../knexfile');  // eslint-disable-line
const { SCHEMA_ESTILO } = require('./consts');

const knexConnection = require('knex')(knexConfig);// eslint-disable-line
const ROUND_LIMIT = 300;

class RodadaProvao extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.rodadas`;
  }

  static get idColumn() {
    return 'id_rodada';
  }

  static get relationMappings() {
    const RodadaProduto = require('./RodadaProduto'); //eslint-disable-line
    return {
      rodada_produtos: {
        relation: Model.HasManyRelation,
        modelClass: RodadaProduto,
        join: {
          from: `${RodadaProvao.tableName}.id_rodada`,
          to: `${RodadaProduto.tableName}.id_rodada`,
        },
      },
    };
  }

  static async updateRodadaProvao({ id_rodada }, data) {
    const options = {
      noDelete: ['rodada_produtos.produto'],
    };

    return RodadaProvao.query().upsertGraph(
      {
        id_rodada,
        ...data,
      },
      options
    );
  }

  static async getRodadasProvao(id_provao, dia) {
    if (!id_provao || !dia) {
      throw new ErrorHandler({
        message: 'O id_provao e o dia  são obrigatórios para puxar as rodadas!',
        status: 400,
      });
    }

    return await RodadaProvao.query() //eslint-disable-line
      .eager('rodada_produtos.produto.[fotoPrincipal, fotos, estampaCor]')
      .limit(ROUND_LIMIT)
      .where({
        dia,
        id_provao,
      });
    // .whereNotNull('rodada');
  }

  static async getResumoRodada(id_rodada) {
    return knexConnection.schema.raw(`SELECT
    estilo.rodadas_produtos.id_rodada_produto,
    Atelier.Produto_Estilo.id_produto_estilo,
    Atelier.Produto_Estilo.produto,
    Atelier.Produto_Estilo.desc_produto,
    Atelier.Produto_Estilo.estampa_correta,
    ROUND(Atelier.Produto_Estilo.preco_estilo / Atelier.Produto_Estilo.custo_produto, 2) AS avg_markup_estilo,
    estilo.configuracoes.nome ,
    estilo.configuracoes.id_configuracao,
    case
      when estilo.configuracoes.fl_salva_texto = 1 then GROUP_CONCAT(DISTINCT estilo.votos_produtos.voto, ': ', cont, ' votos; ' SEPARATOR ' -- ')
      ELSE avg(estilo.votos_produtos.voto)
    END as voto
  FROM
    estilo.rodadas_produtos
  JOIN estilo.rodadas ON
    estilo.rodadas_produtos.id_rodada = estilo.rodadas.id_rodada
  join estilo.provoes ON
    estilo.provoes.id_provao = estilo.rodadas.id_provao
  join estilo.configuracoes_provoes ON
    estilo.provoes.id_provao = estilo.configuracoes_provoes.id_provao
  join estilo.configuracoes on
    estilo.configuracoes_provoes.id_configuracao = estilo.configuracoes.id_configuracao
  LEFT JOIN estilo.votos_produtos ON
    estilo.rodadas_produtos.id_rodada_produto = estilo.votos_produtos.id_rodada_produto
    and estilo.configuracoes_provoes.id_configuracao_provao = estilo.votos_produtos.id_configuracao_provao
  INNER JOIN Atelier.Produto_Estilo ON
    Atelier.Produto_Estilo.id_produto_estilo = estilo.rodadas_produtos.id_produto_estilo
  JOIN Atelier.Usuario ON
    Atelier.Usuario.id_usuario = estilo.votos_produtos.id_usuario
  left join (
    select
      A.id_rodada_produto,
      id_configuracao_provao,
      count(*) as cont
    from
      estilo.votos_produtos A
    join estilo.rodadas_produtos B on
      A.id_rodada_produto = B.id_rodada_produto
    where
      id_rodada = ${id_rodada}
    group by
      A.id_rodada_produto,
      id_configuracao_provao) Z on
    estilo.votos_produtos.id_configuracao_provao = Z.id_configuracao_provao
    and estilo.votos_produtos.id_rodada_produto = Z.id_rodada_produto
  WHERE
    estilo.rodadas_produtos.id_rodada = ${id_rodada}
    and IFNULL(estilo.votos_produtos.voto,
    '') <> ''
  GROUP BY
    estilo.rodadas_produtos.id_rodada_produto,
    Atelier.Produto_Estilo.id_produto_estilo,
    Atelier.Produto_Estilo.produto,
    Atelier.Produto_Estilo.desc_produto,
    Atelier.Produto_Estilo.estampa_correta,
    estilo.configuracoes.id_configuracao,
    ROUND(Atelier.Produto_Estilo.preco_estilo / Atelier.Produto_Estilo.custo_produto, 2),
    estilo.configuracoes.nome
  order by
    id_rodada_produto asc,
    case
      when estilo.configuracoes.nome = 'Observacao' then 'z'
      else estilo.configuracoes.nome
    end asc;
  `);
  }

  static async getRodadaAtiva(id_provao) {
    return await RodadaProvao.query().eager('rodada_produtos.produto.[fotoPrincipal, fotos]').where({ id_provao, fl_ativo: 1 }); //eslint-disable-line
  }

  static async getRodadaById(id_rodada) {
    const rodada = await RodadaProvao.query()
      .eager('rodada_produtos.produto.[fotoPrincipal, fotos]').where({ id_rodada }); //eslint-disable-line
    return rodada;
  }

  static async deleteRodadaById(idRodada) {
    const rodadaProvao = await RodadaProvao.query().findById(idRodada);

    if (!rodadaProvao) {
      throw new ErrorHandler({
        message: 'A rodada não foi encontrada.',
        status: 404,
      });
    }

    await rodadaProvao.$relatedQuery('rodada_produtos').delete();
    await RodadaProvao.query().deleteById(rodadaProvao.id_rodada);

    return rodadaProvao;
  }

  static async createRodadaProvao({ dia, id_provao, nome_rodada }) {
    return await RodadaProvao.query().insert({ //eslint-disable-line
      dia,
      id_provao,
      nome_rodada,
    });
  }

  static async getLastRodada(id_provao) {
    return knexConnection.schema.raw(
      `select A.id_rodada FROM estilo.votos_produtos C JOIN estilo.rodadas_produtos B ON B.id_rodada_produto = C.id_rodada_produto JOIN estilo.rodadas A ON A.id_rodada = B.id_rodada JOIN estilo.provoes D ON D.id_provao = A.id_provao where D.id_provao = ${id_provao} GROUP BY C.id_voto_produto ORDER BY C.id_voto_produto DESC LIMIT 1`
    );
  }

  static async getNextRodadaInProvao(dia, id_rodada, id_provao) {
    return knexConnection.schema.raw(
      `select min(id_rodada) as id_proxima_rodada from estilo.rodadas where id_provao = ${id_provao} and id_rodada > ${id_rodada} and dia =${dia}
      `
    );
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id_provao', 'dia'],
      properties: {
        dia: { type: 'integer' },
        rodada: { type: 'integer' },
        id_marca: { type: 'integer' },
        colecao: { type: 'string', minLength: 1, maxLength: 255 },
      },
    };
  }

  formataParaEnvio() {
    const { id_rodada, rodada_produtos, dia, nome_rodada, fl_ativo } = this;
    return {
      fl_ativo,
      id_rodada,
      dia,
      nome_rodada,
      rodada_produtos: rodada_produtos.map(
        rodadaProduto => rodadaProduto.produto
      ),
    };
  }
}

module.exports = RodadaProvao;
