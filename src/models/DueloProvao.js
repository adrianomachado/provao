const { Model } = require('objection');
const ErrorHandler = require('../services/ErrorHandler');
const { SCHEMA_ESTILO } = require('./consts');

class DueloProvao extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.duelos_provoes`;
  }

  static get idColumn() {
    return 'id_duelos_provoes';
  }

  static async getDueloProvaoToVote(id_torneios_provoes) {
    const CombinacaoProduto = require('./CombinacaoProduto');

    if (!id_torneios_provoes) {
      throw new ErrorHandler({message: 'Id torneio inexistente', status: 400});
    }

    const duelosProvao = await DueloProvao.query()
      .where({ id_torneios_provoes })
      .whereNotNull('id_combinacoes_produtos')
      .whereNull('id_produto_vencedor');

    const duelos = await Promise.all(
      duelosProvao.map(async dueloProvao => {
        const produtos = await CombinacaoProduto.getProdutos(
          dueloProvao.id_combinacoes_produtos
        );
        return {
          id_duelos_provoes: dueloProvao.id_duelos_provoes,
          produtos,
        };
      })
    );

    return duelos;
  }

  static get relationMappings() {
    const CombinacaoProduto = require('./CombinacaoProduto'); //eslint-disable-line
    return {
      produtos: {
        relation: Model.HasOneRelation,
        modelClass: CombinacaoProduto,
        join: {
          from: `${DueloProvao.tableName}.id_combinacoes_produtos`,
          to: `${CombinacaoProduto.tableName}.id_combinacoes_produtos`,
        },
      },
    };
  }

  static async updateDuelos(duelos) {
    return await Promise.all(
      duelos.map(async duelo =>
        DueloProvao.query() //eslint-disable-line
          .update({ id_produto_vencedor: duelo.id_produto_vencedor })
          .where({ id_duelos_provoes: duelo.id_duelos_provoes })
      )
    );
  }
}

module.exports = DueloProvao;
