const { Model } = require('objection');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

const { SCHEMA_ESTILO } = require('./consts');

class VotoProdutos extends Model {
  static get tableName() {
    return `${SCHEMA_ESTILO}.votos_produtos`;
  }

  static get idColumn() {
    return 'id_voto_produto';
  }

  static async saveVoto(
    id_rodada_produto,
    id_usuario,
    id_configuracao_provao,
    voto
  ) {
    return VotoProdutos.query().insert({
      id_rodada_produto,
      id_usuario,
      id_configuracao_provao,
      voto,
    });
  }

  static async getProdutosLessVoted(id_rodada_produto, id_usuario, limit = 1) {
    return knexConnection.schema.raw(`SELECT
      COUNT(DISTINCT id_usuario) AS num_votos_produto,
      A.id_rodada_produto
  FROM
      estilo.rodadas_produtos A
  left join estilo.votos_produtos B on
      A.id_rodada_produto = B.id_rodada_produto
  left join estilo.configuracoes_provoes C on
      B.id_configuracao_provao = C.id_configuracao_provao
  left join estilo.configuracoes D on
      C.id_configuracao = D.id_configuracao
      and D.fl_online = 1
  WHERE
A.id_rodada_produto IN (${id_rodada_produto})
  GROUP BY
      A.id_rodada_produto
  ORDER BY
      num_votos_produto ASC,
      rand()
  LIMIT ${limit}`);
  }

  static async getVoto(id_usuario, id_rodada_produto) {
    return VotoProdutos.query().where({ id_usuario, id_rodada_produto });
  }

  static async updateVoto(
    id_rodada_produto,
    id_usuario,
    id_configuracao_provao,
    voto
  ) {
    return VotoProdutos.query()
      .where({ id_rodada_produto, id_usuario, id_configuracao_provao })
      .update({ voto });
  }
}

module.exports = VotoProdutos;
