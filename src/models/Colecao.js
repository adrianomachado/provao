const { Model } = require('objection');

const { SCHEMA_ATELIER } = require('./consts');

class Colecao extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Colecao`;
  }

  static get idColumn() {
    return 'id_colecao';
  }

  static getColecoesEstilo() {
    return Colecao.query().where('colecao_estilo', '1');
  }

  static async getColecao(id_colecao) {
    return Colecao.query()
      .where({colecao_estilo: 1, id_colecao})
      .first();
  }
}

module.exports = Colecao;
