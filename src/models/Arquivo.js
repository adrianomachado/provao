const { Model } = require('objection');
const { SCHEMA_ATELIER } = require('./consts');

const knexConfig = require('../knexfile');  // eslint-disable-line

const knexConnection = require('knex')(knexConfig);// eslint-disable-line

const BASE_URL_IMG = 'http://soma-images.somalabs.com.br/image';
const DIMENSIONS = '140/200';

class Arquivo extends Model {
  static get tableName() {
    return `${SCHEMA_ATELIER}.Arquivo`;
  }

  static get idColumn() {
    return 'id_arquivo';
  }

  static async getAllImages(id_produto_estilo) {
    return knexConnection.schema.raw(`SELECT 
    CONCAT('http://soma-images.somalabs.com.br/image/',
            A.nome_arquivo,
            '/500/700') AS imgPath,
            PE.desc_produto as label
      FROM
          Atelier.Produto_Estilo PE
              JOIN
          Atelier.Arquivo A ON PE.id_produto_estilo = A.id_produto_estilo
      WHERE
          PE.id_produto_estilo = ${id_produto_estilo}
      ORDER BY CASE
          WHEN A.id_arquivo = PE.id_arquivo THEN 1
          ELSE 0
      END DESC , A.id_arquivo

      `);
  }

  static async getImagesAndPrice(id_produto_estilo) {
    return knexConnection.schema.raw(`SELECT PE.id_produto_estilo,
    CONCAT('http://soma-images.somalabs.com.br/image/',
            A.nome_arquivo,
            '/500/700') AS url,
            PE.preco_estilo as preco_estilo
      FROM
          Produto_Estilo PE
              JOIN
          Arquivo A ON PE.id_produto_estilo = A.id_produto_estilo
      WHERE
          PE.id_produto_estilo = ${id_produto_estilo}
      ORDER BY CASE
          WHEN A.id_arquivo = PE.id_arquivo THEN 1
          ELSE 0
      END DESC , A.id_arquivo

      `);
  }

  $afterGet() {
    this.link_arquivo = `${BASE_URL_IMG}/${this.nome_arquivo}/${DIMENSIONS}`;
  }
}

module.exports = Arquivo;
