import {
  AUTH_LOADING,
  AUTH_LOADED,
  AUTH_RECOVERY_SUCCESS,
  AUTH_ERROR,
  AUTH_CLEAR
} from './actionTypes';

import {PLM_API_URL} from '../consts';

const postOptions = {
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  headers: {
    'Content-Type': 'application/json'
  },
};

const loading_login = () => ({
  type: AUTH_LOADING,
});

const load_login = (loginResponse) => ({
  type: AUTH_LOADED,
  ...loginResponse,
});

const recovery = () => ({
  type: AUTH_RECOVERY_SUCCESS,
});

const error = (message) => ({
  type: AUTH_ERROR,
  message,
});

const clear = () => ({
  type: AUTH_CLEAR,
});

export const forgotPassword = (username) =>
  dispatch => {
    dispatch(loading_login());

    return fetch(`${PLM_API_URL}/login/change_key?login=${username}`)
      .then(res => res.json())
      .then(({ success }) => {
        if (success) {
          dispatch(recovery());
        } else {
          dispatch(error('Usuário não encontrado!'))
        }
      })
      .catch(() => {
        dispatch(error('Usuário não encontrado!'))
      });
  };

export const signIn = (username, password) =>
  dispatch => {
    dispatch(loading_login());

    postOptions.body = JSON.stringify({
      login: username,
      senha: password
    });

    return fetch(`${PLM_API_URL}/login `, postOptions)
      .then(res => res.json())
      .then((loginResponse) => {
        if (loginResponse.token) {
          dispatch(load_login(loginResponse));
        } else {
          dispatch(error('Usuário ou senha incorretos!'));
        }
      })
      .catch(() => {
        dispatch(error('Usuário ou senha incorretos!'));
      });
  };

export const signOut = () =>
  dispatch => {
    dispatch(clear());
  };

export default {
  forgotPassword,
  signIn,
  signOut,
};
