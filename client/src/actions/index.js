import CollectionsApi from "../services/apis/CollectionsApi";
import BrandsApi from "../services/apis/BrandsApi";
import TagsApi from "../services/apis/TagsApi";
import ProvaoApi from "../services/apis/ProvaoApi";

import {
  FETCH_BRANDS_BEGIN,
  FETCH_COLLECTIONS_BEGIN,
  FETCH_BRANDS_SUCCESS,
  FETCH_COLLECTIONS_SUCCESS,
  FETCH_BRANDS_FAILURE,
  FETCH_COLLECTIONS_FAILURE,
  BRAND_TOGGLE,
  COLLECTION_TOGGLE,
  ROUND_DAYS_TOGGLE,
  GET_ROUND_DAYS,
  SHOW_LOADING_DIALOG,
  HIDE_LOADING_DIALOG,
  FETCH_ROUNDS_BEGIN,
  FETCH_ROUNDS_SUCCESS,
  FETCH_ROUNDS_FAILURE,
  FETCH_TAGS_FAILURE,
  FETCH_TAGS_SUCCESS,
  FETCH_TAGS_BEGIN,
} from "./actionTypes";

const loading = type => ({
  type
});

const load = (type, data) => ({
  type: type,
  payload: data
});

const fetchWithApi = (actionType, api, params, func = 'get') => dispatch => {
  dispatch(loading(actionType.begin));
  return api[func](params)
    .then(data => dispatch(load(actionType.success, data)))
    .catch(error => {
      throw load(actionType.failure, error.response);
    });
};

// Actions for selects
const toggle = (type, payload) => ({
  type,
  payload: {
    label: payload.children,
    value: payload.value
  }
});

export const toggleBrand = brand => toggle(BRAND_TOGGLE, brand);
export const toggleCollection = collection =>
  toggle(COLLECTION_TOGGLE, collection);
export const toggleRoundDay = collection =>
  toggle(ROUND_DAYS_TOGGLE, collection);

//*******************************

// Actions for fetchs
export const loadBrands = () =>
  fetchWithApi(
    {
      begin: FETCH_BRANDS_BEGIN,
      success: FETCH_BRANDS_SUCCESS,
      failure: FETCH_BRANDS_FAILURE
    },
    BrandsApi
  );

export const loadCollections = () =>
  fetchWithApi(
    {
      begin: FETCH_COLLECTIONS_BEGIN,
      success: FETCH_COLLECTIONS_SUCCESS,
      failure: FETCH_COLLECTIONS_FAILURE
    },
    CollectionsApi
  );

export const loadRounds = params =>
  fetchWithApi(
    {
      begin: FETCH_ROUNDS_BEGIN,
      success: FETCH_ROUNDS_SUCCESS,
      failure: FETCH_ROUNDS_FAILURE
    },
    ProvaoApi,
    params,
    'getRounds'
  );

export const loadTags = params =>
  fetchWithApi(
    {
      begin: FETCH_TAGS_BEGIN,
      success: FETCH_TAGS_SUCCESS,
      failure: FETCH_TAGS_FAILURE
    },
    TagsApi,
    params
  );
//*******************************

export const getRoundDays = () => loading(GET_ROUND_DAYS);

export const toggleLoadingDialog = (open, message) =>
  open ? load(SHOW_LOADING_DIALOG, message) : loading(HIDE_LOADING_DIALOG);
