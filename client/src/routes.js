import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import ProvaoOrder from "./pages/ProvaoOrder";
import Login from "./pages/Login";
import Vote from "./pages/ProvaoVote";
import Resume from "./pages/Resume";
import ProvaoOnline from"./pages/ProvaoOnline";
import VoteOnline from"./pages/VoteOnline";
import UserManagement from "./pages/UserManagement";
import ProvaoManagement from "./pages/ProvaoManagement";
import ProvaoConfigManagement from "./pages/ProvaoConfigManagement";
import EscolhaProvao from "./pages/EscolhaProvao";
import ProvaoAdd from "./pages/ProvaoAdd";
import ResultadoProvao from "./pages/ResultadoProvao";
import ResultadosProvoes from "./pages/ResultadosProvoes";


import User from "./services/User";
import Navbar from './components/Navbar/Navbar';
import {
  PROVAO_ORDER_ROUTE,
  PROVAO_ONLINE_ROUTE,
  PROVAO_ONLINEX1_ROUTE,
  PROVAO_ROUTE,
  PROVAO_RESUME_ROUTE,
  LOGIN_ROUTE,
  USER_MANAGEMENT_ROUTE,
  PROVAO_MANAGEMENT_ROUTE,
  PROVAO_CONFIG_ROUTE,
  PROVAO_CHOOSE,
  PROVAO_ADD_ROUTE,
  RESULTADO_PROVAO_ROUTE,
  RESULTADOS_PROVOES_ROUTE

} from "./consts";

export default props => (
  <Switch>
    <Route exact path={LOGIN_ROUTE} component={Login} />
    <PrivateRoutes props={props} />
  </Switch>
);

function PrivateRoutes(props) {
  const location = props.location;

  if (User.isAuthenticated()) {
    const user = new User();
    if (!user.canAcessThisRoute(location.pathname)) {
      return (
        <Redirect
          to={{
            pathname: user.getUserInitialRoute(),
            state: { from: location }
          }}
        />
      );
    }
    return (
      <>
        <Navbar />
        <Switch>
          <Route exact path={PROVAO_CHOOSE} component={() => <EscolhaProvao /> } />
          <Route exact path={PROVAO_ROUTE} component={() => <Vote />} />
          <Route exact path={PROVAO_RESUME_ROUTE} component={() => <Resume />} />
          <Route exact path={PROVAO_ONLINEX1_ROUTE} component={() => <ProvaoOnline />} />
          <Route exact path={PROVAO_ONLINE_ROUTE} component={() => <VoteOnline />} />
          <Route exact path={USER_MANAGEMENT_ROUTE} component={() => <UserManagement />} />
          <Route exact path={PROVAO_MANAGEMENT_ROUTE} component={() => <ProvaoManagement />} />
          <Route exact path={PROVAO_CONFIG_ROUTE} component={() => <ProvaoConfigManagement />} />
          <Route exact path={PROVAO_ORDER_ROUTE} component={() => <ProvaoOrder/>} />
          <Route exact path={PROVAO_ADD_ROUTE} component={() => <ProvaoAdd/>} />
          <Route exact path={RESULTADO_PROVAO_ROUTE} component={() => <ResultadoProvao/>} />
          <Route exact path={RESULTADOS_PROVOES_ROUTE} component={() => <ResultadosProvoes/>} />


        </Switch>
      </>
    );
  }

  return (
    <Redirect
      to={{
        pathname: LOGIN_ROUTE,
        state: { from: location }
      }}
    />
  );
}
