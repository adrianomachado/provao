import React from "react";
import PropTypes from "prop-types";

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from "@material-ui/core/TextField";
import makeStyles from "@material-ui/core/styles/makeStyles";


const useStyles = makeStyles(theme => ({
  textfield: {
    width: titleWidth => titleWidth,
  }
}));

function TextFieldEstilo({className, onChange, value, name, titleWidth, ...props}) {

  const classes = useStyles(titleWidth);

  return (
    <Grid container justify={'space-between'} alignItems={'center'} spacing={1} className={className}>
      <Grid item>
        <Typography variant="body2" color="textSecondary" component="p">
          { name }:
        </Typography>
      </Grid>
      <Grid item xs>
        <TextField
          className={classes.textfield}
          value={value}
          {...props}
          fullWidth
          onChange={onChange}
        />
      </Grid>
    </Grid>
  );
}

TextFieldEstilo.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  titleWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default TextFieldEstilo;
