import React, {Component} from "react";
import PropTypes from "prop-types";

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import withStyles from "@material-ui/core/styles/withStyles";


const styles = theme => ({
  select: {
    minWidth: 100,
  }
});

class FieldSelect extends Component {

  renderMenuItems() {
    const { options } = this.props;
    return options.map(({value, label}) => (<MenuItem value={value} key={value}>{label}</MenuItem>));
  }

  render() {
    const { classes, onChange, value, name, field } = this.props;
    return (
      <Grid container justify={'space-between'} alignItems={'baseline'} spacing={1} style={this.props.style}>
        <Grid item>
          <Typography variant="body2" color="textSecondary" component="p">
            { name }:
          </Typography>
        </Grid>
        <Grid item>
          <Select
            disabled={this.props.disabled}
            classes={{select: classes.select}}
            value={value}
            onChange={e => onChange(e.target.value, field)}
          >
            {this.renderMenuItems()}
          </Select>
        </Grid>

      </Grid>
    );
  }
}

FieldSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  field: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    })
  ).isRequired,
};

export default withStyles(styles)(FieldSelect);
