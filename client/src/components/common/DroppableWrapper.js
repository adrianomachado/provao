import React from 'react';
import {Droppable} from "react-beautiful-dnd";

function DroppableWrapper(props) {

  return (
    <Droppable {...props}>
      {(provided) => (
        <div
          {...provided.droppableProps}
          {...provided.droppablePlaceholder}
          ref={provided.innerRef}
          style={{minHeight: props.minHeight}}
        >
          { props.children }
          { provided.placeholder }
        </div>
      )}
    </Droppable>
  );
}

export default DroppableWrapper;
