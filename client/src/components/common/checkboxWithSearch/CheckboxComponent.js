import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { debounce } from "debounce";

import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";

import CustomizedInput from "../CustomizedInput";
import CheckBoxList from "./CheckBoxList";
import LoadingCircle from "../LoadingCircle";

const styles = theme => {};

class CheckBoxComponent extends Component {
  state = {
    checkboxes: [],
    loading: true,
    searchField: ""
  };

  componentDidMount() {
    const { requester, limit = 5 } = this.props;
    const searchDebounced = debounce(search => this.request(search), 400);
    this.setState({
      searchAction: searchDebounced,
      limit
    });
    requester
      .get({ limit })
      .then(data =>
        this.setState({
          checkboxes: this.transformToCheckbox(data),
          loading: false
        })
      );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      this.setState({ checkboxes: this.updateCheckbox(this.state.checkboxes) });
    }
  }

  updateCheckbox(dataList) {
    return dataList.map(data => ({
      ...data,
      checked: this.shouldBeChecked(data["value"])
    }));
  }

  transformToCheckbox(dataList) {
    const { field, value } = this.props;
    return dataList.map(data => ({
      label: data[field],
      value: data[value],
      checked: this.shouldBeChecked(data[value])
    }));
  }

  shouldBeChecked(value) {
    const { filters } = this.props;
    return filters.findIndex(e => e.value === value) !== -1;
  }

  request(search) {
    const { requester } = this.props;
    const { limit } = this.state;
    this.setState({ loading: true, checkboxes: [] });
    requester
      .get({ desc: search, limit })
      .then(data =>
        this.setState({
          checkboxes: this.transformToCheckbox(data),
          loading: false
        })
      );
  }

  renderChips() {
    const { filters } = this.props;
    return filters.map((checkbox, index) => this.renderChip(checkbox, index));
  }

  renderChip({ label, value, checked }, index) {
    if (checked) {
      return (
        <Grid item key={value}>
          <Chip
            label={label}
            color={"primary"}
            variant={"outlined"}
            onDelete={() => this.props.onDeleteChip(index)}
          />
        </Grid>
      );
    }
  }

  handleSearch(value) {
    const { searchAction } = this.state;
    this.setState({ ...this.state, searchField: value });
    searchAction(value);
  }

  handleChangeCheckbox(item) {
    const { filters } = this.props;
    const { checkboxes } = this.state;
    const filtersCopy = Array.from(filters);
    if (item.checked) {
      filtersCopy.push(item);
    } else {
      filtersCopy.splice(filtersCopy.findIndex(e => e.value === item.value), 1);
    }

    const index = checkboxes.findIndex(e => e.value === item.value);
    checkboxes[index] = item;
    this.setState({ ...this.state, checkboxes });
    this.props.onChange(filtersCopy);
  }

  handleMoreItems() {
    const { requester } = this.props;
    const { searchField, limit } = this.state;
    const incrementedLimit = limit + 5;
    requester.get({ desc: searchField, limit: incrementedLimit }).then(data =>
      this.setState({
        checkboxes: this.transformToCheckbox(data),
        limit: incrementedLimit
      })
    );
  }

  render() {
    const { placeholder } = this.props;
    const { searchField, checkboxes, loading } = this.state;

    return (
      <Grid container item alignItems={"center"} spacing={1}>
        <Grid container item spacing={1}>
          {this.renderChips()}
        </Grid>

        <Grid container item>
          <CustomizedInput
            placeholder={placeholder}
            value={searchField}
            onChange={value => this.handleSearch(value)}
          />
        </Grid>

        <Grid container item>
          <LoadingCircle loading={loading} />
          <CheckBoxList
            handleChange={item => this.handleChangeCheckbox(item)}
            items={checkboxes}
          />
        </Grid>

        <Grid container item>
          <Button onClick={() => this.handleMoreItems()} color="primary">
            +
          </Button>
        </Grid>
      </Grid>
    );
  }
}

CheckBoxComponent.propTypes = {
  field: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  requester: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  filters: PropTypes.array
};

const wrappedComponent = withStyles(styles)(withRouter(CheckBoxComponent));

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrappedComponent);
