import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {Typography} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
}));

export default function CheckBoxList(props) {
  const classes = useStyles();
  const { items } = props;

  const handleChange = (item, checked) => {
    props.handleChange({...item, checked});
  };

  return (
    <div className={classes.root}>
      <FormControl component="fieldset">
        <FormGroup>
          {items.map((item, index) => (
            <FormControlLabel
              key={index}
              control={
                <Checkbox
                  checked={item.checked}
                  onChange={event => handleChange(item, event.target.checked)}
                  value={item.value}
                />
              }
              label={<Typography variant={'caption'} color={'textSecondary'}>
                {item.label}
              </Typography>}
            />
          ))}
        </FormGroup>
      </FormControl>
    </div>
  );
}
