import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { CircularProgress } from "@material-ui/core";

class AlertDialog extends Component {
  render() {
    return (
      <Dialog open={this.props.open} onClose={() => this.props.onCancel()}>
        <DialogTitle>{this.props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{this.props.children}</DialogContentText>
        </DialogContent>
        <DialogActions>
          {this.props.loading ? (
            <CircularProgress style={{marginRight:5, marginBottom:5}} />
          ) : (
            <Fragment>
              <Button onClick={() => this.props.onCancel()} color="primary">
                Cancelar
              </Button>
              <Button
                onClick={() => this.props.onConfirm()}
                color="primary"
                autoFocus
              >
                Confirmar
              </Button>
            </Fragment>
          )}
        </DialogActions>
      </Dialog>
    );
  }
}

AlertDialog.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string
};

export default withRouter(AlertDialog);
