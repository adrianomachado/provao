import React from "react";
import PropTypes from "prop-types";

import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import { withStyles } from "@material-ui/core/styles";

import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import SwipeableCarrousel from "../SwipeableCarrousel";

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    padding: theme.spacing(2),
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
    zIndex: 1000
  }
});

const IconButtonClose = withStyles(styles)(({ onClose, classes }) => {
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={onClose}
      >
        <CloseIcon />
      </IconButton>
    </MuiDialogTitle>
  );
});

export default function PhotoDialog({ onClose, open, photos }) {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <Dialog fullScreen={fullScreen} open={open} onClose={onClose}>
      <IconButtonClose onClose={onClose} />
      <SwipeableCarrousel photos={photos} stepper={true} />
    </Dialog>
  );
}

PhotoDialog.propTypes = {
  photos: PropTypes.array.isRequired
};
