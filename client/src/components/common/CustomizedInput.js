import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import { withStyles } from "@material-ui/styles";

import SearchIcon from "@material-ui/icons/Search";
import FilterList from "@material-ui/icons/FilterList";

const styles = theme => ({
  root: {
    border: "1px solid #e9e9e9"
  },
  inputBase: {
    paddingLeft: theme.spacing(2),
    minWidth: "150px"
  }
});

class CustomizedInput extends Component {
  renderPreIcon() {
    const { onClickIcon, iconButton } = this.props;
    if (iconButton) {
      return (
        <IconButton color="primary" onClick={() => onClickIcon()}>
          <FilterList />
        </IconButton>
      );
    }
    return null;
  }

  renderPosIcon() {
    return (
      <IconButton size={'small'}>
        <SearchIcon />
      </IconButton>
    );
  }

  render() {
    const { classes, placeholder } = this.props;

    return (
      <Grid container className={classes.root}>
        {this.renderPreIcon()}

        <InputBase
          placeholder={placeholder}
          value={this.props.value}
          className={classes.inputBase}
          onChange={e => this.props.onChange(e.target.value)}
        />
        {this.renderPosIcon()}
      </Grid>
    );
  }
}

export default withStyles(styles)(withRouter(CustomizedInput));
