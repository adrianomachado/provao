import React from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";

import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import MobileStepper from "@material-ui/core/MobileStepper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
    width: 280
  },
  avatar: {
    padding: theme.spacing(2),
    borderRadius: 0,
    [theme.breakpoints.down("sm")]: {
      width: "90vw",
      height: "90vh"
    },
    [theme.breakpoints.up("md")]: {
      width: "280px",
      height: "90%"
    }
  },
  stepper: {
    maxWidth: "100%",
    flexGrow: 1
  },
  sliderBtn: {
    padding: theme.spacing(2)
  }
}));

export default function SwipeableCarrousel({
  photos,
  Component,
  stepper = false
}) {
  const [activeStep, setActiveStep] = React.useState(0);

  const classes = useStyles();

  const renderImg = img => {
    if (Component) {
      return Component(img);
    }
    return (
      <Avatar className={classes.avatar} key={img} src={img} title={img} />
    );
  };

  const renderStepper = () => {
    if (stepper) {
      return (
        <MobileStepper
          variant="progress"
          steps={photos.length}
          className={classes.stepper}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button
              onClick={() => setActiveStep(activeStep + 1)}
              disabled={activeStep + 1 >= photos.length}
              className={classes.sliderBtn}
            >
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              onClick={() => setActiveStep(activeStep - 1)}
              disabled={activeStep === 0}
              className={classes.sliderBtn}
            >
              <KeyboardArrowLeft />
            </Button>
          }
        />
      );
    }
    return null;
  };

  return (
    <>
      <SwipeableViews
        index={activeStep}
        enableMouseEvents
        onChangeIndex={index => setActiveStep(index)}
      >
        {photos.map(img => renderImg(img))}
      </SwipeableViews>
      {renderStepper()}
    </>
  );
}

SwipeableCarrousel.propTypes = {
  photos: PropTypes.array.isRequired,
  Component: PropTypes.func,
  stepper: PropTypes.bool
};
