import React, { Component } from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = theme => ({
  root: {
    margin: theme.spacing(2)
  }
});

class LoadingCircle extends Component {
  render() {
    const { loading, classes, noPadding, ...others } = this.props;

    return loading ? (
      <CircularProgress
        classes={noPadding ? null : classes.root}
        {...others}
      />
    ) : null;
  }
}

LoadingCircle.propTypes = {
  loading: PropTypes.bool.isRequired,
  noPadding: PropTypes.bool
};

const wrappedComponent = withStyles(styles)(LoadingCircle);

export default wrappedComponent;
