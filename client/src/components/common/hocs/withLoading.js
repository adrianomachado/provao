import React from 'react';

import LoadingCircle from '../LoadingCircle';

export default function withLoading(WrappedComponent) {
  return class extends React.Component {
    render() {
      const { loading } = this.props;
      return(
        loading ? <LoadingCircle loading={true} /> : <WrappedComponent  />
      );
    }
  };
}
