import React from 'react';
import AlertDialog from "../dialogs/AlertDialog";


export default function withAlert(WrappedComponent, title) {
  return class extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        isOpen: false,
        callback: null,
      };
    }

    handleClick = func => {
      this.setState({isOpen: true, callback: func});
    };

    handleConfirm = () => {
      this.setState({isOpen: false});
      this.state.callback();
    };

    render() {
      return(
        <>
          <WrappedComponent alert={{...this.state, onClick: this.handleClick}} />
          <AlertDialog
            open={this.state.isOpen}
            title={title}
            onCancel={() => this.setState({ isOpen: false })}
            onConfirm={this.handleConfirm}
          >
          </AlertDialog>
        </>
      );
    }

  };
}
