import React from "react";
import PropTypes from "prop-types";

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Switch from "@material-ui/core/Switch";

function Switcher({title, ...props}) {


  return (
    <Typography component={'div'} variant={'caption'}>
      <Grid component="label" container alignItems="center" spacing={1}>
        <Grid item>{title.left}</Grid>
        <Grid item>
          <Switch
            {...props}
          />
        </Grid>
        <Grid item>{title.right}</Grid>
      </Grid>
    </Typography>
  );
}

Switcher.propTypes = {
  title: PropTypes.shape({
    left: PropTypes.string,
    right: PropTypes.string,
  })
};

export default Switcher;
