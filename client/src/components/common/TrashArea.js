import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

import Delete from '@material-ui/icons/Delete';
import DroppableWrapper from './DroppableWrapper';


class TrashArea extends Component {

  render() {
    return (
      <DroppableWrapper droppableId={'trash'} direction="horizontal" minHeight={'50px'}>
        <Delete color={'error'}/>
      </DroppableWrapper>
    );
  }
}

export default withRouter(TrashArea);
