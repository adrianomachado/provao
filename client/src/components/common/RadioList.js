import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

import Radio from '@material-ui/core/Radio';
import { Typography } from "@material-ui/core";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

class RadioList extends Component {

  renderRadioLabels() {
    const { padding, disabled = false, data } = this.props;
    const spacing = {
      padding: padding === 'dense' ? '8px' : ''
    };
    return data.map(radio =>
      <FormControlLabel
        disabled={disabled}
        value={radio.value}
        key={radio.value}
        control={<Radio style={spacing} value={radio.value} />}
        label={
          <Typography variant={'caption'} color={'textSecondary'}>
            {radio.label}
          </Typography>

        }
      />
      )
  }

  render() {
    const { onChange, selectedRadio, row = false } = this.props;
    return (
      <FormControl >
        <RadioGroup
          value={selectedRadio}
          onChange={(e)=> onChange(e.target.value)}
          row={row}
        >
          { this.renderRadioLabels() }
        </RadioGroup>

      </FormControl>
    );
  }
}

export default withRouter(RadioList);
