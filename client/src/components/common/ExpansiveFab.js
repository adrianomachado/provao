import React, {useState} from 'react';
import PropTypes from "prop-types";

import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/icons/Menu';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';


const useStyles = makeStyles(theme => ({
  button: {
    position: "fixed",
    zIndex: 999,
    top: '2%',
    left: '2%',
    opacity: 0.5,
    "&:hover": {
      opacity: 1
    }
  },
  menuRef: {
    display: 'none',
  },
  menuRefShow: {
    position: 'relative',
    display: 'flex',
    top: theme.spacing(2)
  },
}));

export default function ExpansiveFab({buttons, onClick}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClick = (e, button) => {
    setOpen(!open);
    if (button){
      onClick(button);
    }
  };

  const FabStyled = button => {
    return (
      <Fab
        color={'secondary'}
        size={'small'}
        onClick={(e) => handleClick(e, button)}
      >
        <Icon>
          {button.name}
        </Icon>
      </Fab>
    );
  };

  const renderIcons = buttons => {
    return buttons.map(button =>
      <Grid item key={button.name}>
        {FabStyled(button)}
      </Grid>
    )
  };

  return (
    <div className={classes.button}>
    <Fab
      color={'secondary'}
      size={'small'}
      onClick={e => handleClick(e)}
    >
      <Menu />
    </Fab>
      <Grow in={open} mountOnEnter unmountOnExit>
      <Grid
        container
        className={open ? classes.menuRefShow : classes.menuRef}
        direction={'column'}
        alignContent={'center'}
        spacing={2}
      >

        {renderIcons(buttons)}

      </Grid>
      </Grow >
    </div>
  );
}

ExpansiveFab.propTypes = {
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  onClick: PropTypes.func.isRequired
};
