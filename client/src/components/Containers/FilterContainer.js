import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { withStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";

import { drawerContainerWidth } from "../../consts";
import CheckboxComponent from "../common/checkboxWithSearch/CheckboxComponent";

import LinesApi from "../../services/apis/LinesApi";
import GroupsApi from "../../services/apis/GroupsApi";
import PrintsApi from "../../services/apis/PrintsApi";
import PlannersApi from "../../services/apis/PlannersApi";


const styles = theme => ({
  root: {
    width: drawerContainerWidth,
    paddingTop: theme.spacing(5)
  }
});

class FilterContainer extends Component {
  constructor(props) {
    const { filtersSaved } = props;
    super(props);
    this.state = {
      lines: this.mapSavedFilter(filtersSaved, "lines"),
      groups: this.mapSavedFilter(filtersSaved, "groups"),
      prints: this.mapSavedFilter(filtersSaved, "prints"),
      planners: this.mapSavedFilter(filtersSaved, "planners")

    };
  }

  mapSavedFilter(obj, field) {
    if (obj.hasOwnProperty(field)) {
      return obj[field];
    }
    return [];
  }

  handleFilter(items, field) {
    this.setState({ [field]: items });
  }

  onDelete() {
    this.setState({ lines: [], groups: [], prints: [],planners:[] });
  }

  renderRemoveFilterBtn() {
    const { lines, groups, prints, planners } = this.state;
    if (
      this.isThereAnyElement(lines) ||
      this.isThereAnyElement(groups) ||
      this.isThereAnyElement(prints) ||
      this.isThereAnyElement(planners)
    ) {
      return (
        <Button onClick={() => this.onDelete()} color="secondary">
          Remover Filtros
        </Button>
      );
    }
    return null;
  }

  isThereAnyElement(array) {
    return array.length > 0;
  }

  handleDeleteChip(field, index) {
    let fieldCopy = this.state[field];
    fieldCopy.splice(index, 1);
    this.setState({[field]: fieldCopy});
  }

  render() {
    const { classes, onSave } = this.props;
    const { lines, groups, prints, planners } = this.state;

    return (
      <Container className={classes.root}>
        <Grid container direction={"column"} spacing={2}>
          <Grid container item justify={"flex-end"}>
            <Grid item>{this.renderRemoveFilterBtn()}</Grid>
            <Grid item>
              <Button
                onClick={() => onSave({ lines, groups, prints, planners })}
                color="primary"
              >
                Salvar Filtros
              </Button>
            </Grid>
          </Grid>

          <Grid item>
            <Typography variant={"overline"}>Linha</Typography>
            <CheckboxComponent
              placeholder={"Linha..."}
              filters={lines}
              requester={LinesApi}
              field={"linha"}
              value={"id_linha"}
              onDeleteChip={index => this.handleDeleteChip('lines', index)}
              onChange={items => this.handleFilter(items, "lines")}
            />
          </Grid>

          <Divider />

          <Grid item>
            <Typography variant={"overline"}>Grupo</Typography>
            <CheckboxComponent
              placeholder={"Grupo..."}
              filters={groups}
              requester={GroupsApi}
              field={"grupo_produto"}
              value={"id_grupo_produto"}
              onDeleteChip={index => this.handleDeleteChip('groups', index)}
              onChange={items => this.handleFilter(items, "groups")}
            />
          </Grid>

          <Divider />

          <Grid item>
            <Typography variant={"overline"}>Estampa</Typography>
            <CheckboxComponent
              placeholder={"Estampa..."}
              filters={prints}
              requester={PrintsApi}
              field={"estampa_cor"}
              value={"id_estampa_cor"}
              onDeleteChip={index => this.handleDeleteChip('prints', index)}
              onChange={items => this.handleFilter(items, "prints")}
            />
          </Grid>
          <Grid item>
            <Typography variant={"overline"}>Planner</Typography>
            <CheckboxComponent
              placeholder={"Planner..."}
              filters={planners}
              requester={PlannersApi}
              field={"planner"}
              value={"planner"}
              onDeleteChip={index => this.handleDeleteChip('planners', index)}
              onChange={items => this.handleFilter(items, "planners")}
            />
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const wrappedComponent = withStyles(styles)(withRouter(FilterContainer));

export default wrappedComponent;
