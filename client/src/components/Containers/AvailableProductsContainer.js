import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { debounce } from "debounce";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Drawer from "@material-ui/core/Drawer";
import Chip from "@material-ui/core/Chip";
import Container from "@material-ui/core/Container";
import { withStyles } from "@material-ui/styles";

import ProductCard from "../ProductCard/ProductCard";
import { drawerContainerWidth, DROPPABLE_ID_AVAILABLE } from "../../consts";
import DroppableWrapper from "../common/DroppableWrapper";
import CustomizedInput from "../common/CustomizedInput";
import FilterContainer from "./FilterContainer";
import ProductsApi from "../../services/apis/ProductsApi";

const styles = theme => ({
  root: {
    width: drawerContainerWidth,
    paddingTop: theme.spacing(5)
  }
});

class OrderingContainer extends Component {
  state = {
    isDrawerOpen: false,
    search: "",
    searchAction: null,
    filters: {},
    filtersJoined: []
  };

  componentDidMount() {
    const searchDebounced = debounce(
      (search, filters) => this.props.searchProducts(search, filters),
      1000
    );
    this.setState({ searchAction: searchDebounced });
  }

  toggleDrawer(state) {
    this.setState({ isDrawerOpen: state });
  }

  handleSearch(search) {
    const { searchAction, filters } = this.state;
    this.setState({ search });
    searchAction(search, this.mountFilters(filters));
  }

  mountFilters(filters) {
    const { lines, groups, prints,planners } = filters;
    return {
      linha: this.mapValues(lines),
      grupo: this.mapValues(groups),
      estampa: this.mapValues(prints),
      planners:this.mapValues(planners)
    };
  }

  mapValues(arr) {
    if (arr) {
      return arr.map(e => e.value);
    }
    return [];
  }

  addFilter(arr, filter) {
    if (filter) {
      return arr.concat(filter);
    }
    return arr;
  }

  joinFilters(lines, groups, prints,planners) {
    let filters = [];
    filters = this.addFilter(filters, lines);
    filters = this.addFilter(filters, groups);
    filters = this.addFilter(filters, prints);
    filters = this.addFilter(filters, planners);
    return filters;
  }

  handleFilters(filters) {
    const { lines, groups, prints, planners } = filters;
    const { search } = this.state;
    const filtersJoined = this.joinFilters(lines, groups, prints,planners);
    this.setState({ filters, filtersJoined });
    this.props.searchProducts(search, this.mountFilters(filters));
    this.toggleDrawer(false);
  }

  renderChip(data) {
    if(data){
      return (
        <Chip
          label={data.toUpperCase()}
          color={"primary"}
          variant={"outlined"}
          key={data}
        />
      );
    }
  }

  render() {
    const { classes, collection, tag, items } = this.props;
    const { filters, filtersJoined } = this.state;

    return (
      <Container>
        <Drawer
          open={this.state.isDrawerOpen}
          onClose={() => this.toggleDrawer(false)}
          anchor={"right"}
        >
          <FilterContainer
            onSave={filters => this.handleFilters(filters)}
            filtersSaved={{ ...filters }}
          />
        </Drawer>

        <Grid
          container
          direction={"column"}
          alignItems={"center"}
          justify={"center"}
          spacing={3}
          className={classes.root}
        >
          <Grid item>
            <Typography variant="h5">Lista de Produtos</Typography>
          </Grid>

          <Grid container item alignItems={"center"} justify={"center"}>
            <CustomizedInput
              placeholder={"Referência..."}
              onClickIcon={() => this.toggleDrawer(true)}
              value={this.state.value}
              onChange={value => this.handleSearch(value)}
              iconButton={true}
            />
          </Grid>

          <Grid item>
            <Grid container spacing={1}>
              <Grid item>{this.renderChip(collection.colecao)}</Grid>
              <Grid item>{this.renderChip(tag.nome)}</Grid>
              <Grid container item spacing={1}>
                {filtersJoined.map((filter, index) => (
                  <Grid item key={index}>
                    {this.renderChip(filter.label)}
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>

          <Grid container item alignItems={"center"} justify={"center"}>
            <DroppableWrapper
              droppableId={DROPPABLE_ID_AVAILABLE}
              direction={"vertical"}
            >
              <Grid container item spacing={1}>
                {items.map((item, index) => (
                  <Grid item key={item.id_produto_estilo}>
                    <ProductCard
                      content={item}
                      img={ProductsApi.getImageURL(item)}
                      index={index}
                    />
                  </Grid>
                ))}
              </Grid>
            </DroppableWrapper>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const wrappedComponent = withStyles(styles)(withRouter(OrderingContainer));

export default wrappedComponent;
