import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import SelectUi from "@material-ui/core/Select";
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";

import { drawerContainerWidth } from "../../consts";
import Round from "../Round/Round";
import AlertDialog from "../common/dialogs/AlertDialog";
import RoundTrialApi from "../../services/apis/RoundTrialApi";
import { toggleLoadingDialog, toggleRoundDay } from "../../actions";
import AxiosWrapper from "../../services/AxiosWrapper";
import { BASE_URL,PROVAO_ROUTE, RESULTADOS_PROVOES_ROUTE} from "../../consts";
import { limitString } from "../../utils";
import {withSnackbar} from "notistack";


const styles = theme => ({
  root: {
    padding: theme.spacing(3)
  },
  paper: {
    width: `calc(100% - ${drawerContainerWidth * 1.25}px)`,
    padding: theme.spacing(4)
  }
});

function Select({ items, ...props }) {
  return (
    <SelectUi {...props}>
      {items.map(item => (
        <MenuItem key={item.label} value={item.value}>
          {item.label}
        </MenuItem>
      ))}
    </SelectUi>
  );
}

class OrderingContainer extends Component {
  state = {
    isModalConfirmOpen: false,
    loadingModal: false,
    confirmValue: null,
    isModalInitRoundOpen: false
  };

  renderRoundParams(title, content) {
    return (
      <Fragment>
        <Typography variant={"caption"} color={"textSecondary"} gutterBottom>
          {title.toUpperCase()}
        </Typography>
        <Typography variant={"h6"} color={"textPrimary"}>
          {content}
        </Typography>
      </Fragment>
    );
  }
  getProvaoId() {
    const { match } = this.props;
    return match.params.id_provao;
  }
  handleModal() {
    this.setState({ isModalConfigRoundOpen: false });
  }

  handleConfirmDialog() {
    this.setState({ isModalConfirmOpen: false, confirmValue: null });
    this.deleteRound();
  }

  handleSelectRound() {
    this.handleModal();
  }

  deleteRound() {
    
    const { confirmValue } = this.state;
    const { toggleLoadingDialog , enqueueSnackbar } = this.props;
    this.setState({ loadingModal: true });
    toggleLoadingDialog(true, "Verificando se é possível apagar a rodada...");
    RoundTrialApi.delete(confirmValue.round.id_rodada)
      .then(() => this.props.onCloseRound(confirmValue.index))
      .catch(error => {
        console.log(error.message)
        if(error.message.includes(500)) {
          enqueueSnackbar("Não é possível deletar uma rodada que ja foi votada", { variant: "warning" })
        } else { 
          alert("Algum problema não identificado ocorreu durante a tentativa de deletar a rodada ")
        } 
      })
      .finally(() => toggleLoadingDialog(false));
  }
  initRound = async round => {
    const { history } = this.props;
    this.setState({ loading: true });
    AxiosWrapper.get(`/rodadas_provao/${round}/iniciar`)
      .then(data => {
        this.setState({ isModalInitRoundOpen: false });
        this.setState({ loading: false });
        const url = PROVAO_ROUTE.replace(':id_provao', this.getProvaoId());
        return history.push(url);
      })
      .catch(data => {
        alert("A rodada já está acontecendo");
        this.setState({ loading: false });

        this.setState({ isModalInitRoundOpen: false });
      });
  };

  handleChangeProvao(e, obj) {
    const {toggleRoundDay, loadRounds } = this.props;
    toggleRoundDay(obj.props);
    loadRounds(obj.props.value);
  }

  navigateToResults() {
    let url = RESULTADOS_PROVOES_ROUTE.replace(':id_provao', this.getProvaoId());
    url += `?dia=${this.props.roundDaySelected.value}`
   return  this.props.history.push(url)
  }

  render() {
    const {
      classes,
      roundDaySelected,
      roundDays
    } = this.props;
    const { tag, collection, name } = this.props;

    return (
      <Grid
        container
        alignItems={"center"}
        spacing={2}
        className={classes.root}
      >

        <AlertDialog
          open={this.state.isModalConfirmOpen}
          title={`Deseja deletar a rodada ${
            this.state.confirmValue ? this.state.confirmValue.index + 1 : null
          }?`}
          onCancel={() => this.setState({ isModalConfirmOpen: false })}
          onConfirm={() => this.handleConfirmDialog()}
        >
          {"Uma vez deletada, não será possível resgatar a rodada."}
        </AlertDialog>
        <AlertDialog
          loading={this.state.loading}
          open={this.state.isModalInitRoundOpen}
          title={`Deseja iniciar a rodada ${
            this.state.confirmValueInitRound
              ? this.state.confirmValueInitRound.index + 1
              : null
          }?`}
          onCancel={() => this.setState({ isModalInitRoundOpen: false })}
          onConfirm={() =>
            this.initRound(this.state.confirmValueInitRound.id_rodada)
          }
        >
          {/* {"Uma vez deletada, não será possível resgatar a rodada."} */}
        </AlertDialog>

        <Paper className={classes.paper}>
          <Grid container item sm={12} spacing={4}>
            <Grid container item>
              <Grid item xs={8}>
                <Grid container spacing={6}>
                  <Grid item>
                    {this.renderRoundParams(
                      "dia",
                      <Select
                        items={roundDays.roundDays}
                        value={roundDaySelected.value}
                        onChange={(e, obj) => this.handleChangeProvao(e, obj)}
                      />
                    )}
                  </Grid>

                  <Grid item>
                    {this.renderRoundParams("Provão", limitString(name, 15))}
                  </Grid>

                  <Grid item>
                    {this.renderRoundParams("marca", limitString(tag.nome, 12))}
                  </Grid>

                  <Grid item>
                    {this.renderRoundParams(
                      "coleção",
                      collection.colecao
                    )}
                  </Grid>
                </Grid>
              </Grid>

              <Grid container item xs={4} alignItems={"flex-end"}>
                <Grid container spacing={3} justify={"flex-end"}>
                <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => this.navigateToResults()}
                    >
                      Ver resultados
                    </Button>
                    
                  </Grid>
                  <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => this.props.addRound()}
                    >
                      + Rodada
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Grid container item>
              <Typography variant="h5" gutterBottom>
                Rodadas
              </Typography>
            </Grid>
          </Grid>

          <Grid container item alignItems={"center"}>
            {this.props.rounds.map((round, index) => (
              <Round
              roundActive={round.fl_ativo === 1}
              id_rodada={round.id_rodada}
                round={
                  round.hasOwnProperty("rodada_produtos")
                    ? round.rodada_produtos
                    : round
                }
                index={index}
                key={index}
                onClose={index => {
                  this.setState({
                    isModalConfirmOpen: true,
                    confirmValue: { round, index }
                  });
                }}
                initRound={index => {
                  this.setState({
                    isModalInitRoundOpen: true,
                    confirmValueInitRound: { id_rodada: round.id_rodada, index }
                  });
                }}
              />
            ))}
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

const wrappedComponent = withSnackbar(withStyles(styles)(withRouter(OrderingContainer)));

const mapStateToProps = state => ({
  roundDays: state.roundDays.list,
  roundDaySelected: state.roundDays.selected
});

const mapDispatchToProps = dispatch => ({
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message)),
  toggleRoundDay: day => dispatch(toggleRoundDay(day)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrappedComponent);
