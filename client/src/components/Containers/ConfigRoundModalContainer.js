import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

import RoundTrialApi from "../../services/apis/RoundTrialApi";

import {
  toggleBrand,
  toggleCollection,
  toggleRoundDay,
  loadRounds,
  toggleLoadingDialog
} from "../../actions";

const styles = theme => ({
  root: {
    padding: theme.spacing(3)
  },
  formControl: {
    minWidth: 120
  }
});

class ConfigRoundModalContainer extends Component {
  renderOptions(list) {
    return list.map(item => (
      <MenuItem key={item.value} value={item.value}>
        {item.label}
      </MenuItem>
    ));
  }

  mapToOptions(label, value, list) {
    return list.map(item => ({ label: item[label], value: item[value] }));
  }

  handleSelect() {
    const {
      collectionSelected,
      roundDaySelected,
      brandSelected,
      toggleLoadingDialog,
      handleSelectRound,
      enqueueSnackbar
    } = this.props;

    if (
      !RoundTrialApi.isRoundValid(
        roundDaySelected.value,
        brandSelected.value,
        collectionSelected.value
      )
    ) {
      enqueueSnackbar(
        "Não foi possível selecionar as rodadas. É preciso selecionar todos os campos.",
        { variant: "error" }
      );
      return;
    }
    handleSelectRound();
    toggleLoadingDialog(true, "Puxando rodada existente");
    this.props
      .loadRounds({
        colecao: collectionSelected.label,
        dia: roundDaySelected.value,
        marca: brandSelected.value
      })
      .catch(() =>
        enqueueSnackbar("Falha ao puxar as rodadas.", { variant: "error" })
      )
      .finally(() => toggleLoadingDialog(false));
      localStorage.setItem("params", JSON.stringify({collectionSelected,roundDaySelected,brandSelected}))
  }

  render() {
    const {
      classes,
      brandSelected,
      collectionSelected,
      roundDaySelected
    } = this.props;

    return (
      <Container className={classes.root}>
        <Grid container spacing={3}>
          <Grid container item justify={"center"}>
            <Typography variant={"h5"}>Configuração da rodada</Typography>
            <Typography variant={"caption"} color={"textSecondary"}>
              Aqui onde serão setadas as informações para criar ou puxar uma
              rodada existente
            </Typography>
          </Grid>

          <Grid item container>
            <Grid container justify={"space-around"}>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel>Dia</InputLabel>
                  <Select
                    value={roundDaySelected.value}
                    onChange={(e, obj) => this.props.toggleRoundDay(obj.props)}
                  >
                    {this.renderOptions(this.props.roundDays.roundDays)}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel>Marca</InputLabel>
                  <Select
                    value={brandSelected.value}
                    onChange={(e, obj) => this.props.toggleBrand(obj.props)}
                  >
                    {this.renderOptions(
                      this.mapToOptions(
                        "marca",
                        "id_marca",
                        this.props.brands.brands
                      )
                    )}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel>Coleção</InputLabel>
                  <Select
                    value={collectionSelected.value}
                    onChange={(e, obj) =>
                      this.props.toggleCollection(obj.props)
                    }
                  >
                    {this.renderOptions(
                      this.mapToOptions(
                        "colecao",
                        "id_colecao",
                        this.props.collections.collections
                      )
                    )}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>

          <Grid container item justify={"flex-end"}>
            <Button onClick={() => this.handleSelect()} color="primary">
              Selecionar rodada
            </Button>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withSnackbar(
  withStyles(styles)(withRouter(ConfigRoundModalContainer))
);

const mapStateToProps = state => ({
  brands: state.brands.list,
  brandSelected: state.brands.selected,
  collections: state.collections.list,
  collectionSelected: state.collections.selected,
  roundDays: state.roundDays.list,
  roundDaySelected: state.roundDays.selected
});

const mapDispatchToProps = dispatch => ({
  toggleBrand: brand => dispatch(toggleBrand(brand)),
  toggleCollection: collection => dispatch(toggleCollection(collection)),
  toggleRoundDay: day => dispatch(toggleRoundDay(day)),
  loadRounds: params => dispatch(loadRounds(params)),
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapperComponent);
