import React, {  useState } from "react";
import { withRouter } from "react-router-dom";

import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import makeStyles from "@material-ui/core/styles/makeStyles";

import ListIcon from '../common/ListIcon';
import User from '../../services/User';
import withAlert from '../common/hocs/withAlert';

import {
  PROVAO_ONLINE_ROUTE,
  PROVAO_CHOOSE,
  PROVAO_MANAGEMENT_ROUTE,
  LOGIN_ROUTE,
  USER_MANAGEMENT_ROUTE
} from '../../consts';
import { matchRoute } from '../../utils';
import ExpansiveFab from "../common/ExpansiveFab";

const drawerWidth = 44;
const TITLE_ALERT = 'Você deseja sair?';


const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerExpanded: {
    width: drawerWidth * 4,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  button: {
    position: "fixed",
    [theme.breakpoints.down('sm')]: {
      top: '2%',
      left: '2%',
    },
    [theme.breakpoints.up('md')]: {
      top: '1%',
      left: '1%',
    },
    opacity: 0.5,
    "&:hover": {
      opacity: 1
    }
  }
}));

function Navbar(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const user = new User();

  const mainMenu = {
    name: 'menu',
    action: () => setOpen(!open)
  };

  const menuIcons = [
    {
      name: 'vertical_split',
      text: 'Gestão de Provão',
      action: () => redirect(PROVAO_MANAGEMENT_ROUTE),
      show: user.canAcessThisRoute(PROVAO_MANAGEMENT_ROUTE),
      route: PROVAO_MANAGEMENT_ROUTE
    },
    {
      name:  'how_to_vote',
      text:  'Provão',
      action: () => redirect(PROVAO_CHOOSE),
      show: user.canAcessThisRoute(PROVAO_CHOOSE),
      route: PROVAO_CHOOSE
    },
    {
      name: 'favorite_border',
      text: 'Provão Online',
      action: () => redirect(PROVAO_ONLINE_ROUTE),
      show: user.canAcessThisRoute(PROVAO_ONLINE_ROUTE),
      route: PROVAO_ONLINE_ROUTE
    },
    {
      name: 'person_add',
      text: 'Gestão Usuários',
      action: () => redirect(USER_MANAGEMENT_ROUTE),
      show: user.canAcessThisRoute(USER_MANAGEMENT_ROUTE),
      route: USER_MANAGEMENT_ROUTE
    },
    {
      name: 'exit_to_app',
      text: 'Sair',
      action: () => logout(props),
      show: true
    },
  ];

  function getIndexInitialSelectedMenu(menuIcons) {
    const urlNow = props.location.pathname;
    return menuIcons.findIndex(menu => {
      if (menu.hasOwnProperty("route")){
        return matchRoute(menu.route, urlNow);
      }
      return false;
    });
  }

  function logout({alert}) {
    alert.onClick(() => {
      User.logout();
      redirect(LOGIN_ROUTE);
    });
  }

  function getMobileNavbar() {
    return (
      <>
        <ExpansiveFab buttons={menuIcons.filter(icon => icon.show)} onClick={handleClick}/>
      </>
    );
  }

  function getDeskptopNavbar() {
    return (
      <Drawer
        variant={'permanent'}
        classes={{paper: open ? classes.drawerExpanded : classes.drawer}}
      >
        <ListIcon icons={[mainMenu]} onClick={handleClick}/>
        <br />
        <br />
        <br />
        <ListIcon
          icons={menuIcons.filter(icon => icon.show)}
          onClick={handleClick}
          showText={open}
          showSelected
          initialIndexSelect={getIndexInitialSelectedMenu(menuIcons)}
        />
      </Drawer>
    );
  }

  function redirect(url) {
    props.history.push(url);
  }

  const handleClick = icon => {
    icon.action();
  };

  return (
    <>
      <Hidden smUp>
        { getMobileNavbar() }
      </Hidden>

      <Hidden xsDown>
        { getDeskptopNavbar() }
      </Hidden>
    </>
  );
}

export default withAlert(withRouter(Navbar), TITLE_ALERT);
