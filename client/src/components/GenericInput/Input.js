import React from "react";
import PropTypes from "prop-types";

import FormControlLabel from "@material-ui/core/FormControlLabel";

import StarRating from "../VoteProductCard/StarRating";
import FieldSelect from "../common/FieldSelect";
import TextFieldEstilo from "../common/TextField";
import Checkbox from "@material-ui/core/Checkbox";


export default function Input({ config, onChange, value, className, disabled = false }) {

  function mapToOptions(rawOptions, rawOptionIntegers) {
    const rawOptionsSplitted = rawOptions.split(";");
    const rawOptionIntegersSplitted = rawOptionIntegers.split(";");
    return rawOptionsSplitted.map((opt, i) => ({
      label: opt,
      value: rawOptionIntegersSplitted[i]
    }));
  }

  switch (config.tipo) {
    case "radio":
      const options = config.valores.split(";");
      return (
        <StarRating
          className={className}
          style={{ marginBottom: 10 }}
          numberOfStars={parseInt(options[options.length - 1])}
          name={config.nome}
          changeRating={value => onChange(value, config)}
          value={value ? parseInt(value) : 0}
        />
      );
    case "select":
      return (
        <FieldSelect
          className={className}
          style={{ marginBottom: 10 }}
          options={mapToOptions(config.valores, config.valores_inteiros)}
          name={config.nome}
          onChange={value => onChange(value, config)}
          value={value}
          disabled={disabled}
        />
      );
    case "numberInput":
      return (
        <TextFieldEstilo
          className={className}
          name={config.nome}
          onChange={e => onChange(e.target.value, config)}
          value={value}
          type={'number'}
          disabled={disabled}
        />
      );
    case "textInput":
      return (
        <TextFieldEstilo
          name={config.nome}
          className={className}
          onChange={e => onChange(e.target.value, config)}
          value={value?value.replace(/--/g,"\n\n"): null}
          rows={config.fl_salva_texto && disabled? 5 : 1}
          multiline={config.fl_salva_texto}
          fullWidth
          disabled={disabled}
        />
      );
    case "showInput":
      return (
        <TextFieldEstilo
          name={config.nome}
          className={className}
          onChange={e => onChange(e.target.value, config)}
          value={value}
          rows={config.fl_salva_texto ? 2 : 1}
          multiline={config.fl_salva_texto}
          fullWidth
          disabled={true}
        />
      );
    case "checkbox":
      return (
        <FormControlLabel
          checked={!!value}
          value={value}
          control={<Checkbox color="primary" />}
          onChange={e => onChange(e.target.checked, config)}
          label={config.nome}
          labelPlacement="start"
        />
      );
    default:
      return null;
  }
}

Input.proptypes = {
  config: PropTypes.shape({
    name: PropTypes.string.isRequired,
    tipo: PropTypes.string.isRequired,
    valores: PropTypes.string,
    valores_inteiros: PropTypes.string,
    estilo: PropTypes.string
  }),
  onChange: PropTypes.func,
  value: PropTypes.string
};
