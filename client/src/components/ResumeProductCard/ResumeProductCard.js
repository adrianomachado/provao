import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import MobileStepper from "@material-ui/core/MobileStepper";
import Button from "@material-ui/core/Button";

import FieldSelect from "../common/FieldSelect";
import AxiosWrapper from "../../services/AxiosWrapper";
import { BASE_URL } from "../../consts";

const styles = theme => ({
  card: {
    width: 280
  },
  media: {
    minHeight: 400
  },
  textField: {
    paddingTop: theme.spacing(2)
  }
});
const statusList = [
  { label: "Peça Física - Cor Certa", value: "Peça Física - Cor Certa" },
  { label: "Peça Física - Cor Errada", value: "Peça Física - Cor Errada" },
  { label: "Sem Peça - Aplicação", value: "Sem Peça - Aplicação" },
  { label: "Sem Peça - Foto", value: "Sem Peça - Foto" }
];
class ResumeProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      listImages: [],
      preco_final: "",
      varejo: "",
      atacado: "",
      estampa_correta: ""
    };
  }
  async componentDidMount() {
    const { product } = this.props;
    const { varejo, atacado, estampa_correta, preco_final } = product;
    this.setState({ varejo, atacado, estampa_correta, preco_final });
    this.getImages(product.id_produto_estilo);
    if (localStorage.getItem("user")) {
      if (
        JSON.parse(localStorage.getItem("user")).usuario.areas[
          "Provão - Coordenador"
        ] === 0 
      ) {
        this.setState({ admin: true });
      } else {
        this.setState({ admin: false });
      }
    }
  }
  handleChange = (event, name) => {
    this.setState({ [name]: event.target.value });
  };
  updateProducts = async () => {
    const { id_produto_estilo } = this.props.product;
    const { atacado, varejo, preco_final, estampa_correta } = this.state;
    const response = await AxiosWrapper.patch(
      `${BASE_URL}/produtos_estilo/update`,
      [
        {
          id_produto_estilo,
          atacado,
          varejo,
          preco_final,
          estampa_correta
        }
      ]
    );
    console.log(response);
  };

  async componentWillReceiveProps(prevProps, prevState) {
    if (prevProps.updateProdutoEstilo === true) {
      this.updateProducts();
    }
  }
  handleNext = () => {
    this.setState({ activeStep: this.state.activeStep + 1 });
  };

  handleBack = () => {
    this.setState({ activeStep: this.state.activeStep - 1 });
  };

  getImages = async id_produto_estilo => {
    const response = await AxiosWrapper.get(
      `${BASE_URL}/produtos_estilo/image/${id_produto_estilo}`
    );

    this.setState({ listImages: response.listImages });
  };

  render() {
    const { product, classes } = this.props;
    const { listImages, admin } = this.state;
    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={
            listImages[this.state.activeStep]
              ? listImages[this.state.activeStep].imgPath
              : null
          }
          title={
            listImages[this.state.activeStep]
              ? listImages[this.state.activeStep].label
              : null
          }
        />
        <MobileStepper
          variant="dots"
          steps={listImages.length}
          position="static"
          // variant="text"
          activeStep={this.state.activeStep}
          nextButton={
            <Button
              size="small"
              onClick={() => this.handleNext()}
              disabled={this.state.activeStep === listImages.length - 1}
            >
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              size="small"
              onClick={() => this.handleBack()}
              disabled={this.state.activeStep === 0}
            >
              <KeyboardArrowLeft />
            </Button>
          }
        />
        <CardContent>
          <Typography gutterBottom variant="button">
            {product.produto}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {product.desc_produto.length > 29
              ? `${product.desc_produto.substring(0, 29)}...`
              : product.desc_produto}
          </Typography>
        </CardContent>
        <CardContent>
          <Grid container spacing={2}>
            <TextField
              name={product.avg_price}
              disabled
              className={classes.textField}
              value={product.avg_price}
              fullWidth
              label="Preço médio"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.avg_score}
              disabled
              className={classes.textField}
              value={product.avg_score}
              fullWidth
              label="Nota"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.avg_nota_venda}
              disabled
              className={classes.textField}
              value={product.avg_nota_venda}
              fullWidth
              label="Nota Venda"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.avg_nota_beleza}
              disabled
              className={classes.textField}
              value={product.avg_nota_beleza}
              fullWidth
              label="Nota Beleza"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.avg_markup_provao}
              disabled
              className={classes.textField}
              value={product.avg_markup_provao}
              fullWidth
              label="Markup"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.avg_markup_estilo}
              disabled
              className={classes.textField}
              value={product.avg_markup_estilo}
              fullWidth
              label="Markup Estilo"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.grade_minima}
              disabled
              className={classes.textField}
              value={product.grade_minima}
              fullWidth
              label="Grade Mínima"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.grade_media}
              disabled
              className={classes.textField}
              value={product.grade_media}
              fullWidth
              label="Grade Média"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.grade_maxima}
              disabled
              className={classes.textField}
              value={product.grade_maxima}
              fullWidth
              label="Grade Máxima"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.markup_meta}
              disabled
              className={classes.textField}
              value={product.markup_meta}
              fullWidth
              label="Meta Markup"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.nota_atacado}
              disabled
              className={classes.textField}
              value={product.nota_atacado}
              fullWidth
              label="Nota Atacado"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name={product.grade_atacado}
              disabled={!admin}
              className={classes.textField}
              value={product.grade_atacado}
              fullWidth
              label="Grade Atacado Votação"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name="atacado"
              disabled={!admin}
              className={classes.textField}
              value={this.state.atacado}
              onChange={event => {
                this.handleChange(event, "atacado");
              }}
              fullWidth
              label="Grade Atacado"
              style={{ marginBottom: 10 }}
            />
            <TextField
              name="varejo"
              disabled={!admin}
              className={classes.textField}
              value={this.state.varejo}
              fullWidth
              label="Grade"
              onChange={event => {
                this.handleChange(event, "varejo");
              }}
              style={{ marginBottom: 10 }}
            />
            <TextField
              name="preco_final"
              disabled={!admin}
              className={classes.textField}
              value={this.state.preco_final}
              fullWidth
              label="Preço Final"
              type="number"
              style={{ marginBottom: 10 }}
              onChange={event => {
                this.handleChange(event, "preco_final");
              }}
            />
            <FieldSelect
              disabled={!admin}
              field="estampa_correta"
              name="Status"
              style={{ marginBottom: 10 }}
              options={statusList}
              onChange2={event => {
                this.handleChange(event, "estampa_correta");
              }}
              value={this.state.estampa_correta}
            />
            {product.obs ? (
              <Fragment>
                <Typography variant="h6">Observações:</Typography>
                <Typography>{product.obs.replace(/--/g, "\n")}</Typography>
              </Fragment>
            ) : null}
          </Grid>
        </CardContent>
      </Card>
    );
  }
}

ResumeProductCard.propTypes = {
  product: PropTypes.object,
  image: PropTypes.string
};

export default withStyles(styles)(ResumeProductCard);
