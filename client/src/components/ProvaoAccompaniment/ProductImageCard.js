import React, { Component } from "react";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";

import SwipeableCarrousel from "../../components/common/SwipeableCarrousel";

const styles = theme => ({
  card: {
    width: 280
  },
  media: {
    minHeight: 400,
  },
  textField: {
    paddingTop: theme.spacing(2)
  },
  inputMargin: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  inputMarginShowOnly: {
    padding: theme.spacing(0.5)
  }
});


class ProductImageCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listImages : this.getImg(props.product.fotos)
      }
  }

  renderParaph() {
    const { product } = this.props;
    if (product.desc_produto.length >= 30) {
      return `${product.desc_produto.substring(0, 30)}...`;
    }
    return product.desc_produto;
  }

  getImg(photos) {
    return photos.map(photo => photo.link_arquivo.replace('140/200', '500/500'));
  }


  render() {
    const { product, classes} = this.props;
    const  {listImages} = this.state;
    return (
      <Card className={classes.card}>
       <SwipeableCarrousel
         photos={listImages}
         stepper
         Component={img => <CardMedia className={classes.media} image={img} title={img} key={img} />}
       />
        <CardContent>
          <Typography gutterBottom variant="button">
            {product.produto}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.renderParaph()}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            R${product.preco_estilo},00
          </Typography>
        </CardContent>

      </Card>
    );
  }
}

ProductImageCard.propTypes = {
  product: PropTypes.object,

};

export default withStyles(styles)(ProductImageCard);
