import React from "react";
import PropTypes from "prop-types";

import makeStyles from "@material-ui/core/styles/makeStyles";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import CardMedia from "@material-ui/core/CardMedia";

const CARD_HEIGHT_MOBILE = "55vh";
const CARD_HEIGHT_LARGE_SCREENS = "60vh";

const styles = makeStyles(theme => ({
  media: {
    [theme.breakpoints.down("sm")]: {
      height: CARD_HEIGHT_MOBILE
    },
    [theme.breakpoints.up("md")]: {
      height: CARD_HEIGHT_LARGE_SCREENS
    }
  }
}));

export default function CardMediaItem({ img, onClick }) {
  const matches = useMediaQuery("(max-width:600px)");
  const classes = styles();
  return (
    <CardMedia
      key={img}
      className={classes.media}
      image={img}
      onClick={() => (matches ? onClick() : null)}
      title={img}
    />
  );
}

CardMediaItem.propTypes = {
  img: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
