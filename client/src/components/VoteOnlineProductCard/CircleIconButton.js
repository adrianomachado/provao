import React from "react";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
  voteBtn: {
    borderRadius: "50%",
    padding: theme.spacing(2)
  }
}));

export default function CircleIconButton({icon, onClick}) {
  const classes = styles();

  return (
    <Button
      variant={"outlined"}
      className={classes.voteBtn}
      onClick={onClick}
    >
      <Icon {...icon}>{icon.name}</Icon>
    </Button>
  );
}

CircleIconButton.propTypes = {
  icon: PropTypes.shape({
    name: PropTypes.string.isRequired
  }),
  onClick: PropTypes.func.isRequired,
};
