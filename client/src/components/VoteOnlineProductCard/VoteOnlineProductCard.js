import React, { Component } from "react";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import PhotoDialog from "../common/dialogs/PhotoDialog";
import SwipeableCarrousel from "../common/SwipeableCarrousel";
import CardMediaItem from "./CardMediaItem";
import CircleIconButton from "./CircleIconButton";
import {formatPrice} from "../../utils";

const CARD_WIDTH_LARGE_SCREENS = 312;
const CARD_WIDTH_MOBILE = "100%";
const MAX_SIZE_TEXT_MOBILE = 15;
const MAX_SIZE_TEXT_DESKTOP = 20;
const DOTS_SIZE = 3;
const PHOTO_DIMENSIONS = "420/600";

const styles = theme => ({
  card: {
    [theme.breakpoints.down("sm")]: {
      width: CARD_WIDTH_MOBILE
    },
    [theme.breakpoints.up("md")]: {
      width: CARD_WIDTH_LARGE_SCREENS
    }
  },
  voteBtnContainer: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2)
  },
});

function GroupLineText({ product }) {
  const matches = useMediaQuery("(max-width:600px)");
  let groupLineText = `${product.linha.linha} - ${product.grupo.grupo_produto}`;

  if (groupLineText.length > MAX_SIZE_TEXT_MOBILE && matches) {
    groupLineText =
      groupLineText.substring(0, MAX_SIZE_TEXT_MOBILE - DOTS_SIZE) + "...";
  }

  if (groupLineText.length > MAX_SIZE_TEXT_DESKTOP) {
    groupLineText =
      groupLineText.substring(0, MAX_SIZE_TEXT_DESKTOP - DOTS_SIZE) + "...";
  }

  return (
    <Typography
      variant="caption"
      color="textSecondary"
      component="p"
      align={"center"}
    >
      {groupLineText}
    </Typography>
  );
}

class VoteOnlineProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      isModalPhotoOpen: false
    };
  }

  handleClickPhoto = () => {
    this.setState({ isModalPhotoOpen: true });
  };

  handleClosePhotoModal = () => {
    this.setState({ isModalPhotoOpen: false });
  };

  resizePhoto(photos) {
    return photos.map(photo => photo.link_arquivo.replace("140/200", PHOTO_DIMENSIONS));
  };

  render() {
    const { product, classes, handleVote } = this.props;
    const { isModalPhotoOpen } = this.state;

    const photosResized = this.resizePhoto(product.fotos);
    return (
      <>
        <PhotoDialog
          open={isModalPhotoOpen}
          photos={photosResized}
          onClose={this.handleClosePhotoModal}
        />
        <Card className={classes.card}>
          <CardContent>
            <Grid container justify={"center"}>
              <Grid container item justify={"center"} xs={"auto"} md={6}>
                <GroupLineText product={product} />
              </Grid>
              <Grid container item justify={"center"} xs={"auto"} md={6}>
                <Typography
                  variant="caption"
                  color="textSecondary"
                  component="p"
                >
                  {formatPrice(product.preco_estilo)}
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
          <SwipeableCarrousel
            Component={img => (
              <CardMediaItem
                key={img}
                img={img}
                className={classes.media}
                onClick={this.handleClickPhoto}
              />
            )}
            photos={photosResized}
            stepper={true}
          />

          <Grid
            container
            justify={"center"}
            className={classes.voteBtnContainer}
          >
            <CircleIconButton icon={{name: 'favorite', color: 'secondary'}} onClick={handleVote}/>
          </Grid>
        </Card>
      </>
    );
  }
}

VoteOnlineProductCard.propTypes = {
  product: PropTypes.object,
  image: PropTypes.string
};

export default withStyles(styles)(VoteOnlineProductCard);
