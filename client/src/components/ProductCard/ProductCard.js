import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  card: {
    width: '140px',
    height: '190px'
  },
  cardMedia: {
    height: '156px'
  }
});

function ProductCard(props) {
  const classes = useStyles();

  return (
    <Draggable draggableId={props.content.id_produto_estilo} index={props.index} key={props.index} >
      {(provided) =>
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
        >
          <Card
            className={classes.card}
          >
            <CardMedia
              alt={props.content}
              className={classes.cardMedia}
              image={props.img}
            />
            <Grid container direction={'column'} alignItems={'center'} justify={'center'}  >
                <Typography variant="overline" color={'textSecondary'}>
                  { props.content.produto }
                  { props.content.estampaCor ? `-${props.content.estampaCor.estampa_cor}` : null}
                </Typography>

            </Grid>
          </Card>
        </div>
      }
    </Draggable>
  );
}

export default ProductCard;
