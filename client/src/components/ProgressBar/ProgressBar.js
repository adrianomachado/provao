import React  from 'react';

import LinearProgress from "@material-ui/core/LinearProgress";

const MIN = 0;

export default function ProgressBar({value, width, max}) {

  const normalise = (value, max) => (value - MIN) * 100 / (max - MIN);

  return (
    <div>
    <LinearProgress
      variant={'determinate'}
      value={normalise(value, max)}
      color={'primary'}
    />
    </div>
  );
}
