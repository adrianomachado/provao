import React from 'react';

import Close from '@material-ui/icons/Close';
import Fab from '@material-ui/core/Fab';

function CloseButton(props) {

  return (
    <Fab size={'small'} {...props}>
      <Close />
    </Fab>
  );
}

export default CloseButton;
