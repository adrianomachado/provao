import React from 'react';

import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: '#E6E6E6',
    padding: theme.spacing(2),
    margin: theme.spacing(2),
    border: '4px dashed #C9C9C9'
  }
}));

function DropArea(props) {

  const classes = useStyles();

  return (
    <div className={classes.root} {...props}>
      {props.children}
    </div>
  );
}

export default DropArea;
