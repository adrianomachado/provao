import React from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  root: {
    borderRadius: '50%',
    border: '4px dashed #C9C9C9',
    backgroundColor: '#fff',
    width: '32px',
    height: '32px',
    textAlign: 'center',
  }
});

function DropArea(props) {

  const classes = useStyles();

  const { children } = props;

  return (
    <div
      className={classes.root}
      {...props}
    >
      <Typography variant="h6" color={'textSecondary'}>
        <strong>{children}</strong>
      </Typography>
    </div>
  );
}

export default DropArea;
