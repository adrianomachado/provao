import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Droppable } from "react-beautiful-dnd";

import Grid from "@material-ui/core/Grid";

import ProductCard from "../ProductCard/ProductCard";
import { DROPPABLE_ID_ROUND } from "../../consts";
import DropArea from "./DropArea";
import CircularNumber from "./CircularNumber";
import CloseButton from "./CloseButton";
import StartRoundButton from "./StartRoundButton";
import ProductsApi from "../../services/apis/ProductsApi";
import User from "../../services/User";

import Fab from "@material-ui/core/Fab";

import Visibility from "@material-ui/icons/Visibility";
import Tooltip from "@material-ui/core/Tooltip";
import {RESULTADO_PROVAO_ROUTE} from "../../consts"

class Round extends Component {
  state = {
    isMouseInside: false
  };

  mouseEnter = () => {
    this.setState({ isMouseInside: true });
  };

  mouseLeave = () => {
    this.setState({ isMouseInside: false });
  };

  renderPlayBtn(index) {
    const { initRound } = this.props;
    const { isMouseInside } = this.state;
    const user = new User();
    const styleBtnPlay = {
      transition: "0.4s",
      position: "absolute",
      right: "0",
      bottom: "center",
      opacity: isMouseInside ? "1" : "0"
    };
    return (
      user.canStartProvao() && (
          <StartRoundButton
            style={styleBtnPlay}
            onClick={() => initRound(index)}
          />
      )
    );
  }

  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  }
  renderResultsBtn(index) {
    const {  id_rodada } = this.props;
    const { isMouseInside } = this.state;
    const url = RESULTADO_PROVAO_ROUTE.replace(':id_provao',this.getIdProvao()).replace(':id_rodada',id_rodada)
    const user = new User();
    const styleBtnView = {
      transition: "0.4s",
      position: "absolute",
      right: "0",
      bottom: "0",
      opacity: isMouseInside ? "1" : "0"
    };
    return (
      user.canStartProvao() && (
        <Tooltip title="Resultado">
          <Fab
            onClick={()=>{this.props.history.push(url)}}
            variant={"outlined"}
            size={"small"}
            color="primary"
            style={styleBtnView}
          >
            <Visibility />
          </Fab>
        </Tooltip>
      )
    );
  }

  render() {
    const { index, round, onClose } = this.props;
    const { isMouseInside } = this.state;

    const styleBtn = {
      transition: "0.4s",
      backgroundColor: "white",
      position: "absolute",
      right: "0",
      top: "0",
      opacity: isMouseInside ? "1" : "0"
    };

    return (
      <Grid
        container
        alignItems={"center"}
        justify={"center"}
        key={index}
        style={{ position: "relative" }}
        onMouseEnter={this.mouseEnter}
        onMouseLeave={this.mouseLeave}
      >
        <CloseButton style={styleBtn} onClick={() => onClose(index)} />
        {this.renderPlayBtn(index)}
        {this.renderResultsBtn(index)}
        <Grid item sm={12}>
          <CircularNumber style={this.props.roundActive? { position: "absolute", top: "0", left: "0",borderColor:'rgb(39, 51, 119)'} : { position: "absolute", top: "0", left: "0"}}>
            {index + 1}
          </CircularNumber>
          <DropArea
          style={this.props.roundActive? {borderColor:'rgb(39, 51, 119)'} : { }}>
            <Droppable
              droppableId={DROPPABLE_ID_ROUND + index}
              direction="horizontal"
            >
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  {...provided.droppablePlaceholder}
                  ref={provided.innerRef}
                  style={{ minHeight: "100px" }}
                >
                  <Grid container item spacing={4}>
                    {round.map((item, index) => (
                      <Grid item key={item.id_produto_estilo}>
                        <ProductCard
                          img={ProductsApi.getImageURL(item)}
                          content={item}
                          index={index}
                        />
                      </Grid>
                    ))}
                    {provided.placeholder}
                  </Grid>
                </div>
              )}
            </Droppable>
          </DropArea>
        </Grid>
      </Grid>
    );
  }
}

export default withRouter(Round);
