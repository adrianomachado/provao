import React from 'react';

import PlayArrow from '@material-ui/icons/PlayArrow';
import Fab from '@material-ui/core/Fab';
import Tooltip from "@material-ui/core/Tooltip";


function StartRoundButton(props) {

  return (
    <Tooltip title="Iniciar rodada">

    <Fab size={'small'} {...props} color="secondary">
      <PlayArrow />
    </Fab>
    </Tooltip>
  );
}

export default StartRoundButton;
