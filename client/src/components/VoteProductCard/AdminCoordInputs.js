import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";

import GenericInput from "../GenericInput/Input";
import ProductsApi from "../../services/apis/ProductsApi";
import LoadingCircle from "../common/LoadingCircle";

const styles = theme => ({
  input: {
    padding: theme.spacing(0.5)
  },
  loadingBtn: {
    marginLeft: theme.spacing(1),
    marginTop: 0,
    marginBottom: 0
  }
});

const inputs = [
  // {
  //   nome: "Grade Varejo",
  //   field: "varejo",
  //   value: "",
  //   tipo: "numberInput"
  // },
  // {
  //   nome: "Grade Atacado",
  //   field: "atacado",
  //   value: "",
  //   tipo: "numberInput"
  // },
  // {
  //   nome: "Status",
  //   field: "estampa_correta",
  //   value: "",
  //   valores:
  //     "Peça Física - Cor Certa;Peça Física - Cor Errada;Sem Peça - Aplicação;Sem Peça - Fotos",
  //   valores_inteiros:
  //     "Peça Física - Cor Certa;Peça Física - Cor Errada;Sem Peça - Aplicação;Sem Peça - Fotos",
  //   tipo: "select"
  // },
  // {
  //   nome: "Reprovado",
  //   field: "reprovado",
  //   value: false,
  //   tipo: "checkbox"
  // },
  // {
  //   nome: "Vitrine",
  //   field: "vitrine",
  //   value: false,
  //   tipo: "checkbox"
  // },
    {
    nome: "Provão Online",
    field: "fl_provao_online",
    value: true,
    tipo: "checkbox"
  }
];

const DEFAULT_MSG_BTN = "Salvar Alterações";
const SAVE_MSG_BTN = "Salvando...";

class AdminCoordInputs extends Component {
  state = {
    inputs,
    loading: false,
    buttonTitle: DEFAULT_MSG_BTN
  };

  componentDidMount() {
    const { product } = this.props;
    const { inputs } = this.state;
    const newState = this.mapProductPropsToInputsState(product, inputs);
    this.setState({inputs: newState});
  }

  mapProductPropsToInputsState(props, state) {
    return state.map(s => ({
      ...s,
      value: props[s.field] ? props[s.field] : ''
    }));

  }

  handleChange(value, input, i) {
    const { inputs } = this.state;
    inputs[i].value = value;
    this.setState({ inputs });
  }

  renderInputs() {
    const { inputs } = this.state;
    const { classes } = this.props;
    return inputs.map((input, i) => (
      <GenericInput
        key={input.nome}
        config={input}
        value={input.value}
        className={classes.input}
        onChange={(value, input) => this.handleChange(value, input, i)}
      />
    ));
  }

  getInputsFormatted() {
    const { inputs } = this.state;
    const inputsFormatted = {};
    inputs.forEach(input => {
      inputsFormatted[input.field] = input.value;
    });
    return inputsFormatted;
  }

  handleUpdateProduct() {
    const { product, enqueueSnackbar } = this.props;
    this.setState({ loading: true, buttonTitle: SAVE_MSG_BTN });
    const inputsFormatted = this.getInputsFormatted();
    const data = [
      {
        id_produto_estilo: product.id_produto_estilo,
        ...inputsFormatted
      }
    ];

    ProductsApi.update(data)
      .then(() =>
        enqueueSnackbar("Dados atualizados com sucesso!", {
          variant: "success"
        })
      )
      .catch(() => enqueueSnackbar("Não foi possível atualizar os dados."), {
        variant: "error"
      })
      .finally(() =>
        this.setState({ loading: false, buttonTitle: DEFAULT_MSG_BTN })
      );
  }

  render() {
    const { loading, buttonTitle } = this.state;
    const { classes,  resumeProvaoPage } = this.props;
    if(resumeProvaoPage) {
      return null
    }
    return (
      <Grid container spacing={2}>
        <Grid item>{this.renderInputs()}</Grid>
        <Grid container item justify={"center"}>
          <Button
            variant={"outlined"}
            color={"primary"}
            onClick={() => this.handleUpdateProduct()}
          >
            {buttonTitle}
            <LoadingCircle
              loading={loading}
              size={16}
              className={classes.loadingBtn}
              noPadding
            />
          </Button>
        </Grid>
      </Grid>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(AdminCoordInputs))
);

wrapperComponent.propTypes = {
  product: PropTypes.object
};

export default wrapperComponent;
