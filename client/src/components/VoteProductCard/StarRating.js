import React, {Component} from "react";
import StarRatings from 'react-star-ratings';

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const STAR_WIDTH = '16px';
const STAR_SPACING = '4px';
const STAR_COLOR = 'orange';

class StarRating extends Component {

  render() {
    return (
      <Grid container justify={'space-between'} spacing={1} style={this.props.style}>
        <Grid item>
          <Typography variant="body2" color="textSecondary" component="p">
            { this.props.name }:
          </Typography>
        </Grid>
        <Grid item>
          <StarRatings
            rating={this.props.value}
            starRatedColor={STAR_COLOR}
            starDimension={STAR_WIDTH}
            starSpacing={STAR_SPACING}
            changeRating={(newRating) => this.props.changeRating(newRating)}
            numberOfStars={this.props.numberOfStars}
          />
        </Grid>

      </Grid>

    );
  }
}

export default StarRating;
