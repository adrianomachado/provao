import React, { Component } from "react";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";

import GenericInput from "../../components/GenericInput/Input";
import SwipeableCarrousel from "../../components/common/SwipeableCarrousel";

const styles = theme => ({
  card: {
    width: 280
  },
  media: {
    minHeight: 400
  },
  textField: {
    paddingTop: theme.spacing(2)
  },
  inputMargin: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  inputMarginShowOnly: {
    padding: theme.spacing(0.5)
  }
});

class VoteProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listImages: this.getImg(props.product.fotos)
    };
  }

  renderParaph() {
    const { product } = this.props;
    if (product.descproduto) { 
      if (product.desc_produto.length >= 30) {
        return `${product.desc_produto.substring(0, 30)}...`;
      }

    }

    return product.desc_produto;
  }

  getImg(photos) {
    return photos.map(photo =>
      photo.link_arquivo.replace("140/200", "500/500")
    );
  }

  renderInputs() {
    const { voteParams, onChange, classes, showOnly } = this.props;

    return voteParams.map(voteParam => {
      if (showOnly) {
          return (
          <GenericInput
            className={showOnly ? classes.inputMarginShowOnly : classes.inputMargin}
            config={showOnly ? this.setInputTypeIntoText(voteParam) : voteParam}
            value={this.findValueInVote(voteParam.id_configuracao)}
            key={voteParam.id_configuracao}
            onChange={onChange}
            disabled={showOnly}
          />
        )
      }
      if (!voteParam.fl_atacado && !voteParam.fl_online) {
        return (
          <GenericInput
            className={showOnly ? classes.inputMarginShowOnly : classes.inputMargin}
            config={showOnly ? this.setInputTypeIntoText(voteParam) : voteParam}
            value={this.findValueInVote(voteParam.id_configuracao)}
            key={voteParam.id_configuracao}
            onChange={onChange}
            disabled={showOnly}
          />
        )
      }
      return null
    });
  }

  renderAtacadoInputs() {
    const { voteParams, onChange, classes, showOnly } = this.props;
    return voteParams.map(voteParam => {
      if (voteParam.fl_atacado === 1) {
        return (
          <GenericInput
            className={showOnly ? classes.inputMarginShowOnly : classes.inputMargin}
            config={showOnly ? this.setInputTypeIntoText(voteParam) : voteParam}
            value={this.findValueInVote(voteParam.id_configuracao)}
            key={voteParam.id_configuracao}
            onChange={onChange}
            disabled={showOnly}
          />
        )
      }
      return null
    });
  }

  renderOnlineInputs() {
    const { voteParams, onChange, classes, showOnly } = this.props;
    return voteParams.map(voteParam => {
      if (voteParam.fl_online === 1) {
        return (
          <GenericInput
            className={showOnly ? classes.inputMarginShowOnly : classes.inputMargin}
            config={showOnly ? this.setInputTypeIntoText(voteParam) : voteParam}
            value={this.findValueInVote(voteParam.id_configuracao)}
            key={voteParam.id_configuracao}
            onChange={onChange}
            disabled={showOnly}
          />
        )
      }
      return null
    });
  }

  setInputTypeIntoText(voteParam) {
    if (voteParam.tipo !== "textInput") {
      return {
        ...voteParam,
        tipo: "showInput"
      };
    }
    return voteParam;
  }

  findValueInVote(id_configuracao) {
    const { votes } = this.props;
    const voto =  votes.find(vote => {
      return vote.id_configuracao === id_configuracao})
      if(voto) {
        return voto.voto
      }
      return null
    }

  render() {
    const { product, classes, hideInput, children } = this.props;
    const { listImages } = this.state;
    return (
      <Card className={classes.card}>
        {listImages.length >=1? (        <SwipeableCarrousel
          photos={listImages}
          stepper
          Component={img => (
            <CardMedia
              className={classes.media}
              image={img}
              title={img}
              key={img}
            />
          )}
        />) :                <CardMedia
        className={classes.media}
        image={"/no-picture.png"}
        title={'no-picture'}
      /> }
        <CardContent>
          <Typography gutterBottom variant="button">
            {product.produto}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.renderParaph()}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            R${product.preco_estilo},00
          </Typography>
        </CardContent>
        {hideInput === true ? null : (
          <CardContent>
            <Grid container spacing={1}>
              {this.props.userIsAtacado? this.renderAtacadoInputs(): this.props.onlineVote? this.renderOnlineInputs() : this.renderInputs()}
              
            </Grid>
            <Grid container spacing={1}>
              {children}
            </Grid>
          </CardContent>
        )}
      </Card>
    );
  }
}

VoteProductCard.propTypes = {
  product: PropTypes.object,
  votes: PropTypes.array,
  showOnly: PropTypes.bool,
  hideInput: PropTypes.bool,
};

export default withStyles(styles)(VoteProductCard);
