import React, { Component } from "react";
import { SnackbarProvider } from "notistack";

import {
  MuiThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { deepOrange, indigo } from "@material-ui/core/colors";
import Routes from "./routes"
import Grid from "@material-ui/core/Grid";
import { withRouter } from "react-router-dom";



const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: indigo[600]
    },
    secondary: {
      main: deepOrange[500]
    }
  }
});


class App extends Component {

  render() {
    return (
      <div>
        <MuiThemeProvider theme={theme}>
          <SnackbarProvider
            maxSnack={3}
            anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
            autoHideDuration={2500}
          >
            <Grid container>
              <Routes />
            </Grid>
          </SnackbarProvider>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default withRouter(App);
