import {
  FETCH_COLLECTIONS_BEGIN,
  FETCH_COLLECTIONS_SUCCESS,
  FETCH_COLLECTIONS_FAILURE,
  COLLECTION_TOGGLE
} from '../actions/actionTypes';
import { combineReducers } from 'redux';

const initialState = {
  collections: [],
  loading: false,
  error: null,
};

const collectionsListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_COLLECTIONS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_COLLECTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        collections: payload
      };
    case FETCH_COLLECTIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload.error
      };
    default:
      return state;
  }
};

const initialStateCollectionSelected = {
  value: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).collectionSelected.value : '',
  label: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).collectionSelected.label : '',
};

const collectionReducer = (state = initialStateCollectionSelected, { type, payload }) => {
  switch (type) {
    case COLLECTION_TOGGLE:
      return payload;
    default:
      return state;
  }
};

export const collectionsReducer = combineReducers({
  list: collectionsListReducer,
  selected: collectionReducer
});
