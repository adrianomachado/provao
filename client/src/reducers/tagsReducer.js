import {
  FETCH_TAGS_BEGIN,
  FETCH_TAGS_SUCCESS,
  FETCH_TAGS_FAILURE,
} from '../actions/actionTypes';
import { combineReducers } from 'redux';

const initialState = {
  tags: [],
  loading: false,
  error: null,
};

const collectionsListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_TAGS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_TAGS_SUCCESS:
      return {
        ...state,
        loading: false,
        tags: payload.etiquetas
      };
    case FETCH_TAGS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload.error
      };
    default:
      return state;
  }
};

export const tagsReducer = combineReducers({
  list: collectionsListReducer,
});
