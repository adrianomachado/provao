import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { collectionsReducer as collections} from './collectionsReducer';
import { brandsReducer as brands} from './brandsReducer';
import { roundDaysReducer as roundDays} from './roundDaysReducer';
import { loadingDialogReducer as loadingDialog} from './loadingDialogReducer';
import { tagsReducer as tags} from './tagsReducer';
import { roundsReducer as rounds} from './roundsReducer';
import  authReducer from './authReducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  brands,
  collections,
  roundDays,
  tags,
  loadingDialog,
  rounds,
  authReducer,
});
