import { SHOW_LOADING_DIALOG, HIDE_LOADING_DIALOG} from '../actions/actionTypes';

const initialState = {open: false, message: ''};

export const loadingDialogReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_LOADING_DIALOG:
      return {
        open: true,
        message: payload,
      };
    case HIDE_LOADING_DIALOG:
      return {
        ...initialState
      };
    default:
      return {...state};
  }
};
