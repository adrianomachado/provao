import {FETCH_BRANDS_BEGIN, FETCH_BRANDS_SUCCESS, FETCH_BRANDS_FAILURE, BRAND_TOGGLE} from '../actions/actionTypes';
import { combineReducers } from 'redux';

const initialState = {
  brands: [],
  loading: false,
  error: null,
};

const brandsListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_BRANDS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_BRANDS_SUCCESS:
      return {
        ...state,
        loading: false,
        brands: payload
      };
    case FETCH_BRANDS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    default:
      return state;
  }
};

const initialStateBrandSelected = {
  value: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).brandSelected.value : '',
  label: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).brandSelected.label : '',
};

const brandReducer = (state = initialStateBrandSelected, { type, payload }) => {
  switch (type) {
    case BRAND_TOGGLE:
      return payload;
    default:
      return state;
  }
};

export const brandsReducer = combineReducers({
  list: brandsListReducer,
  selected: brandReducer
});
