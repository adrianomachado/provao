import { ROUND_DAYS_TOGGLE, GET_ROUND_DAYS } from '../actions/actionTypes';
import { MAX_DAYS } from '../consts';
import { combineReducers } from 'redux';

function getDaysRounds() {
  let days = [];
  for (let i = 1; i <= MAX_DAYS;i++) {
    days.push({label: i, value: i});
  }
  return days;
}

const initialState = {
  roundDays: getDaysRounds(),
};

const roundDaysListReducer = (state = initialState, { type }) => {
  switch (type) {
    case GET_ROUND_DAYS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

const initialStateDaySelected = {
  value: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).roundDaySelected.value : 1,
  label: localStorage.getItem("params")? JSON.parse(localStorage.getItem("params")).roundDaySelected.label : 1,
};

const roundDayReducer = (state = initialStateDaySelected, { type, payload }) => {
  switch (type) {
    case ROUND_DAYS_TOGGLE:
      return payload;
    default:
      return state;
  }
};

export const roundDaysReducer = combineReducers({
  list: roundDaysListReducer,
  selected: roundDayReducer
});
