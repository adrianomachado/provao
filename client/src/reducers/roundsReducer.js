import {FETCH_ROUNDS_BEGIN, FETCH_ROUNDS_SUCCESS, FETCH_ROUNDS_FAILURE} from '../actions/actionTypes';

const initialState = {
  rounds: [],
  loading: false,
  error: null,
};

export const roundsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_ROUNDS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_ROUNDS_SUCCESS:
      return {
        ...state,
        loading: false,
        rounds: payload
      };
    case FETCH_ROUNDS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };
    default:
      return state;
  }
};
