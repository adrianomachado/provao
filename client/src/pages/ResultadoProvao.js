import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import LoadingDialog from "../components/common/dialogs/LoadingDialog";

import { toggleLoadingDialog } from "../actions";
import User from "../services/User";
import ProvaoApi from "../services/apis/ProvaoApi";
import Resume from "./Resume";
import LoadingCircle from "../components/common/LoadingCircle";

const styles = theme => ({
  root: {
    padding: theme.spacing(5.5)
  }
});

const RESULT_MSG = "Resultados da rodada...";

class ProvaoVote extends Component {
  constructor(props) {
    super(props);
    const user = new User().getUser();
    this.state = {
      title: RESULT_MSG,
      voteParams: [],
      productRounds: [],
      loading: true,
      waitingRound: false,
      voteFinished: false,
      user
    };
  }

  async componentDidMount() {
    try {
      var voteParams = await this.getProvaoConfig();
    } finally {
      this.setState({ loading: false, voteParams });
    }
  }

  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  }
  getIdRodada() {
    const { match } = this.props;
    return parseInt(match.params.id_rodada);
  }

  async getProvaoConfig() {
    const { configuracoes } = await ProvaoApi.getConfigs(this.getIdProvao());
    return configuracoes.map(config => ({
      id_configuracao_provao: config.id_configuracao_provao,
      ...config.configuracoes
    }));
  }

  renderScreen() {
    const {

      loading,
      voteParams,
    } = this.state;

    if (loading) {
      return <LoadingCircle loading={loading} />;
    }
    return (
      <Resume
        voteParams={voteParams}
        id_rodada_results={this.getIdRodada()}
        provao={'resultado'}
        onError={()=>{}}
      />
    );
  }



  render() {
    const { loadingDialog, classes } = this.props;
    const { title } = this.state;
    return (
      <Container fixed className={classes.root}>
        <LoadingDialog
          open={loadingDialog.open}
          message={loadingDialog.message}
        />

        <Grid container justify={"center"} spacing={2}>
          <Grid container item justify={"center"}>
            <Typography variant={"h5"} color={"textSecondary"}>
              {title}
            </Typography>
          </Grid>

          {this.renderScreen()}
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoVote))
);

const mapStateToProps = state => ({
  loadingDialog: state.loadingDialog
});

const mapDispatchToProps = dispatch => ({
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapperComponent);
