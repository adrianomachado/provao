import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { DragDropContext } from "react-beautiful-dnd";
import { withSnackbar } from "notistack";

import Container from "@material-ui/core/Container";
import Drawer from "@material-ui/core/Drawer";

import OrderingContainer from "../components/Containers/OrderingContainer";
import AvailableProductsContainer from "../components/Containers/AvailableProductsContainer";
import { getRoundDays, loadRounds, toggleLoadingDialog } from "../actions";
import ProductsApi from "../services/apis/ProductsApi";
import RoundTrialApi from "../services/apis/RoundTrialApi";
import CardMover from "../services/CardMover";
import RoundRequestHandler from "../services/RoundRequestHandler";
import User from "../services/User";
import LoadingDialog from "../components/common/dialogs/LoadingDialog";
import { LIMIT_PER_ROUND } from "../consts";

class ProvaoOrder extends Component {
  state = {
    availableItems: [],
    rounds: [[]],
    originalRounds: null,
    brand: "",
    tag: "",
    collection: "",
    name: "",
    showResults: false
  };

  async componentDidMount() {
    const { getRoundDays } = this.props;

    getRoundDays();
    this.loadProvao(1);
    const { tag, collection, name } = this.getTagAndColectionAndNameFromUrl();
    const { brand } = this.getBrandFromUser();
    this.requestAvailableProducts("", {
      marca: brand,
      colecao: collection.id_colecao
    });
    await this.setState({ tag, collection, name, brand });
  }

  getTagAndColectionAndNameFromUrl() {
    const tag = this.getJsonFromUrl("etiqueta");
    const collection = this.getJsonFromUrl("colecao");
    const name = this.getParamFromUrl("nome");
    return { tag, collection, name };
  }

  getBrandFromUser() {
    const user = new User().getUser();
    return { brand: user.id_marca_estilo };
  }

  getJsonFromUrl(param) {
    return JSON.parse(this.getParamFromUrl(param));
  }

  getParamFromUrl(param) {
    const urlSearch = new URLSearchParams(this.props.location.search);
    return urlSearch.get(param);
  }

  loadProvao(day) {
    const { enqueueSnackbar, toggleLoadingDialog, loadRounds } = this.props;
    toggleLoadingDialog(true, "Carregando dia do provão...");
    loadRounds({
      dia: day,
      id_provao: this.getProvaoId()
    })
      .catch(() =>
        enqueueSnackbar("Falha ao puxar as rodadas.", { variant: "error" })
      )
      .finally(() => toggleLoadingDialog(false));
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.rounds.rounds !== prevProps.rounds.rounds) {
      console.log(this.props.rounds);

      this.setState({
        rounds: this.props.rounds.rounds.map(round => round.rodada_produtos),
        originalRounds: this.props.rounds.rounds,
        showResults: false
      });
    }

    if (prevState.rounds !== this.state.rounds) {
      this.setState({ originalRounds: this.syncRounds(this.state.rounds) });
    }
  }

  getProvaoId() {
    const { match } = this.props;
    return match.params.id_provao;
  }

  addRound() {
    const {
      roundDaySelected,
      enqueueSnackbar,
      toggleLoadingDialog
    } = this.props;

    const newRounds = Array.from(this.state.rounds);
    const newRoundsOriginals = Array.from(this.state.originalRounds);

    toggleLoadingDialog(true, "Criando rodada...");

    RoundTrialApi.create({
      dia: parseInt(roundDaySelected.value),
      id_provao: this.getProvaoId()
    })
      .then(({ rodadaProvaoCriada }) => {
        newRounds.push([]);
        newRoundsOriginals.push({
          id_rodada: rodadaProvaoCriada.id_rodada,
          rodada_produtos: []
        });
        this.setState({
          rounds: newRounds,
          originalRounds: newRoundsOriginals
        });
      })
      .catch(() =>
        enqueueSnackbar("Falha ao cria a rodada.", { variant: "error" })
      )
      .finally(() => toggleLoadingDialog(false));
  }

  onDragEnd = ({ source, destination, draggableId }) => {
    if (!destination) {
      return;
    }

    const oldSyncedRounds = this.syncRounds(this.state.rounds);
    const sourceRoundToBeUpdated = this.findRoundToBeUpdated(
      oldSyncedRounds,
      draggableId
    );

    const cardMover = new CardMover(destination, source, {
      rounds: this.state.rounds,
      availableItems: this.state.availableItems
    });

    cardMover.loadDestinationLogic((destinationListName, destinationList) =>
      destinationListName === "rounds"
        ? destinationList.length < LIMIT_PER_ROUND
        : true
    );

    const newState = cardMover.moveCard();

    const newSyncedRounds = this.syncRounds(newState.rounds);
    const destinationRoundToBeUpdated = this.findRoundToBeUpdated(
      newSyncedRounds,
      draggableId
    );

    const stateBackup = {
      rounds: this.state.rounds,
      availableItems: this.state.availableItems
    };
    const roundRequestHandler = new RoundRequestHandler(source, destination);
    roundRequestHandler
      .update(sourceRoundToBeUpdated, destinationRoundToBeUpdated)
      .catch(() => this.setState({ ...stateBackup }));

    this.setState({ ...newState });
  };

  findRoundToBeUpdated(syncedRounds, draggableId) {
    let roundToBeUpdated = null;
    for (let i = 0; i < syncedRounds.length; i++) {
      const product = syncedRounds[i].rodada_produtos.find(
        produto => produto.id_produto_estilo === draggableId
      );
      if (product) {
        roundToBeUpdated = syncedRounds[i];
        break;
      }
    }
    return roundToBeUpdated;
  }

  requestAvailableProducts(productRef, filters = {}) {
    const { brand, collection } = this.state;
    ProductsApi.get({
      marca: brand,
      colecao: collection.id_colecao,
      desc: productRef,
      ...filters
    }).then(products => this.setState({ availableItems: products }));
  }

  syncRounds(rounds) {
    const { originalRounds } = this.state;
    return originalRounds.map((round, index) => {
      if (index < rounds.length) {
        round.rodada_produtos = rounds[index];
      }
      return round;
    });
  }

  onCloseRound(index) {
    const roundsCopy = Array.from(this.state.rounds);
    const originalRoundsCopy = Array.from(this.state.originalRounds);
    roundsCopy.splice(index, 1);
    originalRoundsCopy.splice(index, 1);
    this.setState({
      rounds: roundsCopy,
      originalRounds: originalRoundsCopy
    });
  }

  render() {
    const { loadingDialog } = this.props;
    const { tag, collection, brand, name } = this.state;
    return (
      <Container fixed>
        <LoadingDialog
          open={loadingDialog.open}
          message={loadingDialog.message}
        />

        <DragDropContext onDragEnd={this.onDragEnd}>
          <OrderingContainer
            rounds={this.state.originalRounds ? this.state.originalRounds : []}
            loadRounds={day => this.loadProvao(day)}
            addRound={() => this.addRound()}
            onCloseRound={index => this.onCloseRound(index)}
            tag={tag}
            collection={collection}
            brand={brand}
            name={name}
          />

          <Drawer variant="permanent" anchor={"right"} open>
            <AvailableProductsContainer
              items={this.state.availableItems}
              searchProducts={(productRef, filters) =>
                this.requestAvailableProducts(productRef, filters)
              }
              tag={tag}
              collection={collection}
            />
          </Drawer>
        </DragDropContext>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  roundDaySelected: state.roundDays.selected,
  loadingDialog: state.loadingDialog,
  rounds: state.rounds
});

const mapDispatchToProps = dispatch => ({
  loadRounds: params => dispatch(loadRounds(params)),
  getRoundDays: () => dispatch(getRoundDays()),
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(withRouter(ProvaoOrder)));
