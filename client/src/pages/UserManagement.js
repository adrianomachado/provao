import React, { Component } from "react";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import FieldSelect from "../components/common/FieldSelect";
import TextField from "../components/common/TextField";
import Switch from "../components/common/Switch";
import { PLM_API_URL } from "../consts";
import AxiosWrapper from "../services/AxiosWrapper";
import Autocomplete from "../components/common/Autocomplete";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  },
  paper: {
    width: "100%",
    padding: theme.spacing(4)
  },
  autocomplete: {
    minWidth: "300px"
  }
});

const config = {
  baseURL: PLM_API_URL
};

const SPACE_BETWEEN_INPUTS = 2;
const SAVE_USER_MESSAGE = "Usuário criado/editado com sucesso!";
const ERROR_SAVE_USER_MESSAGE = "Não foi possível salvar/editar o usuário";

class UserManagement extends Component {
  state = {
    formErrors: [],
    userEdit: true,
    login: "",
    email: "",
    userSelected: "",
    users: [],
    groupSelected: "",
    groups: [],
    brandSelected: "",
    brands: []
  };

  componentDidMount() {
    Promise.all([this.getUser(), this.getBrands(), this.getGroups()]).then(
      data =>
        this.setState({
          users: data[0],
          brands: data[1],
          groups: data[2]
        })
    );
  }

  mapToOptions(options, label, value) {
    return options.map(option => ({
      ...option,
      label: option[label],
      value: option[value]
    }));
  }

  getUser() {
    return AxiosWrapper.get("/admin/usuario", config);
  }

  getBrands() {
    return AxiosWrapper.get("/marcas", config);
  }

  getGroups() {
    return AxiosWrapper.get("/admin/grupo", config);
  }

  saveUser = () => {
    const {
      userSelected,
      email,
      brandSelected,
      groupSelected,
      login,
      password,
      userEdit
    } = this.state;
    const { enqueueSnackbar } = this.props;

    if (!this.isNewUserFormValid()) {
      this.showErrors();
      return;
    }

    const userData = userEdit ? userSelected : { login, senha: password };
    return AxiosWrapper.post(
      this.getUrlToSave(),
      {
        ...userData,
        email,
        id_marca_estilo: brandSelected,
        id_grupo: groupSelected
      },
      config
    )
      .then(() => enqueueSnackbar(SAVE_USER_MESSAGE, { variant: "success" }))
      .catch(() =>
        enqueueSnackbar(ERROR_SAVE_USER_MESSAGE, { variant: "error" })
      );
  };

  showErrors() {
    const { formErrors } = this.state;
    const { enqueueSnackbar } = this.props;
    formErrors.map(error =>
      enqueueSnackbar(error, { variant: "error" })
    );
  }

  getUrlToSave() {
    const { userEdit, userSelected } = this.state;
    if (userEdit) {
      return `/admin/usuario/${userSelected.id_usuario}`;
    }
    return "/admin/usuario";
  }

  handleChange = (value, field) => {
    this.setState({ [field]: value });
  };

  handleAutocomplete = userSelected => {
    this.clearForm(() => {
      this.setState({
        brandSelected: this.treatData(userSelected.id_marca_estilo),
        groupSelected: this.treatData(userSelected.id_grupo),
        email: this.treatData(userSelected.email),
        userSelected
      });
    });
  };

  // Prevent to change to controlled to uncontrolled input
  treatData(field) {
    return field ? field : "";
  }

  handleSwitchChange = e => {
    this.setState({
      userEdit: e.target.checked,
      userSelected: ""
    });
    this.clearForm();
  };

  clearForm(func = null) {
    this.setState(
      {
        groupSelected: "",
        brandSelected: "",
        email: "",
        login: "",
        password: ""
      },
      func
    );
  }

  isNewUserFormValid() {
    const { userEdit, email, login, password } = this.state;
    if (userEdit) {
      return true;
    }
    const errors = [];
    const errorMsg = ' não pode ser vazio!';
    if (email === "") {
      errors.push(`Email ${errorMsg}`);
    }
    if (login === "") {
      errors.push(`Login ${errorMsg}`);
    }
    if (password === "") {
      errors.push(`Password ${errorMsg}`);
    }
    if (errors.length === 0) {
      return true;
    }
    this.setState({ formErrors: errors });
  }

  renderSelect(name, field, { options, label, value }) {
    return (
      <FieldSelect
        name={name}
        value={this.state[field]}
        options={this.mapToOptions(options, label, value)}
        onChange={value => this.handleChange(value, field)}
      />
    );
  }

  renderLoginPasswordForm() {
    return (
      <Grid container spacing={SPACE_BETWEEN_INPUTS}>
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={e => this.handleChange(e.target.value, "login")}
            value={this.state.login}
            name={"Login"}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={e => this.handleChange(e.target.value, "password")}
            value={this.state.password}
            name={"Password"}
            type={"password"}
          />
        </Grid>
      </Grid>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <Container fixed>
        <Grid container spacing={2} className={classes.root}>
          <Paper className={classes.paper}>
            <Grid container spacing={4}>
              {/*Title*/}
              <Grid container item spacing={SPACE_BETWEEN_INPUTS}>
                <Grid item>
                  <Typography variant={"h5"}>Gestão de Usuários</Typography>
                </Grid>

                <Grid item>
                  <Switch
                    title={{ left: "Criar Usuário", right: "Editar Usuário" }}
                    onChange={this.handleSwitchChange}
                    checked={this.state.userEdit}
                  />
                </Grid>
              </Grid>

              {/*First row*/}
              <Grid container item>
                {this.state.userEdit ? (
                  <Autocomplete
                    options={this.mapToOptions(
                      this.state.users,
                      "login",
                      "id_usuario"
                    )}
                    placeholder={"Usuários..."}
                    value={this.state.userSelected}
                    onChange={this.handleAutocomplete}
                  />
                ) : (
                  this.renderLoginPasswordForm()
                )}
              </Grid>

              {/*Second row*/}
              <Grid container item spacing={SPACE_BETWEEN_INPUTS}>
                <Grid item>
                  <TextField
                    onChange={e => this.handleChange(e.target.value, "email")}
                    value={this.state.email}
                    name={"Email"}
                    type={"email"}
                    titleWidth={300}
                    required
                  />
                </Grid>

                <Grid item>
                  {this.renderSelect("Grupos", "groupSelected", {
                    options: this.state.groups,
                    label: "grupo",
                    value: "id_grupo"
                  })}
                </Grid>

                <Grid item>
                  {this.renderSelect("Marcas", "brandSelected", {
                    options: this.state.brands,
                    label: "marca",
                    value: "id_marca"
                  })}
                </Grid>
              </Grid>

              {/*Third row*/}
              <Grid container item>
                <Button
                  color={"primary"}
                  onClick={this.saveUser}
                  variant={"contained"}
                >
                  {this.state.userEdit ? "Salvar alterações" : "Criar Usuário"}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(withSnackbar(UserManagement));

export default wrapperComponent;
