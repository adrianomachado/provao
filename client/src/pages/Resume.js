import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";

import VoteProductCard from "../components/VoteProductCard/VoteProductCard";
import AdminCoordInputs from "../components/VoteProductCard/AdminCoordInputs";
import ProvaoApi from "../services/apis/ProvaoApi";
import User from "../services/User";
import RoundTrialApi from "../services/apis/RoundTrialApi";
import LoadingDialog from "../components/common/dialogs/LoadingDialog";

const styles = theme => ({
  input: {
    padding: theme.spacing(0.5)
  }
});


class Resume extends Component {
  state = {
    resumos: [],
    votos: [],
    id_rodada: null,
    noMoreRounds : false
  };

  async componentDidMount() {
    const { provao, onError,id_rodada_results } = this.props;
    try {
        const { resumo, id_rodada } =   provao && provao !== 'resultado'? await this.getResume(provao): await RoundTrialApi.getResults(id_rodada_results)
      this.setState({
        resumos: resumo,
        votos: resumo.map(r => r.votos),
        id_rodada
      });
    } catch (e) {
      const { enqueueSnackbar } = this.props;
      if (this.props.resumeProvaoPage) {
        return
      }
      enqueueSnackbar(`${provao?"Não existe resumo para esse provão":"Essa rodada ainda não foi votada"}`, {
        variant: "warning"
      });
      onError("Não existem rodadas votadas nesse provão.");
    }
  }

  async handleNextRound() {
    const { provao, enqueueSnackbar } = this.props;
    this.setState({loadingNextRound:true,})
    try {
      const { rodada_iniciada } = await ProvaoApi.startNextRound(provao, this.props.id_rodada_results?this.props.id_rodada_results: this.state.id_rodada);
      if (rodada_iniciada.length === 0) {
        enqueueSnackbar("Não existem mais rodadas nesse provão", {
          variant: "warning"
        });
        this.setState({noMoreRounds:true})
      }
      this.setState({loadingNextRound:false})
    } catch (err) {
      enqueueSnackbar("Não foi possível iniciar a rodada.", {
        variant: "error"
      });
      this.setState({loadingNextRound:false})
    }
  }

  getResume(id_provao) {
    return ProvaoApi.getLastSummary(id_provao);
  }

  renderProducts() {
    const { resumos } = this.state;
    return resumos.map((resumo) => (
      <Grid item key={resumo.id_produto_estilo}>
        {this.renderProductCard(resumo.produto, resumo.votos)}
      </Grid>
    ));
  }

  renderProductCard(product, votes, i) {
    const { voteParams } = this.props;
    return (
      <VoteProductCard
        product={product}
        voteParams={voteParams}
        votes={votes}
        showOnly={true}
      >
        { this.userIsCoord() ? <AdminCoordInputs resumeProvaoPage={this.props.resumeProvaoPage}  product={product}/> : null}
      </VoteProductCard>
    );
  }

  userIsCoord() {
    const user = new User();
    return user.canStartProvao();
  }

  renderNextRoundBtn() {
    if (this.userIsCoord()) {
      return (
        <Grid container item justify={"flex-end"}>
          <Button
            variant={"contained"}
            color={"primary"}
            onClick={() => this.handleNextRound()}
          >
            Próxima rodada
          </Button>
        </Grid>
      );
    }
    return null;
  }


  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  } 
  redirect(url, rowData) {
    if(rowData) {
      const data = {
        etiqueta: JSON.stringify(rowData.etiqueta),
        colecao: JSON.stringify(rowData.colecao),
        nome: rowData.nome
      };
      const searchParams = new URLSearchParams(data);
      return this.props.history.push(`${url}?${searchParams.toString()}`);
    }
    return this.props.history.push(url);
  }

  mountURL(url, idProvao) {
    return url.replace(":id_provao", idProvao);
  }
  renderBackBtn() {

    if (this.userIsCoord() && !this.props.resumeProvaoPage) {
      return (
        <Grid container item justify={"flex-end"}>
          <Button
            variant={"contained"}
            color={"primary"}
            onClick={() =>   this.props.history.goBack()}
          >
            Voltar
          </Button>
        </Grid>
      );
    }
    return null;
  }

  render() {
      if (this.state.loadingNextRound) {
        return (        <LoadingDialog
          open={this.state.loadingNextRound}
          message={'Carregando próxima rodada'}
        />)
      }
    return (
      <>
        <Grid container item spacing={3}>
          {this.renderProducts()}
        </Grid>
        {this.props.provao !=='resultado' && this.state.noMoreRounds === false?this.renderNextRoundBtn():this.renderBackBtn()}
      </>
    );
  }
}

const wrapperComponent = withStyles(styles)(withSnackbar(withRouter(Resume)));

wrapperComponent.propTypes = {
  voteParams: PropTypes.array.isRequired,
  provao: PropTypes.number.isRequired,
  onError: PropTypes.func
};

export default wrapperComponent;
