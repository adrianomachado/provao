import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/styles";

import AxiosWrapper from "../services/AxiosWrapper";
import { BASE_URL, PROVAO_ROUTE } from "../consts";

import ANIMALE from "../assets/ANIMALE.png"
import ABRAND from "../assets/ABRAND.png"
import CRISBARROS from "../assets/CRISBARROS.png"
import FARM from "../assets/FARM.png"
import FABULA from "../assets/FABULA.png"
import FOXTON from "../assets/FOXTON.png"
import FYI from "../assets/FYI.png"
import SOMA from "../assets/SOMA.png"
import {
  getRoundDays,
  loadBrands,
  loadCollections,
  toggleLoadingDialog
} from "../actions";

const styles = theme => ({
  item: {
    height: 300,
    width:350,
  },
  card: {
    marginBottom:theme.spacing(6),
    marginTop: theme.spacing(4),
    transition: theme.transitions.create(["margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    }),
    "&:hover": {
      marginTop: theme.spacing(2)
    }
  },
  image: { height: 100, width: 230, margin:'auto' }
});
class EscolhaProvao extends Component {
  constructor(props) {
    super(props);
    this.state = {
     provoes:[],
     id_marca_estilo: null
    }
  }
  async componentDidMount() {
    const {
      usuario: { id_marca_estilo }
    } = JSON.parse(localStorage.getItem("user"));
    await this.setState({ id_marca_estilo });
    this.getProvoes(id_marca_estilo);
    this.selectImageByBrand()
  }

  async getProvoes(id_marca_estilo) {
    const response = await AxiosWrapper.get(
      `/provoes?id_marca=${id_marca_estilo}`
    );
    this.setState({ provoes: response.provoes });
  }

  async selectImageByBrand() {
    const {id_marca_estilo} = this.state
    switch (id_marca_estilo) {
      case 6: 
      return  this.setState({cardImageBrand: ANIMALE})
      case 5: 
      return  this.setState({cardImageBrand: ABRAND})
      case 7: 
      return  this.setState({cardImageBrand:FYI})
      case 12: 
      return  this.setState({cardImageBrand:FARM})
      case 13: 
      return  this.setState({cardImageBrand:FABULA})
      case 1820: 
      return  this.setState({cardImageBrand:FOXTON})
      case 9472: 
      return  this.setState({cardImageBrand:CRISBARROS})
      default:
        return this.setState({cardImageBrand:SOMA})
    }
  }

  redirectToProvao(id_provao) {
    const { history } = this.props;
    const url = PROVAO_ROUTE.replace(':id_provao', id_provao);
    history.push(url);
  }

  renderProvoesToChoose() {
    const { provoes,cardImageBrand } = this.state;
    const { classes } = this.props;

    const cards = provoes.map(provao => {
      return (
        <Grid item className={classes.item} key={provao.id_provao} onClick={()=> this.redirectToProvao(provao.id_provao)}>
          <Card className={classes.card}>
            <CardContent>
            <Typography variant="h5" component="h3" gutterBottom align="center">
                {provao.nome}
              </Typography>
              <Typography align="center" color="textSecondary" gutterBottom style={{textTransform: 'uppercase'}}>
                {provao.etiqueta.nome}
              </Typography>
              <CardMedia
                className={classes.image}
                image={cardImageBrand}
              />
              <Typography align="center" variant="h5" component="h2">
                {provao.colecao.colecao}
              </Typography>
              <Typography align="center" color="textSecondary">
                {provao.usuario ? provao.usuario.login : null}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      );
    });

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={4}
        wrap="wrap"
        alignContent="space-around"
      >
        {cards}
      </Grid>
    );
  }
  render() {
    const { provoes } = this.state

    return (<Container fixed>
      {provoes.length >1? this.renderProvoesToChoose():<Typography align="center">loading</Typography>}
    </Container>);
  }
}

const mapStateToProps = state => ({
  brandSelected: state.brands.selected,
  collectionSelected: state.collections.selected,
  roundDaySelected: state.roundDays.selected,
  loadingDialog: state.loadingDialog,
  rounds: state.rounds,
  authToken: state.authReducer.token
});

const mapDispatchToProps = dispatch => ({
  loadBrands: () => dispatch(loadBrands()),
  loadCollections: () => dispatch(loadCollections()),
  getRoundDays: () => dispatch(getRoundDays()),
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(withStyles(styles)(withRouter(EscolhaProvao))));
