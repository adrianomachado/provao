import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import LoadingDialog from "../components/common/dialogs/LoadingDialog";

import { toggleLoadingDialog } from "../actions";
import User from "../services/User";
import VoteFinished from "./VoteFinished";
import ProvaoApi from "../services/apis/ProvaoApi";
import LoadingCircle from "../components/common/LoadingCircle";

import VoteProductCard from "../components/VoteProductCard/VoteProductCard";
const styles = theme => ({
  root: {
    padding: theme.spacing(5.5)
  }
});

const RESULT_MSG = "Carregando próximo produto...";
const VOTE_MSG = "";
const ERROR_MESSAGE_VOTE_INVALID = "Problemas para salvar o voto "

class ProvaoOnline extends Component {
  constructor(props) {
    super(props);
    const user = new User().getUser();
    this.state = {
      title: RESULT_MSG,
      voteParams: [],
      productRounds: [],
      loading: true,
      waitingRound: false,
      voteFinished: false,
      user,
      votos:[],
      renderBtn: true,
      renderProductCard: true
    };
  }

  formatVotes(voteParams) {
    return voteParams.map(param => ({ ...param, voto: "" }));
  }
  async componentDidMount() {
    try {
      this.getProductToVote();
    } finally {
      // this.setState({ loading: false, voteParams });
      // this.listenProvaoChannel();
    }
  }

  async getProductToVote() {
    const user = new User();
    const { enqueueSnackbar } = this.props;

    try { 


    const data = await ProvaoApi.getRodadaProduto(
      user.user.id_marca_estilo,
      user.user.id_usuario
    );
    if (data.msg === "Você já votou em todos os produtos! Obrigado :)") {
      this.setState({loading:false, title: data.msg, renderBtn: false, renderProductCard: false})
     return  enqueueSnackbar("Todos os seus produtos já foram votados :)", { variant: "success" });

    } else { 

    }
    this.setState({
      voteParams: data.configuracoes,
      votos: data.produto.map(() => this.formatVotes(data.configuracoes)),
      productRounds: data.produto,
      waitingRound: false,
      title: VOTE_MSG,
      voteFinished: false,
      loading: false,
      renderBtn: true,
      renderProductCard: true
    });
  }
   catch(err) {
     console.log(err)
     this.setState({waitingRound: false,
    title: err.msg})
     console.log(err)
   }
  }
  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  }

  renderProducts() {
    const { productRounds } = this.state;
    const { votos } = this.state;
    return productRounds.map((round, index) => (
      <Grid item key={round.id_rodada_produto}>
        {this.renderProductCard(round.produto, votos[index], index)}
      </Grid>
    ));
  }

  renderProductCard(product, votes, index) {
    const { voteParams, renderProductCard } = this.state;
    const user = new User();
    if (renderProductCard) { 
      return (
        <VoteProductCard
          product={product}
          voteParams={voteParams}
          votes={votes}
          onChange={(value, name) => this.handleVote(value, name, index)}
          hideInput={!!user.canStartProvao()}
          userIsAtacado={false}
          //AUI TEM QUE FICAR TRUE 
          onlineVote={true}
        />
      );

    }
    return

  }

  handleVote(value, name, index) {
    const { votos } = this.state;
    const votesCopy = Array.from(votos);
    const votedIndex = votesCopy[index].findIndex(
      vote => vote.id_configuracao === name.id_configuracao
    );
    votesCopy[index][votedIndex].voto = value;
    this.setState({ votos: votesCopy });
  }
  getErrorsOnline(votos, product) {
    const invalidVotes = votos.filter(vote =>( vote.voto === "" && vote.fl_obrigatorio) && vote.fl_online  );
    return invalidVotes.map(invalidVote =>
      this.mountError(invalidVote.nome, product)
    );
  }
  mountError(field, product) {
    return `O ${field} do ${product} precisa ser preenchido!`;
  }

  getVotes() {
    const { productRounds } = this.state;
    const { votos } = this.state;
    return votos.map((voto, i) => ({
      votos_rodada_produto: voto,
      id_rodada_produto: productRounds[i].id_rodada_produto,
      errors:this.getErrorsOnline(voto, productRounds[i].produto.desc_produto) 
    }));
  }
  

  transformDataToVote(data) {
    const user = new User().getUser();
    return {
      id_usuario: user.id_usuario,
      lista_de_votos: data
    };
  }
  isVoteValid(votos) {
    const errors = votos.flatMap(voto => voto.errors);
    return errors.length === 0;
  }

  showErrors(errors) {
    const { enqueueSnackbar } = this.props;
    errors.forEach(error => enqueueSnackbar(error, { variant: "warning" }));
  }

  handleEndVote() {

    const votos = this.getVotes();
    if (!this.isVoteValid(votos)) {
      const errors = votos.flatMap(voto => voto.errors);
      this.showErrors(errors);
      return;
    }
    ProvaoApi.vote(this.transformDataToVote(votos))
      .then(() => {
        this.setState({ title: RESULT_MSG,loading:true });
        // onVote();
        // socket.emit('endVote',user.user.login)
        
        this.getProductToVote()
      })
      .catch(() =>
        this.props.enqueueSnackbar(ERROR_MESSAGE_VOTE_INVALID, {
          variant: "error"
        })
      );
      // this.getProductToVote()
  }

  renderScreen() {
    const {
      voteFinished,
      loading,
      productRounds,
      votos
    } = this.state;

    if (loading) {
      return <LoadingCircle loading={loading} />;
    }

    if (voteFinished) {
      return (
        <VoteFinished onClick={() => this.setState({ voteFinished: false })} />
      );
    }

    return productRounds.map((round, index) => (
      <Grid item key={round.id_rodada_produto}>
        {this.renderProductCard(round.produto, votos[index], index)}
      </Grid>
    ));

  }

  changeTitle(title) {
    this.setState({ title });
  }

  renderBtn() {
    const { renderBtn } = this.state
    if(renderBtn) { 
      return (
        <Button
          variant={"contained"}
          color={"primary"}
          onClick={() => this.handleEndVote()}
        >
          Próximo produto
        </Button>
      );

    } 
    return 

  }
  render() {
    const { loadingDialog, classes } = this.props;
    const { title } = this.state;
    return (
      <Container fixed className={classes.root}>
        <LoadingDialog
          open={loadingDialog.open}
          message={loadingDialog.message}
        />

        <Grid container justify={"center"} spacing={2}>
          <Grid container item justify={"center"}>
            <Typography variant={"h5"} color={"textSecondary"}>
              {title}
            </Typography>
          </Grid>
          <Grid container justify="center" spacing={2}>
           {this.renderScreen()}
           <Grid container item justify="center" spacing={2} >
          {this.renderBtn()}
          </Grid>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoOnline))
);

const mapStateToProps = state => ({
  loadingDialog: state.loadingDialog
});

const mapDispatchToProps = dispatch => ({
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapperComponent);
