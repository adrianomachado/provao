import React, { Component } from "react";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import MaterialTable from "material-table";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Build from "@material-ui/icons/Build";
import Visibility from "@material-ui/icons/Visibility";
import withStyles from "@material-ui/core/styles/withStyles";
import Switch from "@material-ui/core/Switch";

import ProvaoApi from "../services/apis/ProvaoApi";
import User from "../services/User";
import {
  PROVAO_ORDER_ROUTE,
  PROVAO_CONFIG_ROUTE,
  PROVAO_ADD_ROUTE
} from "../consts";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  },
  tableDiv: {
    width: "100%",
    padding: theme.spacing(4)
  },
  autocomplete: {
    minWidth: "300px"
  }
});

class ProvaoManagement extends Component {
  state = {
    provoes: [],
    loading: true,
    availableToOnline: false
  };

  componentDidMount() {
    const user = new User();
    this.getProvoes(user.getUser());
  }

  async getProvoes(user) {
    try {
      const provoes = await ProvaoApi.get({ id_marca: user.id_marca_estilo })
      this.setState({ ...provoes, loading: false })

    } catch(err) {
      this.setState({ loading:false })
    }

  }

  redirect(url, rowData) {
    if (rowData) {
      const data = {
        etiqueta: JSON.stringify(rowData.etiqueta),
        colecao: JSON.stringify(rowData.colecao),
        nome: rowData.nome
      };
      const searchParams = new URLSearchParams(data);
      return this.props.history.push(`${url}?${searchParams.toString()}`);
    }
    return this.props.history.push(url);
  }

  mountURL(url, idProvao) {
    return url.replace(":id_provao", idProvao);
  }
  makeAvailable = (name, id_provao) => async event => {
    const user = new User();
    event.persist();
    try {
      ProvaoApi.makeProvaoAvailable(id_provao).then(result => {
        // const provoes = this.state.provoes.map(provao => {
        //   if (provao.id_provao === id_provao) {
        //     provao.fl_provao_online = result.provao[0].fl_provao_online === 1? 0 : 1
        //     return provao
        //   }
        //   else  {
        //     return provao
        //   }
        // })
        this.getProvoes(user.getUser());

        // this.setState({ ...this.state, [name]: event.target.checked });
      });
    } catch (err) {
      console.log(err);
      this.setState({loading:false})

      alert("Problemas para disponibilizar provão");
    }
  };

  render() {
    const adminOptions = [
      {
        icon: () => <Build color={"primary"}></Build>,
        tooltip: "Editar Configurações",
        onClick: (event, rowData) =>
          this.redirect(this.mountURL(PROVAO_CONFIG_ROUTE, rowData.id_provao))
      },
      {
        icon: () => <Visibility color={"primary"} />,
        tooltip: "Ver rodadas",
        onClick: (event, rowData) =>
          this.redirect(
            this.mountURL(PROVAO_ORDER_ROUTE, rowData.id_provao),
            rowData
          )
      },
      {
        icon: "add",
        tooltip: "Adicionar Provão",
        isFreeAction: true,
        onClick: event => this.redirect(PROVAO_ADD_ROUTE)
      }
    ];

    const plannerOptions = [
      {
        icon: () => <Visibility color={"primary"} />,
        tooltip: "Ver rodadas",
        onClick: (event, rowData) =>
          this.redirect(
            this.mountURL(PROVAO_ORDER_ROUTE, rowData.id_provao),
            rowData
          )
      },
      {
        icon: "add",
        tooltip: "Adicionar Provão",
        isFreeAction: true,
        onClick: event => this.redirect(PROVAO_ADD_ROUTE)
      }
    ];
    const { classes } = this.props;
    const user = new User();
    let columnsList = [
      { title: "Nome", field: "nome" },
      { title: "Marca", field: "etiqueta.nome" },
      { title: "Coleção", field: "colecao.colecao" },
      { title: "Criador", field: "usuario.login" }
    ];
    //adicionando coluna de provão online se usuário for admin
    if (user.isAdmin()) {
      columnsList.push({
        title: "Provão Online",
        field: "fl_provao_online",
        render: rowData => (
          <Switch
            checked={rowData.fl_provao_online === 1 ? true : false}
            onChange={this.makeAvailable(
              "availableToOnline",
              rowData.id_provao
            )}
            // value="rowData.id_provao"
            color="primary"
            name={rowData.id_provao}
          />
        )
      });
    }

    return (
      <Container fixed>
        <Grid container spacing={2} className={classes.root}>
          <div className={classes.tableDiv}>
            <MaterialTable
              title={"Provões"}
              data={this.state.provoes}
              columns={columnsList}
              actions={user.isAdmin() ? adminOptions : plannerOptions}
              options={{
                actionsColumnIndex: -1
              }}
              isLoading={this.state.loading}
            ></MaterialTable>
          </div>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoManagement))
);

export default wrapperComponent;
