import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import withStyles from "@material-ui/core/styles/withStyles";

import LoadingDialog from "../components/common/dialogs/LoadingDialog";

import { toggleLoadingDialog } from "../actions";
import User from "../services/User";
import RoundTrialApi from "../services/apis/RoundTrialApi";
import ProvaoApi from "../services/apis/ProvaoApi";

import Resume from "./Resume";
import LoadingCircle from "../components/common/LoadingCircle";

import {  RESULTADOS_PROVOES_ROUTE } from "../consts";

import Paper from "@material-ui/core/Paper";
import SelectUi from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

const styles = theme => ({
  root: {
    padding: theme.spacing(5.5),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(0),
    display: "flex"
  },
  paper: {
    padding: theme.spacing(1),
    marginTop: theme.spacing(2)
    // marginLeft: theme.spacing(2)

    // width: "32cm",
    // minHeight:" 46.85cm",
    // padding: "1cm",
    // margin: "0 auto",
    // border: "1px #D3D3D3 solid",
    // borderRadius: "5px",
    // background: "white",
    // boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
  },
  row: {
    // marginLeft: theme.spacing(10)
  }
});

const RESULT_MSG = "Resultados da rodada...";

class ResultadosProvoes extends Component {
  constructor(props) {
    super(props);
    const user = new User().getUser();
    this.state = {
      title: RESULT_MSG,
      voteParams: [],
      productRounds: [],
      loading: true,
      waitingRound: false,
      voteFinished: false,
      user,
      activeDay: 1,
      rounds: []
    };

    this.listDays = [
      { label: 1, value: 1 },
      { label: 2, value: 2 },
      { label: 3, value: 3 },
      { label: 4, value: 4 },
      { label: 5, value: 5 },
      { label: 6, value: 6 },
      { label: 7, value: 7 },
      { label: 8, value: 8 },
      { label: 9, value: 9 },
      { label: 10, value: 10 }
    ];
  }

  renderSelect({ items, ...props }) {
    return (
      <SelectUi {...props}>
        {items.map(item => (
          <MenuItem key={item.label} value={item.value}>
            {item.label}
          </MenuItem>
        ))}
      </SelectUi>
    );
  }

  async componentDidMount() {
    try {
      var voteParams = await this.getProvaoConfig();
      const rounds = await RoundTrialApi.getRoundsByDay(
        this.getIdProvao(),
        this.getParamFromUrl("dia")
      );
      const provao = await ProvaoApi.getById(this.getIdProvao())
      this.setState({provao:provao.provao})

      this.setState({ rounds });
      this.setActiveDay();
    } catch(err) {
      console.log(err)
    }finally {
      this.setState({ loading: false, voteParams });
    }
  }

  getParamFromUrl(param) {
    const urlSearch = new URLSearchParams(this.props.location.search);
    return parseInt(urlSearch.get(param));
  }

  setActiveDay() {
    this.setState({ activeDay: this.getParamFromUrl("dia") });
  }

  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  }

  async getProvaoConfig() {
    const { configuracoes } = await ProvaoApi.getConfigs(this.getIdProvao());
    return configuracoes.map(config => ({
      id_configuracao_provao: config.id_configuracao_provao,
      ...config.configuracoes
    }));
  }

  renderScreen() {
    const { loading, voteParams } = this.state;
    const { classes } = this.props;

    if (loading) {
      return <LoadingCircle loading={loading} />;
    }
    return (
      <Grid container direction="column" className={classes.row}>
        {this.renderHeader()}
        {this.state.rounds.length >= 1 ? (
          this.state.rounds.map(round => {
            return (
              <Paper className={classes.paper}>
                <Resume
                  voteParams={voteParams}
                  id_rodada_results={round.id_rodada}
                  provao={"resultado"}
                  onError={() => {}}
                  resumeProvaoPage={true}
                />
              </Paper>
            );
          })
        ) : (
          <Grid container justify="center" alignItems="center">
            
            <Typography  style={{marginTop: 20}}variant={"h6"} color={"textPrimary"}>
              Nenhuma rodada foi votada para esse dia de provão
            </Typography>
          </Grid>
        )}
      </Grid>
    );
  }

  renderBackBtn() {
    return (
      <Grid container item justify={"flex-end"}>
        <Button
          variant={"contained"}
          color={"primary"}
          onClick={() => this.props.history.goBack()}
        >
          Voltar
        </Button>
      </Grid>
    );
  }

  renderHeader() {
    const { activeDay } = this.state;
    console.log(activeDay);
    return (
      <Grid container direction="row" spacing={6} justify="center">
        <Grid item>
          <Fragment>
            <Typography
              variant={"caption"}
              color={"textSecondary"}
              gutterBottom
            >
              DIA
            </Typography>
            <Typography variant={"h6"} color={"textPrimary"}>
              <this.renderSelect
                items={this.listDays}
                value={activeDay}
                onChange={(e, obj) => {
                  let url = RESULTADOS_PROVOES_ROUTE;
                  url = RESULTADOS_PROVOES_ROUTE.replace(
                    ":id_provao",
                    this.getIdProvao()
                  );
                  url += `?dia=${obj.props.value}`;
                  this.props.history.push(url);
                }}
              />
            </Typography>
          </Fragment>
        </Grid>
        <Grid item>
          <Fragment>
            <Typography
              variant={"caption"}
              color={"textSecondary"}
              gutterBottom
            >
              PROVÃO
            </Typography>
            <Typography variant={"h6"} color={"textPrimary"}>
              {this.state.provao?this.state.provao.nome: null}
            </Typography>
          </Fragment>
        </Grid>

        <Grid item>
          <Fragment>
            <Typography
              variant={"caption"}
              color={"textSecondary"}
              gutterBottom
            >
              MARCA
            </Typography>
            <Typography variant={"h6"} color={"textPrimary"}>
              {this.state.provao? this.state.provao.etiqueta.nome: null}
            </Typography>
          </Fragment>
        </Grid>

        <Grid item>
          <Fragment>
            <Typography
              variant={"caption"}
              color={"textSecondary"}
              gutterBottom
            >
              COLEÇÃO
            </Typography>
            <Typography variant={"h6"} color={"textPrimary"}>
              {this.state.provao? this.state.provao.colecao.colecao: null}
            </Typography>
          </Fragment>
        </Grid>
      </Grid>
    );
  }

  render() {
    const { loadingDialog, classes } = this.props;
    return (
      <Fragment>
        <Container fixed className={classes.root}>
          <LoadingDialog
            open={loadingDialog.open}
            message={loadingDialog.message}
          />
          {/* {this.renderHeader( )} */}
          <Grid container justify={"flex-start"} spacing={2}>
            {/* {this.renderBackBtn()} */}

            {this.renderScreen()}
          </Grid>
        </Container>
      </Fragment>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ResultadosProvoes))
);

const mapStateToProps = state => ({
  loadingDialog: state.loadingDialog
});

const mapDispatchToProps = dispatch => ({
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(mapStateToProps, mapDispatchToProps)(wrapperComponent);
