import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import LoadingDialog from "../components/common/dialogs/LoadingDialog";

import { toggleLoadingDialog } from "../actions";
import Socket from "../services/Socket";
import User from "../services/User";
import VoteFinished from "./VoteFinished";
import ProvaoApi from "../services/apis/ProvaoApi";
import Resume from "./Resume";
import Vote from "./Vote";
import LoadingCircle from "../components/common/LoadingCircle";

const styles = theme => ({
  root: {
    padding: theme.spacing(5.5)
  }
});

const RESULT_MSG = "Resultados da última votação...";
const VOTE_MSG = "Votação";

class ProvaoVote extends Component {
  constructor(props) {
    super(props);
    const user = new User().getUser();
    this.state = {
      title: RESULT_MSG,
      voteParams: [],
      productRounds: [],
      loading: true,
      waitingRound: false,
      voteFinished: false,
      user
    };
  }

  async componentDidMount() {
    try {
      var voteParams = await this.getProvaoConfig();
      await this.tryGetActiveRound();
    } finally {
      this.setState({ loading: false, voteParams });
      this.listenProvaoChannel();
    }
  }

  async tryGetActiveRound() {
    try {
      const productRounds = await this.getRoundActive();
      this.setState({
        productRounds,
        title: VOTE_MSG,
        id_rodada: productRounds[0].id_rodada
      });
    } catch (e) {
      this.setState({ waitingRound: true });
    }
  }

  listenProvaoChannel() {
    const socket = new Socket();
    socket.listen(`provao_${this.getIdProvao()}`, async data => {
      if (data.votacao) {
        this.mountRound();
      } else {
        this.setState({
          waitingRound: true,
          voteFinished: false,
          title: RESULT_MSG
        });
      }
    });
  }

  async mountRound() {
    const productRounds = await this.getRoundActive();
    this.setState({
      productRounds,
      waitingRound: false,
      title: VOTE_MSG,
      voteFinished: false
    });
  }

  getIdProvao() {
    const { match } = this.props;
    return parseInt(match.params.id_provao);
  }

  orderConfigs({configuracoes}) {
    console.log(configuracoes.nome)
    if(configuracoes.nome.includes("ota")) {
      return -1
      
    } 
    if (configuracoes.nome.includes("bserva")) {
      return 1
    }

    return 0 


  }

  async getProvaoConfig() {
    const { configuracoes } = await ProvaoApi.getConfigs(this.getIdProvao());
    configuracoes.sort(this.orderConfigs)
    return configuracoes.map(config => ({
      id_configuracao_provao: config.id_configuracao_provao,
      ...config.configuracoes
    }));
  }

  getRoundActive() {
    return ProvaoApi.getActiveRound(this.getIdProvao()).then(
      data => data[0].rodada_produtos
    );
  }

  renderScreen() {
    const {
      waitingRound,
      voteFinished,
      loading,
      voteParams,
      productRounds
    } = this.state;

    if (loading) {
      return <LoadingCircle loading={loading} />;
    }
    
    if (waitingRound) {
      return (
        <Resume
          id_rodada_results={this.state.id_rodada}
          voteParams={voteParams}
          provao={this.getIdProvao()}
          onError={title => this.changeTitle(title)}
        />
      );
    }


    return (
      <Vote
        voteParams={voteParams}
        productRounds={productRounds}
        onVote={() => this.setState({ voteFinished: true, title: "" })}
        onEndRound={(id_rodada) =>
          this.setState({
            waitingRound: true,
            title: RESULT_MSG,
            id_rodada
          })
        }
      />
    );
  }

  changeTitle(title) {
    this.setState({ title });
  }

  render() {
    const { loadingDialog, classes } = this.props;
    const { title } = this.state;
    return (
      <Container fixed className={classes.root}>
        <LoadingDialog
          open={loadingDialog.open}
          message={loadingDialog.message}
        />

        <Grid container justify={"center"} spacing={2}>
          <Grid container item justify={"center"}>
            <Typography variant={"h5"} color={"textSecondary"}>
              {title}
            </Typography>
          </Grid>

          {this.renderScreen()}
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoVote))
);

const mapStateToProps = state => ({
  loadingDialog: state.loadingDialog
});

const mapDispatchToProps = dispatch => ({
  toggleLoadingDialog: (open, message) =>
    dispatch(toggleLoadingDialog(open, message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapperComponent);
