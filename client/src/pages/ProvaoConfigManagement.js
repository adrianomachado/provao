import React, { Component } from "react";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import ProvaoApi from "../services/apis/ProvaoApi";
import RadioList from "../components/common/RadioList";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  },
  paper: {
    width: "100%",
    padding: theme.spacing(4)
  }
});

const SAVE_CONFIG_MESSAGE_SUCCESS = "Configurações salvas com sucesso!";
const SAVE_CONFIG_MESSAGE_FAILURE = "Não foi possivel salvar as configurações.";

class ProvaoConfigManagement extends Component {
  state = {
    configurations: []
  };

  componentDidMount() {
    Promise.all([this.getProvaoConfigs(), this.getAvailableConfigs()]).then(
      ([provaoConfig, availableConfigs]) => {
        const provaoConfigIds = provaoConfig.configuracoes.map(
          config => config.id_configuracao
        );
        const availableConfigsChecked = this.mapCheckedConfigs(
          availableConfigs.configuracoes,
          provaoConfigIds
        );
        this.setState({configurations: availableConfigsChecked});
      }
    );
  }

  mapCheckedConfigs(configs, ids) {
    return configs.map(config => ({
      ...config,
      checked: ids.includes(config.id_configuracao)
    }));
  }

  getAvailableConfigs() {
    return ProvaoApi.getAvailableConfigs();
  }

  getProvaoConfigs() {
    return ProvaoApi.getConfigs(this.getProvaoId());
  }

  handleChange(checked, index) {
    const configurationsCopy = Array.from(this.state.configurations);
    configurationsCopy[index].checked = checked;
    this.setState({ configurations: configurationsCopy });
  }

  handleSave() {
    const { enqueueSnackbar } = this.props;
    const { configurations } = this.state;
    const provaoId = this.getProvaoId();
    const configClickeds = configurations.filter(config => config.checked);
    ProvaoApi.saveConfigs(provaoId, configClickeds)
      .then(() =>
        enqueueSnackbar(SAVE_CONFIG_MESSAGE_SUCCESS, { variant: "success" })
      )
      .then(() => this.props.history.push('/gestao_provao'))
      .catch(() =>
        enqueueSnackbar(SAVE_CONFIG_MESSAGE_FAILURE, { variant: "error" })
      );
  }

  redirectToBackward() {
    const { history } = this.props;
    history.goBack();
  }

  getProvaoId() {
    const { match } = this.props;
    return match.params.id_provao;
  }

  renderQuestion(question, answers) {
    return (
      <Grid container alignItems={"center"} spacing={4}>
        <Grid item>
          <Typography variant={"caption"}>{question}</Typography>
        </Grid>
        <Grid item>
          {this.renderAnswers(answers ? answers.split(";") : [])}
        </Grid>
      </Grid>
    );
  }

  renderAnswers(answers) {
    return (
      <RadioList
        data={answers.map(answer => ({ label: answer, value: answer }))}
        padding={"dense"}
        row={true}
        disabled={true}
      />
    );
  }

  renderCheckboxDefault(config, index) {
    if (!config.fl_atacado && !config.fl_online) {
      return (
        <FormGroup
          key={config.id_configuracao}
          name="position"
          value={config.id_configuracao}
          onChange={e => this.handleChange(e.target.checked, index)}
          row
        >
          <FormControlLabel
            value={config.id_configuracao}
            control={<Checkbox color="primary" checked={config.checked}/>}
            label={this.renderQuestion(config.nome, config.valores)}
            labelPlacement="end"
          />
        </FormGroup>
      );

    }

  }
  renderCheckboxAtacado(config, index) {
    if (config.fl_atacado ===1) {
      return (
        <FormGroup
          key={config.id_configuracao}
          name="position"
          value={config.id_configuracao}
          onChange={e => this.handleChange(e.target.checked, index)}
          row
        >
          <FormControlLabel
            value={config.id_configuracao}
            control={<Checkbox color="primary" checked={config.checked}/>}
            label={this.renderQuestion(config.nome, config.valores)}
            labelPlacement="end"
          />
        </FormGroup>
      );

    }

  }
  renderCheckboxOnline(config, index) {
    if (config.fl_online === 1) {
      return (
        <FormGroup
          key={config.id_configuracao}
          name="position"
          value={config.id_configuracao}
          onChange={e => this.handleChange(e.target.checked, index)}
          row
        >
          <FormControlLabel
            value={config.id_configuracao}
            control={<Checkbox color="primary" checked={config.checked}/>}
            label={this.renderQuestion(config.nome, config.valores)}
            labelPlacement="end"
          />
        </FormGroup>
      );

    }

  }

  render() {
    const { classes } = this.props;
    return (
      <Container fixed>
        <Grid container spacing={2} className={classes.root}>
          <Paper className={classes.paper}>
            <Typography variant={"h6"}>Configuração Provão</Typography>
            <br />
            <Typography variant={"body1"} color={"textSecondary"}>
              Selecione as perguntas do seu provão:
            </Typography>

            {this.state.configurations.map((config, i) =>
              this.renderCheckboxDefault(config, i)
            )}
            <br />
            <Typography variant={"h6"}>Votos Usuário Atacado</Typography>
            {this.state.configurations.map((config, i) =>
              this.renderCheckboxAtacado(config, i)
            )}
            <br />
            <Typography variant={"h6"}>Votos Usuário Online</Typography>
            {this.state.configurations.map((config, i) =>
              this.renderCheckboxOnline(config, i)
            )}
            <br />
            <Grid container spacing={2}>
              <Grid item>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => this.redirectToBackward()}
                >
                  Cancelar
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleSave()}
                >
                  Salvar
                </Button>
              </Grid>

            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoConfigManagement))
);

export default wrapperComponent;
