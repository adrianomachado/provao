import React, { Component } from "react";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import VoteOnlineProductCard from "../components/VoteOnlineProductCard/VoteOnlineProductCard";

import DuelApi from "../services/apis/DuelApi";
import ProgressBar from '../components/ProgressBar/ProgressBar';
import LinearProgress from "@material-ui/core/LinearProgress";

const styles = theme => ({
  root: {
    marginTop: theme.spacing(3)
  },
  gridProgress: {
    width: '30vw',
  },
  loadProgress: {
    width: '30%'
  }
});

// Constante para definir a meta de votos para cada usuário
const VOTE_GOAL = 500;

class ProvaoOnline extends Component {
  state = {
    duels: [],
    tournament: null,
    activeDuel: 0,
    votes: 0,
    loading: true,
  };

  componentDidMount() {
    this.getDuels();
  }

  getDuels() {
    DuelApi.get()
      .then(data => this.checkIfDuelsAreEmptyAndWarn(data))
      .then(data =>
        this.setState({
          duels: data.duelos,
          tournament: data.id_torneios_provoes,
          votes: data.votos,
          loading: false
        }
      )
    );
  }

  checkIfDuelsAreEmptyAndWarn(data) {
    const { enqueueSnackbar } = this.props;
    if (data.duelos.length === 0) {
      enqueueSnackbar( "Não existem duelos ativos.",
        { variant: "error" });
    }
    return data;
  }

  mapToSave(duels) {
    const { tournament } = this.state;
    return {
      id_torneios_provoes: tournament,
      duelos: duels.map(duel => ({
        id_duelos_provoes: duel.id_duelos_provoes,
        id_produto_vencedor: duel.id_produto_vencedor
      }))
    };
  }

  handleVote(productID) {
    const { activeDuel, duels } = this.state;

    const duelsCopy = Array.from(duels);
    const activeDuelIncremmented = activeDuel + 1;
    duelsCopy[activeDuel].id_produto_vencedor = productID;

    this.setState({
      duels: duelsCopy,
      activeDuel: activeDuelIncremmented
    });

    if (this.shouldGetMoreDuels(activeDuelIncremmented, duels)) {
      this.requestMoreDuels(duelsCopy);
    }
  }

  shouldGetMoreDuels(activeDuel, duels) {
    return activeDuel === duels.length;
  }

  requestMoreDuels(duels) {
    this.setState({loading: true});
    DuelApi.post(this.mapToSave(duels))
      .then(data => this.checkIfDuelsAreEmptyAndWarn(data))
      .then(data =>
        this.setState({
          duels: data.duelos,
          tournament: data.id_torneios_provoes,
          votes: data.votos,
          activeDuel: 0,
          loading: false
        })
      );
  }

  renderVote() {
    if (this.isFetching() || this.state.duels.length === 0) {
      return this.renderWaitScreen();
    }
    return (
      <>
        {this.renderVoteBar()}
        <Grid container item direction={"row"} justify={"center"} spacing={1}>
          {this.renderProducts()}
        </Grid>
      </>
    );
  }

  isFetching() {
    return this.state.loading;
  }

  renderWaitScreen() {
    const { classes } = this.props;
    return (
      <>
        <Grid container justify={"center"}>
          <Typography variant={"overline"} color={"textSecondary"}>
            Pegando mais produtos para votar...
          </Typography>
        </Grid>
        <Grid container justify={'center'}>
          <LinearProgress className={classes.loadProgress}/>
        </Grid>
      </>
    );
  }

  renderVoteBar() {
    const { votes } = this.state;
    const { classes } = this.props;
    return (
      <Grid container item justify={"center"} alignItems={'center'} spacing={2}>
        <Grid item>
          <Typography variant={"caption"} color={'textSecondary'}>
            Meta de votos:
          </Typography>
        </Grid>
        <Grid item className={classes.gridProgress}>
          <ProgressBar
            value={votes}
            max={VOTE_GOAL}
          />
        </Grid>

      </Grid>
    );
  }

  renderProducts() {
    const { duels, activeDuel } = this.state;
    return duels[activeDuel].produtos.map(product => (
      <Grid item key={product.id_produto_estilo} xs={6} md={'auto'}>
        <VoteOnlineProductCard
          product={product}
          handleVote={() => this.handleVote(product.id_produto_estilo)}
        />
      </Grid>
    ));
  }

  render() {
    const { classes } = this.props;
    return (
      <Container fixed className={classes.root}>
        <Grid container spacing={2}>

          <Grid container item justify={"center"}>
            <Typography variant={"h6"}>
              Qual produto vende mais?
            </Typography>
          </Grid>
          {this.renderVote()}
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(ProvaoOnline)
);

export default wrapperComponent;
