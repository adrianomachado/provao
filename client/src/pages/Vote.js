import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";

import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import VoteProductCard from "../components/VoteProductCard/VoteProductCard";
import ProvaoApi from "../services/apis/ProvaoApi";
import User from "../services/User";

import io from 'socket.io-client';
import {BASE_URL} from "../consts"
import VoteFinished from "./VoteFinished";
const styles = theme => ({});

const ERROR_MESSAGE_VOTE_INVALID = "Não foi possível computar o voto.";

class Vote extends Component {
  constructor({ voteParams, productRounds }) {
    super();
    this.state = {
      votos: productRounds.map(() => this.formatVotes(voteParams)),
      activeRound: productRounds[0].id_rodada,
      userList: [],
      voteFinished: false,
    };
  }
  componentDidMount() {
    const socket = io("/",{
      // transports: ['websocket','polling'],
      transports: ['websocket','polling'],

      forceNew: true
    })
    socket.on('reconnect_attempt', () => {
      socket.io.opts.transports = ['polling', 'websocket'];
    });
    this.setState({socket})
    const user = new User();
      socket.emit('adduser',{user: user.user.login, provao: this.getProvaoId(), voto:false})


    socket.on('updatechat', (usernames) => { 
      this.transformObjectToarray(usernames)
    })      
  }

  transformObjectToarray(usernames) {
    const listName = Object.keys(usernames)
    const userList = listName.map(user => { 
      return usernames[user]
    })
    this.setState({userList})


  }

  getProvaoId() {
    const { match } = this.props;
    return match.params.id_provao;
  }

  formatVotes(voteParams) {
    return voteParams.map(param => ({ ...param, voto: "" }));
  }

  handleVote(value, name, index) {
    const { votos } = this.state;
    const votesCopy = Array.from(votos);
    const votedIndex = votesCopy[index].findIndex(
      vote => vote.id_configuracao === name.id_configuracao
    );
    votesCopy[index][votedIndex].voto = value;
    this.setState({ votos: votesCopy });
  }

  handleEndVote() {
    const { onVote } = this.props;
    const { socket } = this.state
    const user = new User();

    const votos = this.getVotes();
    if (!this.isVoteValid(votos)) {
      const errors = votos.flatMap(voto => voto.errors);
      this.showErrors(errors);
      return;
    }
    ProvaoApi.vote(this.transformDataToVote(votos))
      .then(() => {
        onVote();
        socket.emit('endVote',user.user.login)
        this.setState({ title: "" ,voteFinished:true});
      })
      .catch(() =>
        this.props.enqueueSnackbar(ERROR_MESSAGE_VOTE_INVALID, {
          variant: "error"
        })
      );
  }

  isVoteValid(votos) {
    const errors = votos.flatMap(voto => voto.errors);
    return errors.length === 0;
  }

  getErrors(votos, product) {
    const invalidVotes = votos.filter(vote =>( vote.voto === "" && vote.fl_obrigatorio) && !vote.fl_atacado && !vote.fl_online );
    return invalidVotes.map(invalidVote =>
      this.mountError(invalidVote.nome, product)
    );
  }

  getErrorsAtacado(votos, product) {
    const invalidVotes = votos.filter(vote =>( vote.voto === "" && vote.fl_obrigatorio) && vote.fl_atacado  );
    return invalidVotes.map(invalidVote =>
      this.mountError(invalidVote.nome, product)
    );
  }

  getErrorsOnline(votos, product) {
    const invalidVotes = votos.filter(vote =>( vote.voto === "" && vote.fl_obrigatorio) && vote.fl_online  );
    return invalidVotes.map(invalidVote =>
      this.mountError(invalidVote.nome, product)
    );
  }

  mountError(field, product) {
    return `O ${field} do ${product} precisa ser preenchido!`;
  }

  showErrors(errors) {
    const { enqueueSnackbar } = this.props;
    errors.forEach(error => enqueueSnackbar(error, { variant: "warning" }));
  }

  getVotes() {
    const { productRounds } = this.props;
    const { votos } = this.state;
    const user = new User()
    return votos.map((voto, i) => ({
      votos_rodada_produto: voto,
      id_rodada_produto: productRounds[i].id_rodada_produto,
      errors: user.isAtacado()?this.getErrorsAtacado(voto, productRounds[i].produto.desc_produto) : this.getErrors(voto, productRounds[i].produto.desc_produto)
    }));
  }

  transformDataToVote(data) {
    const user = new User().getUser();
    return {
      id_usuario: user.id_usuario,
      lista_de_votos: data
    };
  }

  async handleEndRound() {
    const { onEndRound, enqueueSnackbar } = this.props;
    const { activeRound } = this.state;
    try {
      const {rodada_finalizada} = await ProvaoApi.endRound(activeRound);
      onEndRound(rodada_finalizada.id_rodada);
    } catch (e) {
      enqueueSnackbar("Não foi possível finalizar a rodada.", {
        variant: "error"
      });
    }
  }

  renderProducts() {
    const { productRounds } = this.props;
    const { votos } = this.state;
    return productRounds.map((round, index) => (
      <Grid item key={round.id_rodada_produto}>
        {this.renderProductCard(round.produto, votos[index], index)}
      </Grid>
    ));
  }

  renderProductCard(product, votes, index) {
    const { voteParams } = this.props;
    const user = new User();

    return (
      <VoteProductCard
        product={product}
        voteParams={voteParams}
        votes={votes}
        onChange={(value, name) => this.handleVote(value, name, index)}
        hideInput={!!user.canStartProvao()}
        userIsAtacado={!!user.isAtacado()}

      />
    );
  }

  renderBtn() {
    const user = new User();
    if (user.canStartProvao()) {
      return (
        <Button
          variant={"contained"}
          color={"primary"}
          onClick={() => this.handleEndRound()}
        >
          Finalizar Rodada
        </Button>
      );
    }
    return (
      <Button
        variant={"contained"}
        color={"primary"}
        onClick={() => this.handleEndVote()}
      >
        Concluir votação
      </Button>
    );
  }
  renderUserOnlineList() {
    const USER = new User()
      return (
        <List dense style={{
          width: '100%',
          maxWidth: 360,
          backgroundColor: '#fafafa',
        }}>
          {this.state.userList.map(user => {
            const labelId = `checkbox-list-secondary-label-${user.user}`;
            if(user.user === USER.user.login ) return null 
            return (
              <ListItem key={user.user} button>
                <ListItemText id={labelId} primary={`${user.user}`} />
                <ListItemSecondaryAction>
                  <Checkbox
                    edge="end"
                    onChange={() => { }}
                    checked={user.voto}
                    inputProps={{ 'aria-labelledby': labelId }}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      );

    

  }
  render() {
    const user = new User()
    if (this.state.voteFinished) {
      return <VoteFinished onClick={() => this.setState({ voteFinished: false })}/>
    }
    return (
      <>
        <Grid container item spacing={3}>
          {this.renderProducts()}

        </Grid>
        {user.canStartProvao()? this.renderUserOnlineList() : null }

        <Grid container item justify={"flex-end"}>
          {this.renderBtn()}
        </Grid>
      </>
    );
  }
}

const wrapperComponent = withStyles(styles)(withSnackbar(withRouter(Vote)));

wrapperComponent.propTypes = {
  productRounds: PropTypes.array.isRequired,
  voteParams: PropTypes.array.isRequired,
  onVote: PropTypes.func.isRequired,
  onEndRound: PropTypes.func.isRequired
};

export default wrapperComponent;
