import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  waitContainer: {
    marginTop: theme.spacing(5)
  }
}));

export default function VoteFinished({ onClick }) {
  const classes = useStyles();
  return (
    <Grid container className={classes.waitContainer} spacing={2}>
      <Grid container item justify={"center"}>
        <Typography variant={"h5"} color={"textSecondary"}>
          Voto Computado! Aguarde os outros participantes votarem...
        </Typography>
      </Grid>
      <Grid container item justify={"center"}>
        <Button
          variant={"outlined"}
          color={"primary"}
          onClick={onClick}
        >
          Revisar voto
        </Button>
      </Grid>
    </Grid>
  );
}

VoteFinished.propTypes = {
  onClick: PropTypes.func.isRequired
};
