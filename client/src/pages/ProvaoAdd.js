import React, { Component } from "react";
import {connect} from "react-redux";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";

import ProvaoApi from "../services/apis/ProvaoApi";
import User from "../services/User";
import FieldSelect from "../components/common/FieldSelect";
import TextField from "../components/common/TextField";

import {
  loadCollections,
  loadTags
} from "../actions";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  },
  paper: {
    width: "100%",
    padding: theme.spacing(4)
  }
});

// const SAVE_CONFIG_MESSAGE_SUCCESS = "Configurações salvas com sucesso!";
const SAVE_CONFIG_MESSAGE_FAILURE = "Não foi possivel salvar as configurações.";

class ProvaoAdd extends Component {
  state = {
    tagSelected: '',
    collectionSelected: '',
    name: '',
  };

  componentDidMount() {
    const { enqueueSnackbar } = this.props;
    this.props
      .loadCollections()
      .catch(() =>
        enqueueSnackbar("Falha ao puxar Coleções.", { variant: "error" })
      );
    this.props
      .loadTags()
      .catch(() =>
        enqueueSnackbar("Falha ao puxar as Etiquetas.", { variant: "error" })
      );
  }

  mapToOptions(options, label, value) {
    return options.map(option => ({
      ...option,
      label: option[label],
      value: option[value]
    }));
  }

  renderSelect(name, field, { options, label, value }) {
    return (
      <FieldSelect
        name={name}
        value={this.state[field]}
        options={this.mapToOptions(options, label, value)}
        onChange={value => this.handleChange(value, field)}
      />
    );
  }

  handleChange = (value, field) => {
    this.setState({ [field]: value });
  };

  handleSave() {
    const { enqueueSnackbar } = this.props;
    const { tagSelected, collectionSelected, name } = this.state;
    const user = new User();
    const data = {
      id_etiqueta: tagSelected,
      id_colecao: collectionSelected,
      nome: name,
      id_usuario: user.getUser().id_usuario
    };
    ProvaoApi.save(data)
      .then((result) => { 
        //SALVA AS CONFIGURAÇÕES SE JA TIVER ALGUMA CONFIGURACAO ANTIGA
        if(result.configuracoes_ultimo_provao.length >=1) {
          //só usei o map pq o back tá retornando em um formato diferente
          ProvaoApi.saveConfigs(result.provao.id_provao, result.configuracoes_ultimo_provao.map(configs => {
            return configs.configuracoes
          })).then(result2 => {
            if (user.isAdmin()) return this.props.history.push('/provao_configuracao/:id_provao'.replace(':id_provao',result.provao.id_provao))

            return this.props.history.push('gestao_provao')
          })
        } else {
          if (user.isAdmin()) return this.props.history.push('/provao_configuracao/:id_provao'.replace(':id_provao',result.provao.id_provao))
          alert("Foi criado um provão sem nenhuma configuração, entre como administrador e crie as configurações do seu provão ")


        }
      }
      )
      .catch(() =>
        enqueueSnackbar(SAVE_CONFIG_MESSAGE_FAILURE, { variant: "error" })
      );
  }

  redirectToBackward() {
    const { history } = this.props;
    history.goBack();
  }

  render() {
    const { classes, collections, tags } = this.props;
    return (
      <Container fixed>
        <Grid container spacing={2} className={classes.root}>
          <Paper className={classes.paper}>
            <Typography variant={"h6"}>Adicionar Provão</Typography>

            <br />

            <Grid container spacing={2}>
              <Grid item>
                <TextField
                  onChange={e => this.handleChange(e.target.value, "name")}
                  value={this.state.name}
                  name={"Nome do Provão"}
                />
              </Grid>
              <Grid item>
                {this.renderSelect("Marca", "tagSelected", {
                  options: tags.tags,
                  label: "nome",
                  value: "id_etiqueta"
                })}
              </Grid>
              <Grid item>
                {this.renderSelect("Coleção", "collectionSelected", {
                  options: collections.collections,
                  label: "colecao",
                  value: "id_colecao"
                })}
              </Grid>
            </Grid>

            <br />
            <br />
            <br />

            <Grid container spacing={2}>
              <Grid item>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => this.redirectToBackward()}
                >
                  Cancelar
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => this.handleSave()}
                >
                  Salvar
                </Button>
              </Grid>

            </Grid>

          </Paper>
        </Grid>
      </Container>
    );
  }
}

const wrapperComponent = withStyles(styles)(
  withSnackbar(withRouter(ProvaoAdd))
);

const mapStateToProps = state => ({
  collections: state.collections.list,
  tags: state.tags.list,
});

const mapDispatchToProps = dispatch => ({
  loadCollections: () => dispatch(loadCollections()),
  loadTags: () => dispatch(loadTags()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(wrapperComponent);
