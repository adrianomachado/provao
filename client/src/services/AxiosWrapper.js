import axios from 'axios';

import { BASE_URL } from '../consts';
import User from './User';

const defaultConfig = {
  baseURL: BASE_URL,
  headers: {
    authorization: getAuth(),
  }
};

function getAuth() {
  if (User.isAuthenticated()) {
    const user = new User();
    return user.getToken();
  }
  return null;
}

export default class AxiosWrapper {

  static get(url, config){
    return axios.get(url, {...defaultConfig, ...config})
      .then(response => response.data);
  }

  static delete(url, config){
    return axios.delete(url, {...defaultConfig, ...config})
      .then(response => response.data);
  }

  static post(url, data, config) {
    return axios.post(url, data, {...defaultConfig, ...config})
      .then(response => response.data);
  }

  static patch(url, data) {
    return axios.patch(url, data, {...defaultConfig})
      .then(response => response.data);
  }

};
