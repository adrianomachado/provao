import io from 'socket.io-client';
import { BASE_URL } from "../consts";

class Socket {

  constructor() {
    this.socket = io("/",{
      // transports: ['websocket'],
      transports: ['polling','websocket'],
      // timeout: 5000,

      forceNew: true
    });
  }

  listen(route, action) {
    this.socket.on(route, data => {
      action(data);
    });
  }



}

export default Socket;
