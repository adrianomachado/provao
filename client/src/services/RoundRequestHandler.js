import RoundTrialApi from "./apis/RoundTrialApi";
import { DROPPABLE_ID_AVAILABLE, DROPPABLE_ID_ROUND } from "../consts";

export default class RoundRequestHandler {
  constructor(source, destination) {
    this.sourceName = source.droppableId;
    this.destinationName = destination.droppableId;
  }

  updateRound(round) {
    return RoundTrialApi.update(round.id_rodada, {
      rodada_produtos: round.rodada_produtos.map(p => ({
        id_produto_estilo: p.id_produto_estilo
      }))
    });
  }

  shouldRequest() {
    return !(
      this.destinationName.includes(DROPPABLE_ID_AVAILABLE) &&
      this.sourceName.includes(DROPPABLE_ID_AVAILABLE)
    );
  }

  shouldUpdateOnlyDestination() {
    return (
      this.destinationName === this.sourceName ||
      this.sourceName.includes(DROPPABLE_ID_AVAILABLE)
    );
  }

  shouldUpdateOnlySource() {
    return (
      (this.destinationName.includes(DROPPABLE_ID_ROUND) &&
        this.sourceName.includes(DROPPABLE_ID_AVAILABLE)) ||
      (this.destinationName.includes(DROPPABLE_ID_AVAILABLE) &&
        this.sourceName.includes(DROPPABLE_ID_ROUND))
    );
  }

  update(sourceList, destinationList) {
    if (!this.shouldRequest()) {
      return Promise.resolve();
    }

    if (this.shouldUpdateOnlyDestination()) {
      return this.updateRound(destinationList);
    }

    if (this.shouldUpdateOnlySource()) {
      return this.updateRound(sourceList);
    }

    return Promise.all([
      this.updateRound(sourceList),
      this.updateRound(destinationList)
    ]);
  }
}
