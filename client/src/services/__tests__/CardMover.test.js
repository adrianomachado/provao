import CardMover from '../CardMover';
import { DROPPABLE_ID_ROUND, DROPPABLE_ID_AVAILABLE } from '../../consts';

describe('CardMoverTest', () => {

  describe('available list to rounds', () => {

    let cardMover, destination, source;

    beforeEach(() => {
      destination = {index: 0, droppableId: DROPPABLE_ID_ROUND + '0' };
      source = {index: 0, droppableId: DROPPABLE_ID_AVAILABLE };
      const roundsList = [[]];
      const availableItemsList = [{id_produto_estilo: 1}];
      cardMover = new CardMover(destination, source, {rounds: roundsList, availableItems: availableItemsList });
    });

    it('available items should be empty', () => {
      const { availableItems } = cardMover.moveCard();
      expect(availableItems).toHaveLength(0);
    });

    it('rounds should have one item', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds).toHaveLength(1);
    });

  });

  describe('rounds to available list', () => {

    let cardMover, destination, source;

    beforeEach(() => {
      source = {index: 0, droppableId: DROPPABLE_ID_ROUND + '0' };
      destination = {index: 0, droppableId: DROPPABLE_ID_AVAILABLE };
      const roundsList = [[{id_produto_estilo: 1}]];
      const availableItemsList = [];
      cardMover = new CardMover(destination, source, {rounds: roundsList, availableItems: availableItemsList });
    });

    it('available items should be empty', () => {
      const { availableItems } = cardMover.moveCard();
      expect(availableItems).toHaveLength(1);
    });

    it('rounds should have one item', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[0]).toHaveLength(0);
    });

  });

  describe('rounds to rounds', () => {

    let cardMover, destination, source;

    beforeEach(() => {
      source = {index: 0, droppableId: DROPPABLE_ID_ROUND + '0' };
      destination = {index: 0, droppableId: DROPPABLE_ID_ROUND + '1' };
      const roundsList = [[{id_produto_estilo: 1}], [{id_produto_estilo: 2}]];
      const availableItemsList = [];
      cardMover = new CardMover(destination, source, {rounds: roundsList, availableItems: availableItemsList });
    });

    it('round source should be empty', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[0]).toHaveLength(0);
    });

    it('round destination should have one item', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[1]).toHaveLength(2);
    });

  });

  describe('round to same round', () => {

    let cardMover, destination, source;

    beforeEach(() => {
      source = {index: 0, droppableId: DROPPABLE_ID_ROUND + '0' };
      destination = {index: 1, droppableId: DROPPABLE_ID_ROUND + '0' };
      const roundsList = [[{id_produto_estilo: 1}, {id_produto_estilo: 2}]];
      const availableItemsList = [];
      cardMover = new CardMover(destination, source, {rounds: roundsList, availableItems: availableItemsList });
    });

    it('round should be have same quantity of items', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[0]).toHaveLength(2);
    });

    it('should change items orders (testing first item)', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[0][0].id_produto_estilo).toBe(2);
    });

    it('should change items orders (testing second item)', () => {
      const { rounds } = cardMover.moveCard();
      expect(rounds[0][1].id_produto_estilo).toBe(1);
    });

  });

});
