import ProductsApi from "../apis/ProductsApi";
import axios from "axios";
import { BASE_URL } from "../../consts";

jest.mock("axios");

describe("ProductsApi test", () => {
  describe("requester", () => {
    beforeEach(() => {
      const products = [{ id_produto_estilo: 1 }];
      const resp = { data: products };
      axios.get.mockResolvedValue(resp);
    });

    it("should make get request", () => {
      ProductsApi.get({ marca: 1 });

      expect(axios.get).toHaveBeenCalledTimes(1);
    });

    it("should be able to pass parameters", () => {
      ProductsApi.get({
        marca: 1,
        colecao: 1,
        desc: "johnDoe",
        linha: 1,
        grupo: 1
      });

      expect(axios.get).toHaveBeenCalledWith("/produtos_estilo", {
        baseURL: BASE_URL,
        params: { marca: 1, colecao: 1, desc: "johnDoe", linha: 1, grupo: 1 }
      });
    });
  });
});
