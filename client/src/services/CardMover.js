import cloneDeep from "lodash.clonedeep";

export default class CardMover {
  constructor(destination, source, lists,) {
    this.destination = destination;
    this.source = source;
    this.lists = { ...cloneDeep(lists) };
    this.func = null;
  }

  loadDestinationLogic(func) {
    this.func = func;
  }

  moveCard() {
    const { destination, source, lists } = this;

    const MOVE_TO_NOWHERE = !this.destination;
    if (MOVE_TO_NOWHERE) {
      return { ...lists };
    }

    this.sourceInfo = this.getNameAndIndexFrom(source);
    this.destinationInfo = this.getNameAndIndexFrom(destination);

    const newList = this.moveCardsBetweenLists(destination, source, lists);

    return {
      ...newList
    };
  }

  // Works with one deep level arrays only
  getSourceList() {
    const {
      name: listName,
      index
    } = this.sourceInfo;
    return index === null
        ? this.lists[listName]
        : this.lists[listName][index];
  }

  // Works with one deep level arrays only
  getDestinationList() {
    const {
      name: listName,
      index
    } = this.destinationInfo;
    return index === null
      ? this.lists[listName]
      : this.lists[listName][index];
  }

  getNameAndIndexFrom({ droppableId }) {
    let name = null,
      index = null;
    if (this.isDroppableContainsIndex(droppableId)) {
      const separatedName = this.separateDroppableName(droppableId);
      name = separatedName[0];
      index = separatedName[1];
    } else {
      name = droppableId;
    }
    return {
      name,
      index
    };
  }

  shouldMoveToDestination(destinationSelected) {
    const { func, destinationInfo } = this;
    if ( func === null ) {
      return true;
    }

    return func(destinationInfo.name , destinationSelected);
  }

  moveCardsBetweenLists(destination, source, lists) {

    const sourceSelected = this.getSourceList();
    const destinationSelected = this.getDestinationList();

    if (
      this.shouldMoveToDestination(destinationSelected) ||
      this.isListsEquals(destination, source)
    ) {
      const [elemRemoved] = sourceSelected.splice(source.index, 1);
      destinationSelected.splice(destination.index, 0, elemRemoved);
    }

    return lists;
  }

  isListsEquals(destination, source) {
    return destination.droppableId === source.droppableId;
  }

  separateDroppableName(droppableName) {
    return droppableName.split("-");
  }

  isDroppableContainsIndex(droppableName) {
    return droppableName.search(/\d/g) !== -1;
  }
}
