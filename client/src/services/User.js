import {
  PROVAO_ROUTE,
  PROVAO_ONLINE_ROUTE,
  PROVAO_ONLINEX1_ROUTE,

  PROVAO_ORDER_ROUTE,
  PROVAO_CHOOSE,
  PROVAO_MANAGEMENT_ROUTE,
  PROVAO_ADD_ROUTE,
  PROVAO_CONFIG_ROUTE,
  USER_MANAGEMENT_ROUTE,
  RESULTADO_PROVAO_ROUTE,
  RESULTADOS_PROVOES_ROUTE

} from '../consts'

import { matchRoute } from '../utils';

const AREA_PROVAO_COORDENADOR = 'Provão - Coordenador';
const AREA_PROVAO_ATACADO = 'Provão - Atacado';
const AREA_PROVAO_VAREJO = 'Provão - Varejo';
const AREA_PROVAO_ORDENACAO = 'Provão - Ordenação';
const AREA_PROVAO_ONLINE = 'Provão - Online';
const AREA_PROVAO_ONLINE_X1 = 'Provão - Online - Físico';


class User {

  static isAuthenticated(){
    if (!localStorage.getItem("user")) {
      return false;
    }
    return true;
  }

  static logout() {
    localStorage.removeItem("user");
    localStorage.removeItem("params");
  }

  constructor() {
    this.user = localStorage.getItem("user")
      ? JSON.parse(localStorage.getItem("user")).usuario
      : null;
    this.token = localStorage.getItem("user")
      ? JSON.parse(localStorage.getItem("user")).token
      : null;
  }

  getToken() {
    return this.token;
  }

  getUser() {
    return this.user;
  }

  getAreas() {
    const user = this.getUser();
    return user.areas;
  }

  getUserInitialRoute() {
    if (this.canOrderProvao() || this.isAdmin()) {
      return PROVAO_MANAGEMENT_ROUTE;
    }

    if (this.isProvaoOnlineVoter()) {
      return PROVAO_ONLINE_ROUTE;
    }
    return PROVAO_CHOOSE;
  }

  isAdmin() {
    const user = this.getUser();
    return user.admin == 1;
  }

  canStartProvao() {
    const user = this.getUser();
    return user.areas.hasOwnProperty(AREA_PROVAO_COORDENADOR);
  }

  canOrderProvao() {
    const areas = this.getAreas();
    return areas.hasOwnProperty(AREA_PROVAO_COORDENADOR)
      || areas.hasOwnProperty(AREA_PROVAO_ORDENACAO);
  }

  isProvaoOnlineVoter() {
    const areas = this.getAreas();
    return areas.hasOwnProperty(AREA_PROVAO_ONLINE);
  }

  isProvaoOnlineX1() {
    const areas = this.getAreas
    return areas.hasOwnProperty(AREA_PROVAO_ONLINE_X1)
  }

  isAtacado(){
    const areas = this.getAreas()
    return areas.hasOwnProperty(AREA_PROVAO_ATACADO)
  }

  canAccessProvao() {
    const areas = this.getAreas();
    return areas.hasOwnProperty(AREA_PROVAO_ATACADO)
    || areas.hasOwnProperty(AREA_PROVAO_VAREJO)
    || areas.hasOwnProperty(AREA_PROVAO_COORDENADOR)
    || areas.hasOwnProperty(AREA_PROVAO_ORDENACAO)
  }

  canAcessThisRoute(route) {
    if(this.isAdmin() && !matchRoute(route,"/")) {
      return true;
    }
    if (matchRoute(route, RESULTADOS_PROVOES_ROUTE)) {
      return true;
    }

    if (matchRoute(route, PROVAO_ONLINE_ROUTE) && this.isProvaoOnlineVoter()) {
      return true;
    }
    if (matchRoute(route, PROVAO_ONLINEX1_ROUTE) && this.isProvaoOnlineVoter()) {
      return true;
    }
    if (matchRoute(route, PROVAO_MANAGEMENT_ROUTE) && this.canOrderProvao()) {
      return true;
    }
    if (matchRoute(route, PROVAO_ROUTE) && this.canAccessProvao()) {
      return true;
    }
    if (matchRoute(route, PROVAO_CHOOSE) && this.canAccessProvao()) { //eslint-disable-line
      return true;
    }
    if (matchRoute(route, PROVAO_CONFIG_ROUTE) && this.isAdmin()) {
      return true;
    }
    if (matchRoute(route, PROVAO_ADD_ROUTE) && this.canOrderProvao()) {
      return true;
    }
    if (matchRoute(route, PROVAO_ORDER_ROUTE) && this.canOrderProvao()) {
      return true;
    }
    if (matchRoute(route, USER_MANAGEMENT_ROUTE) && this.canOrderProvao()) {
      return true;
    }
    if (matchRoute(route, RESULTADO_PROVAO_ROUTE) && this.canOrderProvao()) {
      return true;
    }

    return false;
  };

}

export default User;
