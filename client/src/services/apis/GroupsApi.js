import AxiosWrapper from "../AxiosWrapper";

const GROUP_ROUTE = '/grupos_produto';

export default class LinesApi {

  static get(params){
    return AxiosWrapper.get(GROUP_ROUTE, {params: {...params}});
  }

};
