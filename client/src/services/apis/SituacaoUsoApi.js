import AxiosWrapper from "../AxiosWrapper";

const SITUACAO_ROUTE = '/situacao_uso';

export default class CollectionsApi {

  static get(params){
    return AxiosWrapper.get(SITUACAO_ROUTE, {params});
  }

};
