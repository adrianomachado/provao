
import AxiosWrapper from "../AxiosWrapper";

const ROUNDS_ROUTE = '/rodadas_provao';

export default class  RoundTrialApi {

  static get(params) {
    return AxiosWrapper.get(ROUNDS_ROUTE, {params});
  }
  static getRoundsByDay(id_provao,dia ) {
    return AxiosWrapper.get(`api/provoes/${id_provao}?dia=${dia}`);
  }

  static create(data) {
    return AxiosWrapper.post(ROUNDS_ROUTE, data);
  }

  static update(idRound, data) {
    return AxiosWrapper.patch(`${ROUNDS_ROUTE}/${idRound}`, data);
  }

  static isRoundValid(day , brand, collection) {
    return !(day === '' || brand === '' || collection === '');
  }

  static delete(idRound){
    return AxiosWrapper.delete(`${ROUNDS_ROUTE}/${idRound}`);
  }

  static getActiveRound(params) {
    return AxiosWrapper.get(`${ROUNDS_ROUTE}/ativa`, {params});
  }

  static vote(idRound, data) {
    return AxiosWrapper.post(`${ROUNDS_ROUTE}/${idRound}/voto`, data);
  }
  static getResults(idRound) {
    return AxiosWrapper.get(`${ROUNDS_ROUTE}/${idRound}/resumo`);
  }

};
