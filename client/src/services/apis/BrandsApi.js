import AxiosWrapper from "../AxiosWrapper";

const BRAND_ROUTE = '/marcas';

export default class CollectionsApi {

  static get(params){
    return AxiosWrapper.get(BRAND_ROUTE, {params});
  }

};
