import AxiosWrapper from "../AxiosWrapper";

const COLLECTION_ROUTE = '/colecoes';

export default class CollectionsApi {

  static get(params){
    return AxiosWrapper.get(COLLECTION_ROUTE, {params});
  }

};
