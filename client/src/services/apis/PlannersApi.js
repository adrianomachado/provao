
import AxiosWrapper from "../AxiosWrapper";

const PLANNERS_ROUTE = '/filterPlanner';

export default class PlannersApi {

  static get(filters){
    return AxiosWrapper.get(PLANNERS_ROUTE, {params: filters});
  }

};
