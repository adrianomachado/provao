import AxiosWrapper from "../AxiosWrapper";

const PRINT_ROUTE = '/estampas_produto';

export default class PrintsApi {

  static get(params){
    return AxiosWrapper.get(PRINT_ROUTE, {params: {...params}});
  }

};
