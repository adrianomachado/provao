
import AxiosWrapper from "../AxiosWrapper";

const PRODUCTS_ROUTE = '/produtos_estilo';

export default class ProductsApi {

  static get(filters){
    return AxiosWrapper.get(PRODUCTS_ROUTE, {params: filters})
  }

  static getImageURL(product) {
    if (product.fotoPrincipal) {
      return product.fotoPrincipal.link_arquivo;
    }
    if (product.fotos.length > 0) {
      return product.fotos[0].link_arquivo;
    }
    return '/no-picture.png'
  }

  static update(data) {
    return AxiosWrapper.patch(`${PRODUCTS_ROUTE}/update`, data);
  }

};
