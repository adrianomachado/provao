import AxiosWrapper from "../AxiosWrapper";

const LINE_ROUTE = '/linhas_produto';

export default class LinesApi {

  static get(params){
    return AxiosWrapper.get(LINE_ROUTE, {params: {...params}});
  }

};
