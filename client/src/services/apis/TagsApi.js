import AxiosWrapper from "../AxiosWrapper";
import User from "../User";

const BRAND_ROUTE = '/etiquetas';

export default class TagsApi {

  static get(params){
    const user = new User().getUser();
    return AxiosWrapper.get(`${BRAND_ROUTE}/${user.id_marca_estilo}`, {params});
  }

};
