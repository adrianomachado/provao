import AxiosWrapper from "../AxiosWrapper";

const PROVAO_ROUTE = "/provoes";
const PROVAO_CONFIG_ROUTE = "/configuracoes";
const PROVAO_PRODUCT_ROUND = "/rodadas_produto";
const PROVAO_ROUND = "/rodadas_provao";
const PROVAO_ONLINE = "/provaoOnline";


export default class ProvaoApi {
  static getById(id_provao) { 
    return AxiosWrapper.get(`api/provao/${id_provao}`)
  }
  static get(filters) {
    return AxiosWrapper.get(PROVAO_ROUTE, { params: filters });
  }

  static save(data) {
    return AxiosWrapper.post(PROVAO_ROUTE, data);
  }

  static getAvailableConfigs(filters) {
    return AxiosWrapper.get(PROVAO_CONFIG_ROUTE, { params: filters });
  }

  static getRounds(params) {
    return AxiosWrapper.get(`${PROVAO_ROUTE}/${params.id_provao}`, { params });
  }
  static getRodadaProduto(id_marca,id_usuario) {
    return AxiosWrapper.get(`${PROVAO_ONLINE}?id_marca=${id_marca}&id_usuario=${id_usuario}`)
  }

  static getConfigs(id_provao, filters) {
    return AxiosWrapper.get(
      `${PROVAO_ROUTE}/${id_provao + PROVAO_CONFIG_ROUTE}`,
      { params: filters }
    );
  }

  static saveConfigs(id_provao, configs) {
    return AxiosWrapper.patch(
      `${PROVAO_ROUTE}/${id_provao + PROVAO_CONFIG_ROUTE}`,
      {
        configuracoes: configs.map(config => ({
          id_configuracao: config.id_configuracao
        }))
      }
    );
  }

  static getActiveRound(id_provao) {
    return AxiosWrapper.get(`${PROVAO_ROUTE}/${id_provao}/rodada_ativa`);
  }
  static makeProvaoAvailable(id_provao) {
    return AxiosWrapper.get(`${PROVAO_ROUTE}/${id_provao}/disponibilizar_provao`);
  }

  static getLastSummary(id_provao) {
    return AxiosWrapper.get(`${PROVAO_ROUTE}/${id_provao}/resumo`);
  }

  static vote(data) {
    return AxiosWrapper.patch(`${PROVAO_PRODUCT_ROUND}/vote`, data);
  }

  static endRound(id_rodada) {
    return AxiosWrapper.get(`${PROVAO_ROUND}/${id_rodada}/finalizar`);
  }
  static startNextRound(id_provao, id_rodada) {
    return AxiosWrapper.get(
      `${PROVAO_ROUTE}/proxima_rodada?id_provao=${id_provao}&id_rodada=${id_rodada}`
    );
  }
}
