import AxiosWrapper from "../AxiosWrapper";
import User from "../User";

const DUEL_ROUTE = "/duelos";

export default class DuelApi {
  static get(params) {
    const user = new User().getUser();
    return AxiosWrapper.get(`${DUEL_ROUTE}/${user.id_usuario}`, { params });
  }

  static post(data) {
    const user = new User().getUser();
    return AxiosWrapper.post(`${DUEL_ROUTE}`, {...data, id_usuario: user.id_usuario});
  }
}
