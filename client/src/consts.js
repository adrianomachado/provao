
export const drawerContainerWidth = 320;
// export const BASE_URL = 'https://private-b3f60-provaoordenacao.apiary-mock.com';
// export const BASE_URL = 'http://localhost:3307';
export const BASE_URL = '/api';

// export const BASE_URL = 'https://private-6dcc18-provaoonline.apiary-mock.com';
// export const BASE_URL = 'https://estilo-backend-dot-apt-bonbon-179602.appspot.com';
// export const BASE_URL = 'http://private-27c98f-adrianoricardomachado.apiary-mock.com';
// export const BASE_URL = 'https://estilo-backend-homolog-dot-apt-bonbon-179602.appspot.com';

export const DROPPABLE_ID_AVAILABLE = 'availableItems';
export const DROPPABLE_ID_ROUND = 'rounds-';
export const DROPPABLE_TRASH = 'trash';
export const MAX_DAYS = 10;
export const LIMIT_PER_ROUND = 4;
// export const PLM_API_URL = 'https://plm-homolog-dot-apt-bonbon-179602.appspot.com/api';
export const PLM_API_URL = 'https://desenvolvimento.somagrupo.com.br/api';

export const PROVAO_ADD_ROUTE = '/adicionar_provao';
export const PROVAO_ORDER_ROUTE = '/provao_ordenacao/:id_provao';
export const PROVAO_CONFIG_ROUTE = '/provao_configuracao/:id_provao';
export const LOGIN_ROUTE = '/login';
export const PROVAO_MANAGEMENT_ROUTE = '/gestao_provao';
export const PROVAO_ROUTE = '/provao/:id_provao';
export const ACOMPANHAMENTO_PROVAO_ROUTE = '/acompanhamento_provao/:id_provao';
export const PROVAO_ONLINE_ROUTE = '/provao_online';
export const PROVAO_ONLINEX1_ROUTE = '/provao_online_x1';
export const PROVAO_RESUME_ROUTE = '/resumo';
export const USER_MANAGEMENT_ROUTE = '/gestao_usuarios';
export const PROVAO_CHOOSE = '/escolha_provao';
export const RESULTADO_PROVAO_ROUTE = '/resultado_provao/:id_provao/rodada/:id_rodada';
export const RESULTADOS_PROVOES_ROUTE = '/resultados_provoes/:id_provao'




