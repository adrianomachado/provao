
const { src, dest, task, series } = require('gulp');
const babel = require('gulp-babel');
const nodemon = require('gulp-nodemon');
const uglify = require('gulp-uglify');

task('build', function () {
  return src( [
    'node_modules/babel-polyfill/dist/polyfill.js',
    'src/**/*.js'
  ])
    .pipe(babel({
      presets: ['@babel/preset-env'],
      plugins: ['@babel/transform-runtime']
    }))
    .pipe(uglify())
    .pipe(dest('dist'));
});

task('watch', function () {
  return nodemon({
    exec: 'node --inspect',
    script: 'dist/bin/server.js', // run ES5 code
    watch: 'src/**', // watch ES2015 code
    tasks: ['build'], // compile synchronously onChange
  });
});


